import { API_URL, METRIC_URL, NODE_BASE_URL, PHP_BASE_URL, SUMMARY_URL } from "./common";


// Auth APIs

export const REGISTER_URL = `${NODE_BASE_URL}${API_URL}/auth/register`;
export const LOGIN_URL = `${NODE_BASE_URL}${API_URL}/auth/login`;

// Common APIs

export const OVERVIEW_ALL_CLIENTS = `${NODE_BASE_URL}${METRIC_URL}/getTenants`;
export const OVERVIEW_ALL_MODULES = `${NODE_BASE_URL}${METRIC_URL}/getModules`;

// Dashboard APIs

export const DASHBOARD_CARD_TOTAL_VIEWS = `${NODE_BASE_URL}${METRIC_URL}/getTotalViews`;
export const DASHBOARD_CARD_UNIQUE_USERS = `${NODE_BASE_URL}${METRIC_URL}/getTotalUsers`;
export const DASHBOARD_CARD_AVERAGE_LOAD_TIME = `${NODE_BASE_URL}${METRIC_URL}/getAvgLoadTime`;
export const DASHBOARD_CARD_AVERAGE_SESSION_TIME = `${NODE_BASE_URL}${METRIC_URL}/getAverageSessionTimeDashBoard`;

export const DASHBOARD_CHART_VISITS_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/clickTrendsDB`;
export const DASHBOARD_CHART_USERS_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/userTrendsDB`;
export const DASHBOARD_CHART_PERFORMANCE_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/performanceTrendsDB`;
export const DASHBOARD_CHART_SESSION_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/sessionTrendsDB`;
export const DASHBOARD_CHART_PLATFORM_COUNT = `${NODE_BASE_URL}${METRIC_URL}/platformCountDB`;
export const DASHBOARD_CHART_BROWSER_COUNT = `${NODE_BASE_URL}${METRIC_URL}/browserCountDB`;
export const DASHBOARD_CHART_MODULE_SESSION_TIME = `${NODE_BASE_URL}${METRIC_URL}/moduleSessionsTimeDB`;
export const DASHBOARD_TABLE_FEATURE_CLICKS = `${NODE_BASE_URL}${METRIC_URL}/featureClicksDB`;


// Visits Trends API

export const VISITS_CARD_TOTAL_VIEWS = `${NODE_BASE_URL}${METRIC_URL}/getTotalViewsPage`;
export const VISITS_CARD_UNIQUE_USERS = `${NODE_BASE_URL}${METRIC_URL}/getTotalUsersDataPage`;
export const VISITS_CARD_MODULE_USER_COUNT = `${NODE_BASE_URL}${METRIC_URL}/mostVisitedModuleCountPage`;
export const VISITS_CARD_MOST_VISITED_MODULE = `${NODE_BASE_URL}${METRIC_URL}/getmostVisitedModuleUserCountPage`;

export const VISITS_CHART_VISITS_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/getClickTrendsPage`;
export const VISITS_CHART_PLATFORM_COUNT = `${NODE_BASE_URL}${METRIC_URL}/getPlatformCountPage`;
export const VISITS_TABLE_USERS_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/getVisitedUsersPage`;
export const VISITS_CHART_MODULE_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/getVisitedModulesPage`;
export const VISITS_TABLE_DATA = `${NODE_BASE_URL}${METRIC_URL}/getModuleDetailedMetricsPage`;


// Performance Trends APIs

export const PERFORMANCE_CARD_AVERAGE_LOAD_TIME = `${NODE_BASE_URL}${METRIC_URL}/getAvgPerformanceLoadPage`;
export const PERFORMANCE_CARD_BEST_MODULE_PAGE = `${NODE_BASE_URL}${METRIC_URL}/bestModulePerformanceMetricsPage`;
export const PERFORMANCE_CARD_WORST_MODULE_PAGE = `${NODE_BASE_URL}${METRIC_URL}/worstModulePerformanceMetricsPage`;

export const PERFORMANCE_CHART_LOAD_TIME_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/getPerformanceTrendsLoadPage`;
export const PERFORMANCE_CHART_MODULE_LOAD_TIME_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/getModuleAverageLoadTimeLoadPage`;
export const PERFORMANCE_CHART_MODULE_CLICK_LOAD_TIME_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/getClickPerformanceTrendsLoadPage`;
export const PERFORMANCE_TABLE_DATA = `${NODE_BASE_URL}${METRIC_URL}/getModuleDetailedMetricsLoadPage`;

// Session Trends APIs

export const SESSIONS_CARD_AVERAGE_SESSION_TIME = `${NODE_BASE_URL}${METRIC_URL}/getaverageSessionTimeResultSession`;
export const SESSIONS_CARD_MODULE_DATA = `${NODE_BASE_URL}${METRIC_URL}/getBestWorstModulesSession`;

export const SESSIONS_CHART_SESSION_TIME_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/getsessionTrendsSession`;
export const SESSIONS_CHART_MODULE_SESSION_TIME_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/getmoduleSessionsTimeSession`;
export const SESSIONS_TABLE_DATA = `${NODE_BASE_URL}${METRIC_URL}/getusersSessionTimeSession`;


// Module Analytics APIs

export const MODULE_CARD_TOTAL_VIEWS = `${NODE_BASE_URL}${METRIC_URL}/getTotalVisitsModule`;
export const MODULE_CARD_UNIQUE_USERS = `${NODE_BASE_URL}${METRIC_URL}/getUniqueUsersModule`;
export const MODULE_CARD_AVERAGE_LOAD_TIME = `${NODE_BASE_URL}${METRIC_URL}/getAverageLoadTimeModule`;
export const MODULE_CARD_AVERAGE_SESSION_TIME = `${NODE_BASE_URL}${METRIC_URL}/getModuleAvgSessionTimeModule`;

export const MODULE_CHART_VISITS_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/getClickTrendsModule`;
export const MODULE_CHART_USERS_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/getUserTrendsModule`;
export const MODULE_CHART_PERFORMANCE_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/getPerformanceTrendsModule`;
export const MODULE_CHART_SESSION_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/getModuleSessionTrendsModule`;
export const MODULE_CHART_PLATFORM_COUNT = `${NODE_BASE_URL}${METRIC_URL}/getPlatformCountModule`;
export const MODULE_CHART_USER_VISITS = `${NODE_BASE_URL}${METRIC_URL}/getUserVisitCountModule`;

// Comapre Tenants APIs

export const COMPARE_TENANTS_CHART_VISITS_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/compareClickTrends`;
export const COMPARE_TENANTS_CHART_SESSION_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/compareLoadTrends`;


// Reports APIs

export const REPORTS_TABLE_DATA = `${NODE_BASE_URL}${METRIC_URL}/getReports`;
export const REPORTS_GENERATE_SUMMARY = `${PHP_BASE_URL}${SUMMARY_URL}/generateSummary`;


// User Analytics APIs

export const USER_ANALYTICS_CHART_VISITS_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/getClickTrendsUser`;
export const USER_ANALYTICS_CHART_SESSION_TRENDS = `${NODE_BASE_URL}${METRIC_URL}/getSessionTrendsUser`;
export const USER_ANALYTICS_CHART_TABLE_DATA = `${NODE_BASE_URL}${METRIC_URL}/getMostVisitedRoutesUser`;



export const OVERVIEW_CHART_DASHBOARD_DATA = `${NODE_BASE_URL}${METRIC_URL}/getDashboardMetrics`;
export const OVERVIEW_CHART_CLICK_DATA = `${NODE_BASE_URL}${METRIC_URL}/getOverallClickData`;
export const OVERVIEW_CHART_PERFORMANCE_DATA = `${NODE_BASE_URL}${METRIC_URL}/getOverallPerformanceData`;
export const OVERVIEW_CHART_USER_METRICS = `${NODE_BASE_URL}${METRIC_URL}/getUserMetrics`;
export const OVERVIEW_CHART_SESSION_DATA = `${NODE_BASE_URL}${METRIC_URL}/getSessionMetrics`;
export const OVERVIEW_CHART_COMPARISON_DATA = `${NODE_BASE_URL}${METRIC_URL}/compareTenants`;
export const OVERVIEW_CHART_MODULE_DATA = `${NODE_BASE_URL}${METRIC_URL}/getOverallModuleMetrics`;
export const OVERVIEW_CHART_REPORTS_DATA = `${NODE_BASE_URL}${METRIC_URL}/getReports`;
export const OVERVIEW_USERS_DATA = `${NODE_BASE_URL}${METRIC_URL}/getAllUsers`;
export const OVERVIEW_GENERATE_SUMMARY = `${PHP_BASE_URL}${SUMMARY_URL}/generateSummary`;
export const OVERVIEW_SHARE_REPORT = `${NODE_BASE_URL}${METRIC_URL}/updateReports`;

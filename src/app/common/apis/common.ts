export const NODE_BASE_URL = "http://localhost:5000";
export const PHP_BASE_URL = "http://localhost:80";
export const API_URL = "/api";
export const METRIC_URL = "/api/data";
export const SUMMARY_URL = "/userEvents/data";
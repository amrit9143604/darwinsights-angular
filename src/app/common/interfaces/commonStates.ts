import { Observable } from 'rxjs';
import {
  CardAverageLoadTimeData,
  CardDataState,
  CardModuleBestWorstData,
  CardModulePage,
  CardMostVisitedModule,
  CardUniqueUsersInMostVisitedModule,
  CardUsersData,
  CardUsersSessionsData,
  CardViewsData,
} from './cardStates';
import {
  BrowserChartData,
  ChartDataState,
  CompareChartPerformanceData,
  CompareChartVisitsData,
  LoadTimeChartData,
  ModuleChartData,
  ModuleClickPerformanceChartData,
  ModuleDetailedLoadChartData,
  ModuleDetailsTrendsChartData,
  ModuleLoadTimeChartData,
  ModuleSessionChartData,
  ModuleSessionTimeChartData,
  ModuleVisitsChartData,
  PlatformChartData,
  ReportsTableData,
  SessionTimeChartData,
  UniqueUsersChartData,
  UsersRoutesData,
  UsersTrendsChartData,
  VisitsChartData,
} from './chartStates';


export interface AuthState {
  isLoggedIn: boolean;
}


export interface AuthResponse {
  token: string;
  user: {
    userId: string;
  };
}

export interface DashboardState {
  totalViews: CardDataState<CardViewsData>;
  uniqueUsers: CardDataState<CardUsersData>;
  averageLoadTime: CardDataState<CardAverageLoadTimeData>;
  usersSessionTime: CardDataState<CardUsersSessionsData>;
  visitsTrends: ChartDataState<VisitsChartData>;
  uniqueUsersTrends: ChartDataState<UniqueUsersChartData>;
  loadTimeTrends: ChartDataState<LoadTimeChartData>;
  sessionTrends: ChartDataState<SessionTimeChartData>;
  platformTrends: ChartDataState<PlatformChartData>;
  browserTrends: ChartDataState<BrowserChartData>;
  moduleSessionTrends: ChartDataState<ModuleSessionChartData>;
}

export interface VisitsState {
  totalViews: CardDataState<CardViewsData>;
  uniqueUsers: CardDataState<CardUsersData>;
  mostUsedModule: CardDataState<CardMostVisitedModule>;
  uniqueUsersInMostVisitedModule: CardDataState<CardUniqueUsersInMostVisitedModule>;
  visitsTrends: ChartDataState<VisitsChartData>;
  platformTrends: ChartDataState<PlatformChartData>;
  moduleTrends: ChartDataState<ModuleChartData>;
  visitedUsers: ChartDataState<UsersTrendsChartData>;
  moduleDetailsTrends: ChartDataState<ModuleDetailsTrendsChartData>;
}

export interface PerformanceState {
  averageLoadTime: CardDataState<CardAverageLoadTimeData>;
  bestModulePage: CardDataState<CardModulePage>;
  worstModulePage: CardDataState<CardModulePage>;
  loadTimeTrends: ChartDataState<LoadTimeChartData>;
  moduleLoadTimeTrends: ChartDataState<ModuleLoadTimeChartData>;
  moduleClickPerformanceTrends: ChartDataState<ModuleClickPerformanceChartData>;
  moduleDetailedLoadTrends: ChartDataState<ModuleDetailedLoadChartData>;
}

export interface SessionsState {
  usersSessionTime: CardDataState<CardUsersSessionsData>;
  sessionTrends: ChartDataState<SessionTimeChartData>;
  moduleSessionTrends: ChartDataState<ModuleSessionChartData>;
  bestWorstModule: CardDataState<CardModuleBestWorstData>;
}

export interface ModuleAnalyticsState {
  totalViews: CardDataState<CardViewsData>;
  uniqueUsers: CardDataState<CardUsersData>;
  averageLoadTime: CardDataState<CardAverageLoadTimeData>;
  usersSessionTime: CardDataState<CardUsersSessionsData>;
  visitsTrends: ChartDataState<ModuleVisitsChartData>;
  uniqueUsersTrends: ChartDataState<UniqueUsersChartData>;
  loadTimeTrends: ChartDataState<LoadTimeChartData>;
  moduleSessionTrends: ChartDataState<ModuleSessionTimeChartData>;
  platformTrends: ChartDataState<PlatformChartData>;
}
export interface ReportsState {
  tableData: ChartDataState<ReportsTableData>;
}


export interface ComparisonState {
  visitsTrends: ChartDataState<VisitsChartData>;
  sessionTrends: ChartDataState<SessionTimeChartData>;
}


export interface UserAnalyticsState {
  visitsTrends: ChartDataState<VisitsChartData>;
  sessionTrends: ChartDataState<SessionTimeChartData>;
  routesTrends: ChartDataState<UsersRoutesData>;
}

export const timeLabels: { [key: string]: string } = {
  WEEKLY: 'Last 7 Days',
  BIWEEKLY: 'Last 14 Days',
  MONTHLY: 'Last 30 Days',
  QUARTERLY: 'Last 3 Months',
};

export const timeArray = [
  {
    label: 'Last 7 Days',
    value: 'WEEKLY',
  },
  {
    label: 'Last 14 Days',
    value: 'BIWEEKLY',
  },
  {
    label: 'Last 30 Days',
    value: 'MONTHLY',
  },
  {
    label: 'Last 3 Months',
    value: 'QUARTERLY',
  },
];

export interface metricOptions {
    label: string;
    value: string;
  }
  
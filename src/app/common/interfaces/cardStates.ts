export interface CardDataState<T> {
  data: T;
  loading: boolean;
  error: string | null;
}

export interface CardViewsData {
  totalViews: number | null;
  percentageChange: number | null;
}

export interface CardUsersData {
  totalUsers: number | null;
  percentageChange: number | null;
}

export interface CardAverageLoadTimeData {
  averageLoadTime: number | null;
  percentageChange: number | null;
}

export interface CardUsersSessionsData {
  averageSessionTime: number | null;
  sessionPercentageChange: number | null;
}

export interface CardMostVisitedModule {
  mostVisitedModule: string | null;
  views: number | null;
}

export interface CardUniqueUsersInMostVisitedModule { 
  mostVisitedModule: string | null;
  uniqueUserCount: number | null;
}

export interface CardModulePage { 
  module: string | null;
  loadTime: number | null;
}

export interface CardModuleBestWorstData {
  highestUsedModule: string;
  lowestUsedModule: string;
  highestUsage: string;
  lowestUsage: string;
}
export interface ChartDataState<T> {
  data: T;
  loading: boolean;
  error: string | null;
}

export interface ChartOptions<T> {
  series: Series<T>[];
}

export interface Series<T> {
  data: T[];
  xKey: string;
  yKey: string;
  yName: string;
}

export interface VisitsChartData {
  clickTrends: VisitsData[];
}

export interface VisitsData {
  views: number;
  date: string;
}
export interface ModuleVisitsChartData {
  clickTrends: ModuleVisitsData[];
}

export interface ModuleVisitsData {
  visits: number;
  date: string;
}

export interface UniqueUsersChartData {
  userTrends: UsersData[];
}

export interface UsersData {
  activeUsers: number;
  date: string;
}

export interface LoadTimeChartData {
  performanceTrends: LoadTimeData[];
}

export interface LoadTimeData {
  avgPerformance: number;
  date: string;
}

export interface SessionTimeChartData {
  sessionTrends: SessionData[];
}

export interface ModuleSessionTimeChartData {
  moduleSessionTrends: SessionData[];
}

export interface    SessionData {
  sessionTime: number;
  date: string;
  unit: string;
}

export interface PlatformChartData {
  result: PlatformData[];
}

export interface PlatformData {
  count: number;
  platform: string;
}

export interface BrowserChartData {
  result: BrowserData[];
}

export interface BrowserData {
  count: number;
  browser: string;
}

export interface ModuleSessionChartData {
  result: ModuleSessionData[];
}

export interface ModuleSessionData {
  timeSpentInSecs: number;
  Module: string;
}

export interface ModuleChartData {
  visitedModules: ModuleData[];
}

export interface ModuleData {
  views: number;
  route: string;
}

export interface UsersTrendsChartData {
  visitedUsers: UsersTrendsData[];
}

export interface UsersTrendsData {
  count: number;
  ip: string;
  userId: string;
}

export interface ModuleDetailsTrendsChartData {
  moduleDetailedMetrics: ModuleDetailsTrendsData[];
}

export interface ModuleDetailsTrendsData {
  totalClicks: number;
  moduleName: string;
  route: string;
  platform: string;
  avgLoadTime: number;
}

export interface ModuleLoadTimeChartData {
  moduleAverageLoadTime: ModuleLoadTimeData[];
}

export interface ModuleLoadTimeData {
  Module: string;
  'Average Load Time': number;
}


export interface ModuleClickPerformanceChartData {
  clickPerformanceTrends: ModuleClickPerformanceData[];
}

export interface ModuleClickPerformanceData {
  moduleName: string;
  date: string;
  views: number;
  avgPerformance: number;
}

export interface ModuleDetailedLoadChartData {
  moduleDetailedMetrics: ModuleDetailedLoadData[];
}

export interface ModuleDetailedLoadData {
  moduleName: string;
  route: string;
  platform: string;
  totalClicks: number;
  avgLoadTime: number;
}



export interface CompareLoadTimeChartData {
  performanceTrends: LoadTimeData[];
}

export interface CompareLoadTimeData {
  avgPerformance: number;
  date: string;
}

export interface CompareChartVisitsData{
  clickTrends: CompareData[];
}

export interface CompareChartPerformanceData{
  performanceTrends: CompareData[];
}

export interface CompareData{
  [key:string]:any;
  date: string
}

export interface ReportsTableData{
  tableData: ReportsData[];
}


export interface ReportsData{
  _id: string,
  reportName: string,
  reportUrl: string,
  generatedOn: string,
  summaryType: string,
  createdBy: string,
}



export interface UsersRoutesData{
  tableData: RoutesData[];
}


export interface RoutesData{
  visitCount: number,
  module: string,
  platform: string,
  route: string,
}



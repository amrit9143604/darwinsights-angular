import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable, map, tap } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { selectIsLoggedIn } from '../store/main.selectors';

@Injectable({
  providedIn: 'root',
})
export class RouteGuard implements CanActivate {
  constructor(private store: Store, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.store.pipe(
      select(selectIsLoggedIn),
      map((isLoggedIn: boolean) => {
        if (route.data['allowWhenLoggedIn'] === isLoggedIn) {
          return true;
        } else {
          return this.router.parseUrl(route.data['redirectTo']);
        }
      })
    );
  }
}

import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthService } from '../../services/auth.service';
import { login, logout, register } from '../../store/auth.actions';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { UpperCasePipe } from '@angular/common';
import { noWhitespaceValidator } from '../../validators/emptySpaces';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-feature-register',
  templateUrl: './feature-register.component.html',
  styleUrls: ['./feature-register.component.scss'],
  providers: [UpperCasePipe],
})
export class FeatureRegisterComponent implements OnInit {
  authForm!: FormGroup;
  constructor(private store: Store, private toastr: ToastrService) {}

  ngOnInit(): void {
    this.authForm = new FormGroup({
      employeeId: new FormControl('', [
        Validators.required,
        noWhitespaceValidator(),
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', [
        Validators.required,
        noWhitespaceValidator(),
        Validators.minLength(6),
      ]),
    });
  }

  onSubmit(): void {
    const controls = this.authForm.controls;

    if (this.authForm.valid) {
      const employeeId = controls['employeeId'].value.trim().toUpperCase();
      const password = controls['password'].value.trim();
      const email = controls['email'].value.trim();

      this.store.dispatch(
        register({ userId: employeeId, email: email, password: password })
      );
    }
  }

}

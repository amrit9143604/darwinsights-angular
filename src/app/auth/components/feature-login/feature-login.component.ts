import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthService } from '../../services/auth.service';
import { login, logout } from '../../store/auth.actions';
import {
  FormGroup,
  FormControl,
  Validators,
  ValidationErrors,
} from '@angular/forms';
import { noWhitespaceValidator } from '../../validators/emptySpaces';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-feature-login',
  templateUrl: './feature-login.component.html',
  styleUrls: ['./feature-login.component.scss'],
})
export class FeatureLoginComponent implements OnInit {
  authForm!: FormGroup;

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.authForm = new FormGroup({
      employeeId: new FormControl('', [
        Validators.required,
        noWhitespaceValidator(),
      ]),
      password: new FormControl('', [
        Validators.required,
        noWhitespaceValidator(),
        Validators.minLength(6),
      ]),
    });
  }

  onSubmit(): void {
    if (this.authForm.valid) {
      const employeeId = this.authForm
        .get('employeeId')!
        .value.trim()
        .toUpperCase();
      const password = this.authForm.get('password')!.value.trim();

      this.store.dispatch(login({ userId: employeeId, password: password }));
    }
  }

}

import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { map, switchMap, catchError, tap, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { Router } from '@angular/router';
import {
  login,
  loginSuccess,
  loginFailure,
  register,
  registerSuccess,
  registerFailure,
} from './auth.actions';
import { AuthService } from '../services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private toastr: ToastrService
  ) {}

  loginUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(login),
      mergeMap(({ userId, password }) => {
        return this.authService.login(userId, password).pipe(
          map(({ user }) =>
            loginSuccess({
              userId: user.userId,
              tenant: null,
              time: 'WEEKLY',
              module: 'Attendance',
            })
          ),
          tap(() => {
            this.toastr.success('Login Successful', 'Success', {
              timeOut: 3000,
              progressBar: true,
              closeButton: true,
              extendedTimeOut: 1000,
              easing: 'ease',
            });
          }),
          catchError((error) => {
            this.toastr.error('Wrong Credentials', 'Error', {
              timeOut: 3000,
              progressBar: true,
              closeButton: true,
              extendedTimeOut: 1000,
              easing: 'ease',
            });
            return of(loginFailure());
          })
        );
      })
    )
  );

  registerUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(register),
      switchMap(({ userId, email, password }) => {
        return this.authService.register(userId, email, password).pipe(
          map(({ token, user }) =>
            registerSuccess({
              userId: user.userId,
              tenant: null,
              time: 'WEEKLY',
              module: 'Attendance',
            })
          ),
          tap(() => {
            this.toastr.success('Register Successful', 'Success', {
              timeOut: 3000,
              progressBar: true,
              closeButton: true,
              extendedTimeOut: 1000,
              easing: 'ease',
            });
          }),
          catchError((error) => {
            this.toastr.error('User Already Exists', 'Error', {
              timeOut: 3000,
              progressBar: true,
              closeButton: true,
              extendedTimeOut: 1000,
              easing: 'ease',
            });
            return of(registerFailure());
          })
        );
      })
    )
  );
}

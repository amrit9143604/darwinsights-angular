import { createAction, props } from '@ngrx/store';

export const login = createAction(
  '[Main Auth Component] Login',
  props<{ userId: string; password: string }>()
);

export const loginSuccess = createAction(
  '[Main Auth Component] loginSuccess',
  props<{
    userId: string;
    tenant: string | null;
    time: string;
    module: string | null;
  }>()
);
export const loginFailure = createAction('[Main Auth Component] loginFailure');

export const register = createAction(
  '[Main Auth Component] Register',
  props<{ userId: string; email: string, password: string }>()
);


export const registerSuccess = createAction(
  '[Main Auth Component] registerSuccess',
  props<{
    userId: string;
    tenant: string | null;
    time: string;
    module: string | null;
  }>()
);
export const registerFailure = createAction('[Main Auth Component] registerFailure');

export const logout = createAction('[Main Auth Component] Logout');


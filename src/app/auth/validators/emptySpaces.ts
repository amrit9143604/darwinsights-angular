import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function noWhitespaceValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value || '';
    if (!value) return null;
    const isWhitespace = value.trim().length === 0;
    return !isWhitespace ? null : { noWhitespaceValidator: true };
  };
}

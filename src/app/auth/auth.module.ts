import { NgModule, forwardRef } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { AuthComponent } from './auth.component';
import { FeatureLoginComponent } from './components/feature-login/feature-login.component';
import { FeatureRegisterComponent } from './components/feature-register/feature-register.component';
import { UiAuthComponent } from './components/ui-auth/ui-auth.component';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from './store/auth.effects';
import { FormsModule, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { SharedModule } from '../shared/shared.module';
import { UiFormInputComponent } from '../shared/ui-form-input/ui-form-input.component';

@NgModule({
  declarations: [
    AuthComponent,
    FeatureLoginComponent,
    FeatureRegisterComponent,
    UiAuthComponent,
  ],
  imports: [
    EffectsModule.forFeature([AuthEffects]),
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UiFormInputComponent),
      multi: true,
    },
    ],
})
export class AuthModule {}

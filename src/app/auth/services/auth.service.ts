import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { CookieService } from 'ngx-cookie-service';
import { Observable, tap } from 'rxjs';
import { LOGIN_URL, REGISTER_URL } from 'src/app/common/apis/constants';
import { AuthResponse } from 'src/app/common/interfaces/commonStates';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private http: HttpClient,
    private router: Router,
    private cookieService: CookieService
  ) {}

  login(userId: string, password: string): Observable<AuthResponse> {
    const body = {
      userId: userId,
      password: password,
    };
    return this.http.post<any>(LOGIN_URL, body).pipe(
      tap((data) => {
        if (data) {
          console.log(data);
          this.cookieService.set('USER_ID', data.user.userId, 2, '/');
          this.cookieService.set('JWT_AUTH', data.token, 2, '/');
          this.cookieService.set('TENANT', 'null', 2, '/');
          this.cookieService.set('TIME', 'WEEKLY', 2, '/');
          this.cookieService.set('MODULE', 'Attendance', 2, '/');
        this.router.navigateByUrl('/metrics/dashboard');
        }
      })
    );
  }

  register(userId: string, email: string, password: string): Observable<AuthResponse> {
    const body = {
      userId: userId,
      email: email,
      password: password,
    };
    return this.http.post<any>(REGISTER_URL, body).pipe(
      tap((data) => {
        if (data) {
          this.cookieService.set('USER_ID', data.user.userId, 2, '/');
          this.cookieService.set('JWT_AUTH', data.token, 2, '/');
          this.cookieService.set('TENANT', 'null', 2, '/');
          this.cookieService.set('TIME', 'WEEKLY', 2, '/');
          this.cookieService.set('MODULE', 'Attendance', 2, '/');

          this.router.navigateByUrl('/metrics/dashboard');
        }
      })
    );
  }
}

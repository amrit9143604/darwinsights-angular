import { NgModule } from '@angular/core';
import { FeatureLoginComponent } from './components/feature-login/feature-login.component';
import { FeatureRegisterComponent } from './components/feature-register/feature-register.component';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth.component';
import { UiAuthComponent } from './components/ui-auth/ui-auth.component';

const routes: Routes = [
  {
    path: 'auth',
    redirectTo: 'auth/login',
    pathMatch: 'full',
  },
  {
    path: '',
    component: UiAuthComponent,
    children: [
      {
        path: 'login',
        component: FeatureLoginComponent,
        title: 'Login | Darwinsight',
      },
      {
        path: 'register',
        component: FeatureRegisterComponent,
        title: 'Register | Darwinsight',
      },
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}

import { Component, Input, OnChanges } from '@angular/core';
import { AgChartOptions } from 'ag-charts-community';
import 'ag-charts-enterprise';

@Component({
  selector: 'app-ui-area-chart',
  templateUrl: './ui-area-chart.component.html',
  styleUrls: ['./ui-area-chart.component.scss'],
})
export class UiAreaChartComponent implements OnChanges {
  @Input() xKey!: string;
  @Input() yKey!: string;
  @Input() yName!: string;
  @Input() data!: any[];

  chartOptions!: AgChartOptions;

  constructor() {}

  ngOnChanges(): void {
    console.log(this.data);
    if (this.data) {
      this.chartOptions = {
        data: this.data,
        animation: {
          enabled: true,
        },
        series: [
          {
            type: 'area',
            xKey: this.xKey,
            yKey: this.yKey,
            yName: this.yName,
            stroke: '#72a4e7',
            strokeWidth: 3,
            fillOpacity: 1,
            fill: '#aecaf1',
            connectMissingData: true,
            marker: {
              enabled: true
            },
          },
        ],
      };
    } else {
      this.chartOptions = {
        data: [],
        animation: {
          enabled: true,
        },
        series: [
          {
            type: 'area',
            xKey: this.xKey,
            yKey: this.yKey,
            yName: this.yName,
          },
        ],
      };
    }
  }
}

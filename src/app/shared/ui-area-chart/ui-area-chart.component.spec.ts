import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiAreaChartComponent } from './ui-area-chart.component';

describe('UiAreaChartComponent', () => {
  let component: UiAreaChartComponent;
  let fixture: ComponentFixture<UiAreaChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiAreaChartComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UiAreaChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

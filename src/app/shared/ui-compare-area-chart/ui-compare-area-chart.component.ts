import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { AgChartOptions } from 'ag-charts-community';


@Component({
  selector: 'app-ui-compare-area-chart',
  templateUrl: './ui-compare-area-chart.component.html',
  styleUrls: ['./ui-compare-area-chart.component.scss']
})
export class UiCompareAreaChartComponent implements OnChanges {

  @Input() xKey!: string;
  @Input() yKeys!: string[];
  @Input() yNames!: string[];
  @Input() data!: any[];

  chartOptions!: AgChartOptions;

  constructor() {
  }


  ngOnChanges(): void {
    console.log(this.data);
    if (this.data) {
      const series: any[] = [];
      this.yKeys.map((yKey, index) => {
        series.push({
          type: 'area',
          xKey: this.xKey,
          yKey: yKey,
          yName: this.yNames[index],
        });
      });
      console.log(series);

      this.chartOptions = {
        data: this.data,
        animation: {
          enabled: true,
        },
        series: series,
      };
      console.log(this.chartOptions);
    }
  }

}

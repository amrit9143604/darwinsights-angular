import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiCompareAreaChartComponent } from './ui-compare-area-chart.component';

describe('UiCompareAreaChartComponent', () => {
  let component: UiCompareAreaChartComponent;
  let fixture: ComponentFixture<UiCompareAreaChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiCompareAreaChartComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UiCompareAreaChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

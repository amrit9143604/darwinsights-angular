import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiCardWrapperComponent } from './ui-card-wrapper.component';

describe('UiCardWrapperComponent', () => {
  let component: UiCardWrapperComponent;
  let fixture: ComponentFixture<UiCardWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiCardWrapperComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UiCardWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

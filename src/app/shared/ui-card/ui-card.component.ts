import {
  Component,
  HostBinding,
  Input,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-ui-card',
  templateUrl: './ui-card.component.html',
  styleUrls: ['./ui-card.component.scss']
})
export class UiCardComponent implements OnInit {
  @Input() name!: string;
  @Input() total$!: Observable<number | string | null>;
  @Input() percentage$!: Observable<number | null>;
  @Input() cardColor!: string;
  @Input() textColor!: string;
  @Input() changeColor!: string;
  @Input() changeBackgroundColor!: string;
  @Input() isLoading: boolean = false;

  @HostBinding('style.--card-color') get cardBgColor() {
    return this.cardColor;
  }

  @HostBinding('style.--text-color') get textBgColor() {
    return this.textColor;
  }

  @HostBinding('style.--change-color') get changeSpanColor() {
    return this.changeColor;
  }

  @HostBinding('style.--change-background-color')
  get changeSpanBackgroundColor() {
    return this.changeBackgroundColor;
  }

  constructor() {}

  ngOnInit(): void {}
}

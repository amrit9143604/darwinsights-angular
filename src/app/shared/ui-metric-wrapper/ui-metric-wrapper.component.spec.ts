import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiMetricWrapperComponent } from './ui-metric-wrapper.component';

describe('UiMetricWrapperComponent', () => {
  let component: UiMetricWrapperComponent;
  let fixture: ComponentFixture<UiMetricWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiMetricWrapperComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UiMetricWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

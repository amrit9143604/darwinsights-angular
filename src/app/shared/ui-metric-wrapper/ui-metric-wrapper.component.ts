import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-ui-metric-wrapper',
  templateUrl: './ui-metric-wrapper.component.html',
  styleUrls: ['./ui-metric-wrapper.component.scss']
})
export class UiMetricWrapperComponent implements OnInit {

  @Input() title!: string;
  @Input() isLoading: boolean = false;
  @Input() chartType: boolean = false;
  @Output() changeChartType = new EventEmitter<void>();

  constructor() { }

  ngOnInit(): void {
  }

  onChangeChartType(): void {
    this.changeChartType.emit();
  }

}

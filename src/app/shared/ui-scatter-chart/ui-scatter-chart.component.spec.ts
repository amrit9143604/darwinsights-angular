import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiScatterChartComponent } from './ui-scatter-chart.component';

describe('UiScatterChartComponent', () => {
  let component: UiScatterChartComponent;
  let fixture: ComponentFixture<UiScatterChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiScatterChartComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UiScatterChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiLineChartComponent } from './ui-line-chart.component';

describe('UiLineChartComponent', () => {
  let component: UiLineChartComponent;
  let fixture: ComponentFixture<UiLineChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiLineChartComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UiLineChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

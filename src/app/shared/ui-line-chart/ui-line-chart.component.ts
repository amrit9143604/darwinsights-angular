import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { AgChartOptions } from 'ag-charts-community';
import "ag-charts-enterprise";

@Component({
  selector: 'app-ui-line-chart',
  templateUrl: './ui-line-chart.component.html',
  styleUrls: ['./ui-line-chart.component.scss'],
})
export class UiLineChartComponent implements OnChanges {
  @Input() xKey!: string;
  @Input() yKey!: string;
  @Input() yName!: string;
  @Input() data!: any[]; 

  chartOptions!: AgChartOptions;

  constructor() {}

  ngOnChanges(): void {

    if(this.data){
      this.chartOptions = {
        data: this.data,
        animation:{
          enabled: true
        },
        
        series: [
          {
            type: 'line',
            xKey: this.xKey,
            yKey: this.yKey,
            yName: this.yName,
            connectMissingData: true,
          },
        ],
      };
    }
    else{
      this.chartOptions = {
        data: [],
        animation:{
          enabled: true
        },
        series: [
          {
            type: 'line',
            xKey: this.xKey,
            yKey: this.yKey,
            yName: this.yName,  
          },
        ],
      };
    }
  }
}

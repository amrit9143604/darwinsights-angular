import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiBarChartComponent } from './ui-bar-chart.component';

describe('UiBarChartComponent', () => {
  let component: UiBarChartComponent;
  let fixture: ComponentFixture<UiBarChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiBarChartComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UiBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { AgChartOptions } from 'ag-charts-community';


@Component({
  selector: 'app-ui-bar-chart',
  templateUrl: './ui-bar-chart.component.html',
  styleUrls: ['./ui-bar-chart.component.scss']
})
export class UiBarChartComponent implements OnChanges {

  @Input() xKey!: string;
  @Input() yKey!: string;
  @Input() yName!: string;
  @Input() data!: any[]; 

  chartOptions!: AgChartOptions;

  constructor() {}

  ngOnChanges(): void {
    if(this.data){
      this.chartOptions = {
        data: this.data,
        animation:{
          enabled: true
        },
        
        series: [
          {
            type: 'bar',
            xKey: this.xKey,
            yKey: this.yKey,
            yName: this.yName,
          },
        ],
      };
    }
    else{
      this.chartOptions = {
        data: [],
        animation:{
          enabled: true
        },
        series: [
          {
            type: 'bar',
            xKey: this.xKey,
            yKey: this.yKey,
            yName: this.yName,  
          },
        ],
      };
    }
  }

}

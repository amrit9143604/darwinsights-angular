import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiPieChartComponent } from './ui-pie-chart.component';

describe('UiPieChartComponent', () => {
  let component: UiPieChartComponent;
  let fixture: ComponentFixture<UiPieChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiPieChartComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UiPieChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { AgChartOptions } from 'ag-charts-community';

@Component({
  selector: 'app-ui-pie-chart',
  templateUrl: './ui-pie-chart.component.html',
  styleUrls: ['./ui-pie-chart.component.scss'],
})
export class UiPieChartComponent implements OnInit, OnChanges {
  @Input() angle!: string;
  @Input() legend!: string;
  @Input() data!: any[];

  getData() {
    return [
      { asset: 'Stocks', amount: 60000 },
      { asset: 'Bonds', amount: 40000 },
      { asset: 'Cash', amount: 7000 },
      { asset: 'Real Estate', amount: 5000 },
      { asset: 'Commodities', amount: 3000 },
    ];
  }

  public chartOptions!: AgChartOptions;

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(): void {
    if (this.data) {
      this.chartOptions = {
        data: this.data,
        animation: {
          enabled: true,
        },

        series: [
          {
            type: 'pie',
            angleKey: this.angle,
            legendItemKey: this.legend,
          },
        ],
      };
    } else {
      this.chartOptions = {
        data: [],
        animation: {
          enabled: true,
        },
        series: [
          {
            type: 'pie',
            angleKey: this.angle,
            legendItemKey: this.legend,
          },
        ],
      };
    }
  }
}

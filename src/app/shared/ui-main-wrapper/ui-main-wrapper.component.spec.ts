import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiMainWrapperComponent } from './ui-main-wrapper.component';

describe('UiMainWrapperComponent', () => {
  let component: UiMainWrapperComponent;
  let fixture: ComponentFixture<UiMainWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiMainWrapperComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UiMainWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

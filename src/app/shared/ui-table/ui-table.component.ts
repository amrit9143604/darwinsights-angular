import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
// import "ag-grid-community/styles/ag-grid.css";
// import "ag-grid-community/styles/ag-theme-quartz.css";
import { ColDef } from 'ag-grid-community'; 

interface IRow {
  make: string;
  model: string;
  price: number;
  electric: boolean;
}

@Component({
  selector: 'app-ui-table',
  templateUrl: './ui-table.component.html',
  styleUrls: ['./ui-table.component.scss']
})
export class UiTableComponent implements OnInit {
  @Input() rowData!: any[]
  @Input() colDefs!: any[]
  @Output() shareButtonClick: EventEmitter<TemplateRef<void>> = new EventEmitter<TemplateRef<void>>();



  constructor() { }

  ngOnInit(): void {
  }


  themeClass =
    "ag-theme-quartz";

  // Row Data: The data to be displayed.
  // rowData: IRow[] = [
  //   { make: "Tesla", model: "Model Y", price: 64950, electric: true },
  //   { make: "Ford", model: "F-Series", price: 33850, electric: false },
  //   { make: "Toyota", model: "Corolla", price: 29600, electric: false },
  //   { make: "Mercedes", model: "EQA", price: 48890, electric: true },
  //   { make: "Fiat", model: "500", price: 15774, electric: false },
  //   { make: "Nissan", model: "Juke", price: 20675, electric: false },
  // ];

  // Column Definitions: Defines & controls grid columns.
  // colDefs: ColDef<IRow>[] = [
  //   { field: "make" },
  //   { field: "model" },
  //   { field: "price" },
  //   { field: "electric" },
  // ];

  defaultColDef: ColDef = {
    flex: 1,
  };

}

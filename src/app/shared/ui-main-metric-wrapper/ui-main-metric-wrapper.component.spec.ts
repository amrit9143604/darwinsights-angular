import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiMainMetricWrapperComponent } from './ui-main-metric-wrapper.component';

describe('UiMainMetricWrapperComponent', () => {
  let component: UiMainMetricWrapperComponent;
  let fixture: ComponentFixture<UiMainMetricWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiMainMetricWrapperComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UiMainMetricWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

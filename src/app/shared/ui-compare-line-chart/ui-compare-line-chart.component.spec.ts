import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiCompareLineChartComponent } from './ui-compare-line-chart.component';

describe('UiCompareLineChartComponent', () => {
  let component: UiCompareLineChartComponent;
  let fixture: ComponentFixture<UiCompareLineChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiCompareLineChartComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UiCompareLineChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

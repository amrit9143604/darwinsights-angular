import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { AgChartOptions } from 'ag-charts-community';

@Component({
  selector: 'app-ui-compare-line-chart',
  templateUrl: './ui-compare-line-chart.component.html',
  styleUrls: ['./ui-compare-line-chart.component.scss'],
})
export class UiCompareLineChartComponent implements OnChanges {
  @Input() xKey!: string;
  @Input() yKeys!: string[];
  @Input() yNames!: string[];
  @Input() data!: any[];

  chartOptions!: AgChartOptions;

  constructor() {}

  ngOnChanges(): void {
    console.log(this.data);
    if (this.data) {
      const series: any[] = [];
      this.yKeys.map((yKey, index) => {
        series.push({
          type: 'line',
          xKey: this.xKey,
          yKey: yKey,
          yName: this.yNames[index],
        });
      });
      console.log(series);

      this.chartOptions = {
        data: this.data,

        animation: {
          enabled: true,
        },
        series: series,
      };
      console.log(this.chartOptions);
    }
  }
}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-ui-navbar',
  templateUrl: './ui-navbar.component.html',
  styleUrls: ['./ui-navbar.component.scss'],
})
export class UiNavbarComponent implements OnInit {
  @Input() userId!: string;
  @Output() logout: EventEmitter<void> = new EventEmitter<void>();

  constructor() {}

  navItems = [
    { label: 'Dashboard', route: '/metrics/dashboard' },
    { label: 'Visits Analytics', route: '/metrics/visits' },
    { label: 'Load Analytics', route: '/metrics/performance' },
    { label: 'Sessions Analytics', route: '/metrics/sessions' },
    { label: 'Tenant Analytics', route: '/metrics/comparison' },
    { label: 'Module Analytics', route: '/metrics/module-analytics' },
    { label: 'User Analytics', route: '/metrics/user-analytics' },
    { label: 'Reports', route: '/metrics/reports' },
  ];

  ngOnInit(): void {}

  onLogout(): void {
    this.logout.emit();
  }
}

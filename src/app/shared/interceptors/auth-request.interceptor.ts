import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import {
  LOGIN_URL,
  REGISTER_URL,
  REPORTS_GENERATE_SUMMARY,
} from 'src/app/common/apis/constants';
import { Store } from '@ngrx/store';
import { logout } from 'src/app/auth/store/auth.actions';

@Injectable()
export class AuthTokenInterceptor implements HttpInterceptor {
  constructor(
    private router: Router,
    private store: Store,
    private cookieService: CookieService
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const checkToken = this.cookieService.get('JWT_AUTH');

    if (req.url.endsWith(REPORTS_GENERATE_SUMMARY)) {
      return next.handle(req);
    }


    const cloneReq = req.clone({
      headers: req.headers.set('Authorization', `${checkToken}`),
    });

    return next.handle(cloneReq).pipe(
      catchError((error: HttpErrorResponse) => {
        console.log('Error status:', error.status);

        if (error.status === 401 || error.status === 403) {
          this.store.dispatch(logout());
          this.router.navigateByUrl('/auth/login');
        }

        throw error;
      })
    );
  }
}

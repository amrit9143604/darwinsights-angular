import {
  Component,
  Input,
  OnDestroy,
  Optional,
  Self,
  forwardRef,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
  NgControl,
} from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-ui-form-input',
  templateUrl: './ui-form-input.component.html',
  styleUrls: ['./ui-form-input.component.scss'],
  providers: [],
})
export class UiFormInputComponent implements ControlValueAccessor {
  @Input() type: string = 'text';
  @Input() placeholder: string = '';
  @Input() appUppercase: boolean = false;

  value!: string;
  onChange!: (value: string) => {};
  onTouch!: Function;


  constructor(@Optional() @Self() public ngControl: NgControl) {
    if (ngControl) {
      ngControl.valueAccessor = this;
    }
  }

  writeValue(value: any): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }


  onInputChange() {
    this.onChange(this.value);
  }


  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }
}

import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appUppercase]',
})
export class UppercaseDirective {
  @Input() bool: boolean = false;

  constructor(private renderer: Renderer2, private elementRef: ElementRef) {}

  @HostListener('input', ['$event'])
  onInput(event: any): void {
    if (!this.bool) return;
    
    const start = this.elementRef.nativeElement.selectionStart || 0;
    const end = this.elementRef.nativeElement.selectionEnd || 0;
    const value = this.elementRef.nativeElement.value;
    
    if (value !== undefined) {
      const normalizedValue = value.toUpperCase();
      this.writeValue(normalizedValue);
      this.elementRef.nativeElement.setSelectionRange(start, end);
    }
  }

  writeValue(value: any): void {
    const normalizedValue = value == null ? '' : value;
    this.renderer.setProperty(this.elementRef.nativeElement, 'value', normalizedValue);
  }
}

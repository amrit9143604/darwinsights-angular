import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appHover]'
})
export class HoverDirective {

  constructor(private el: ElementRef, private renderer: Renderer2) { }

  @HostListener('mouseenter') onMouseEnter() {
    this.hover(true);
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.hover(false);
  }

  private hover(shouldHover: boolean) {
    if (shouldHover) {
      // Apply hover styles when mouse enters
      this.renderer.setStyle(this.el.nativeElement, 'color', '#5090dc');
    } else {
      // Remove hover styles when mouse leaves
      this.renderer.removeStyle(this.el.nativeElement, 'color');
    }
  }

}

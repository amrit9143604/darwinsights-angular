import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ui-not-found',
  templateUrl: './ui-not-found.component.html',
  styleUrls: ['./ui-not-found.component.scss']
})
export class UiNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

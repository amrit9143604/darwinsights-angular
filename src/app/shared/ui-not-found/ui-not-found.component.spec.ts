import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiNotFoundComponent } from './ui-not-found.component';

describe('UiNotFoundComponent', () => {
  let component: UiNotFoundComponent;
  let fixture: ComponentFixture<UiNotFoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiNotFoundComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UiNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { SharedComponent } from './shared.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { UiNavbarComponent } from './ui-navbar/ui-navbar.component';
import { UiCardComponent } from './ui-card/ui-card.component';
import { UiMetricWrapperComponent } from './ui-metric-wrapper/ui-metric-wrapper.component';
import { UiLineChartComponent } from './ui-line-chart/ui-line-chart.component';
import { AgChartsAngular } from 'ag-charts-angular';
import { UiBarChartComponent } from './ui-bar-chart/ui-bar-chart.component';
import { UiPieChartComponent } from './ui-pie-chart/ui-pie-chart.component';
import { UiScatterChartComponent } from './ui-scatter-chart/ui-scatter-chart.component';
import { UiLoaderComponent } from './ui-loader/ui-loader.component';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { UiAreaChartComponent } from './ui-area-chart/ui-area-chart.component';
import { DropdownModule } from 'primeng/dropdown';
import { UiHeaderComponent } from './ui-header/ui-header.component';
import { UiMainWrapperComponent } from './ui-main-wrapper/ui-main-wrapper.component';
import { UiCardWrapperComponent } from './ui-card-wrapper/ui-card-wrapper.component';
import { UiMainMetricWrapperComponent } from './ui-main-metric-wrapper/ui-main-metric-wrapper.component';
import { UiTableComponent } from './ui-table/ui-table.component';
import { AgGridAngular } from 'ag-grid-angular';
import { HoverDirective } from './directives/hover.directive';
import { UiCompareBarChartComponent } from './ui-compare-bar-chart/ui-compare-bar-chart.component';
import { UiNotFoundComponent } from './ui-not-found/ui-not-found.component';
import { UppercaseDirective } from './directives/uppercase.directive';
import { UiCompareLineChartComponent } from './ui-compare-line-chart/ui-compare-line-chart.component';
import { UiCompareAreaChartComponent } from './ui-compare-area-chart/ui-compare-area-chart.component';
import { UiFormInputComponent } from './ui-form-input/ui-form-input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    SharedComponent,
    UiNavbarComponent,
    UiCardComponent,
    UiMetricWrapperComponent,
    UiLineChartComponent,
    UiBarChartComponent,
    UiPieChartComponent,
    UiScatterChartComponent,
    UiLoaderComponent,
    UiAreaChartComponent,
    UiHeaderComponent,
    UiMainWrapperComponent,
    UiCardWrapperComponent,
    UiMainMetricWrapperComponent,
    UiTableComponent,
    HoverDirective,
    UiCompareBarChartComponent,
    UiNotFoundComponent,
    UppercaseDirective,
    UiCompareLineChartComponent,
    UiCompareAreaChartComponent,
    UiFormInputComponent,
  ],
  imports: [
    CommonModule,
    SharedRoutingModule,
    BsDropdownModule.forRoot(),
    AgChartsAngular,
    AgGridAngular,
    ProgressSpinnerModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    BsDropdownModule,
    UiNavbarComponent,
    UiCardComponent,
    UiMetricWrapperComponent,
    UiLineChartComponent,
    AgChartsAngular,
    AgGridAngular,
    UiBarChartComponent,
    UiPieChartComponent,
    UiScatterChartComponent,
    UiLoaderComponent,
    UiAreaChartComponent,
    DropdownModule,
    UiHeaderComponent,
    UiMainWrapperComponent,
    UiCardWrapperComponent,
    UiMainMetricWrapperComponent,
    UiTableComponent,
    UiCompareBarChartComponent,
    UppercaseDirective,
    UiCompareLineChartComponent,
    UiCompareAreaChartComponent,
    UiFormInputComponent,
  ],
})
export class SharedModule {}

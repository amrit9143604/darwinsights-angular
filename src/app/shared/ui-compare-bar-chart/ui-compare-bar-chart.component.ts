import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { AgChartOptions } from 'ag-charts-community';


@Component({
  selector: 'app-ui-compare-bar-chart',
  templateUrl: './ui-compare-bar-chart.component.html',
  styleUrls: ['./ui-compare-bar-chart.component.scss']
})
export class UiCompareBarChartComponent implements OnChanges {

  @Input() xKey1!: string;
  @Input() yKey1!: string;
  @Input() yName1!: string;


  @Input() xKey2!: string;
  @Input() yKey2!: string;
  @Input() yName2!: string;
  @Input() data!: any[]; 

  chartOptions!: AgChartOptions;

  constructor() {}

  ngOnChanges(): void {
    if(this.data){
      this.chartOptions = {
        data: this.data,
        animation:{
          enabled: true
        },
        
        series: [
          {
            type: 'line',
            xKey: this.xKey1,
            yKey: this.yKey1,
            yName: this.yName1,
          },

          {
            type: 'line',
            xKey: this.xKey2,
            yKey: this.yKey2,
            yName: this.yName2,
          },
        ],
      };
    }
    else{
      this.chartOptions = {
        data: [],
        animation:{
          enabled: true
        },
        series: [
          {
            type: 'bar',
            xKey: this.xKey1,
            yKey: this.yKey1,
            yName: this.yName1,  
          },
          {
            type: 'bar',
            xKey: this.xKey2,
            yKey: this.yKey2,
            yName: this.yName2,  
          },
        ],
      };
    }
  }
}

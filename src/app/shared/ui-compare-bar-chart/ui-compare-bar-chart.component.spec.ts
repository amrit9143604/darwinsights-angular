import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiCompareBarChartComponent } from './ui-compare-bar-chart.component';

describe('UiCompareBarChartComponent', () => {
  let component: UiCompareBarChartComponent;
  let fixture: ComponentFixture<UiCompareBarChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiCompareBarChartComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UiCompareBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

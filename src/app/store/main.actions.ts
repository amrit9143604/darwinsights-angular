import { createAction, props } from '@ngrx/store';

export const validateUser = createAction('[Main App Component] ValidateUser', props<{value: boolean}>());

import { createReducer, on } from '@ngrx/store';
import { validateUser } from './main.actions';
import { loginSuccess, logout, registerSuccess } from '../auth/store/auth.actions';

export const initialState = {
  isLoggedIn: false,
};

export const mainReducer = createReducer(
  initialState,
  on(validateUser, (state, action) => ({
    ...state,
    isLoggedIn: action.value,
  })),

  on(loginSuccess, (state ) => ({
    ...state,
    isLoggedIn: true,
  })),

  on(registerSuccess, (state ) => ({
    ...state,
    isLoggedIn: true,
  })),

  on(logout, (state ) => ({
    ...state,
    isLoggedIn: false,
  }))
);

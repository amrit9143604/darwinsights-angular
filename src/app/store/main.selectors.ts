import { createFeatureSelector, createSelector } from "@ngrx/store";
import { AuthState } from "../common/interfaces/commonStates";

export const authFeatureSelector = createFeatureSelector<AuthState>("auth");

export const selectIsLoggedIn = createSelector(
  authFeatureSelector,
  (authState) => authState.isLoggedIn
);

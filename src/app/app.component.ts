import { Component, OnInit } from '@angular/core';
import { AppService } from './services/app.service';
import { Store, select } from '@ngrx/store';
import { validateUser } from './store/main.actions';
import { setData } from './metrics/store/metrics.actions';
import { setTheme } from 'ngx-bootstrap/utils';
import { CookieService } from 'ngx-cookie-service';
import { metricData } from './metrics/store/metrics.selectors';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  constructor(
    private store: Store,
    private cookieService: CookieService,
    
  ) {
    setTheme('bs5');

  }

  ngOnInit(): void {
    const token = this.cookieService.get('JWT_AUTH');
    const userId = this.cookieService.get('USER_ID');
    const tenant = this.cookieService.get('TENANT');
    const time = this.cookieService.get('TIME') ?? '';
    if (token && userId) {
      this.store.dispatch(validateUser({ value: true }));
      // this.store.dispatch(setData({ token, userId, tenant, time }));
    } else {
      this.store.dispatch(validateUser({ value: false }));
    }
  }
}

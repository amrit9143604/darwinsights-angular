import { NgModule, isDevMode } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MetricsRoutingModule } from './metrics-routing.module';
import { MetricsComponent } from './metrics.component';
import { StoreModule } from '@ngrx/store';
import { metricsReducer } from './store/metrics.reducers';
import { UiNavbarComponent } from '../shared/ui-navbar/ui-navbar.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [MetricsComponent],
  imports: [
    CommonModule,
    StoreModule.forFeature('metrics', metricsReducer),
    MetricsRoutingModule,
    SharedModule
  ],
})
export class MetricsModule {}

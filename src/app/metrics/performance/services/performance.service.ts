import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
  Observable,
  Subject,
  Subscription,
  catchError,
  map,
  takeUntil,
  throwError,
} from 'rxjs';
import { metricData } from '../../store/metrics.selectors';
import {
  PERFORMANCE_CARD_AVERAGE_LOAD_TIME,
  PERFORMANCE_CARD_BEST_MODULE_PAGE,
  PERFORMANCE_CARD_WORST_MODULE_PAGE,
  PERFORMANCE_CHART_LOAD_TIME_TRENDS,
  PERFORMANCE_CHART_MODULE_CLICK_LOAD_TIME_TRENDS,
  PERFORMANCE_CHART_MODULE_LOAD_TIME_TRENDS,
  PERFORMANCE_TABLE_DATA,
} from 'src/app/common/apis/constants';
import {
  CardAverageLoadTimeData,
  CardModulePage,
} from 'src/app/common/interfaces/cardStates';
import {
  LoadTimeChartData,
  ModuleClickPerformanceChartData,
  ModuleDetailedLoadChartData,
  ModuleLoadTimeChartData,
} from 'src/app/common/interfaces/chartStates';

@Injectable({
  providedIn: 'root',
})
export class PerformanceService {
  private tenantId!: string | null;
  private range!: string | null;
  private body!: { tenantId: string | null; range: string | null };
  private destroy$: Subject<void> = new Subject<void>();

  constructor(private http: HttpClient, private store: Store) {
    this.store
      .pipe(select(metricData), takeUntil(this.destroy$))
      .subscribe((state) => {
        this.tenantId = state.tenant;
        this.range = state.time;
        this.body = { tenantId: this.tenantId, range: this.range };
      });
  }
  private handleError(error: string) {
    console.error('An error occurred:', error);
    return throwError('Something went wrong');
  }

  private post(url: string): Observable<Object> {
    return this.http.post(url, this.body);
  }

  getAverageLoadTime(): Observable<CardAverageLoadTimeData> {
    return this.post(PERFORMANCE_CARD_AVERAGE_LOAD_TIME).pipe(
      map((response: any) => ({
        averageLoadTime: response.avgPerformance.averageLoadTime,
        percentageChange: response.performancePercentageChange,
      })),
      catchError(this.handleError)
    );
  }

  getBestModule(): Observable<CardModulePage> {
    return this.post(PERFORMANCE_CARD_BEST_MODULE_PAGE).pipe(
      map((response: any) => ({
        module: response.bestModulePerformanceMetrics.mostPerformingModule,
        loadTime: response.bestModulePerformanceMetrics.bestLoadTime,
      })),
      catchError(this.handleError)
    );
  }

  getWorstModule(): Observable<CardModulePage> {
    return this.post(PERFORMANCE_CARD_WORST_MODULE_PAGE).pipe(
      map((response: any) => ({
        module: response.worstModulePerformanceMetrics.worstPerformingModule,
        loadTime: response.worstModulePerformanceMetrics.worstLoadTime,
      })),
      catchError(this.handleError)
    );
  }

  getLoadTimeTrends(): Observable<LoadTimeChartData> {
    return this.post(PERFORMANCE_CHART_LOAD_TIME_TRENDS).pipe(
      map((response: any) => ({
        performanceTrends: response.performanceTrends,
      })),
      catchError(this.handleError)
    );
  }

  getModuleLoadTrends(): Observable<ModuleLoadTimeChartData> {
    return this.post(PERFORMANCE_CHART_MODULE_LOAD_TIME_TRENDS).pipe(
      map((response: any) => ({
        moduleAverageLoadTime: response.moduleAverageLoadTime,
      })),
      catchError(this.handleError)
    );
  }

  getModuleClicksLoadsTrends(): Observable<ModuleClickPerformanceChartData> {
    return this.post(PERFORMANCE_CHART_MODULE_CLICK_LOAD_TIME_TRENDS).pipe(
      map((response: any) => ({
        clickPerformanceTrends: response.clickPerformanceTrends,
      })),
      catchError(this.handleError)
    );
  }

  getModuleDetails(): Observable<ModuleDetailedLoadChartData> {
    return this.post(PERFORMANCE_TABLE_DATA).pipe(
      map((response: any) => ({
        moduleDetailedMetrics: response.moduleDetailedMetrics,
      })),
      catchError(this.handleError)
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

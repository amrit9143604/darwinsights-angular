import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Observable, Subject, Subscription, map, takeUntil, tap } from 'rxjs';
import {
  ChartDataState,
  LoadTimeChartData,
  LoadTimeData,
  ModuleClickPerformanceChartData,
  ModuleClickPerformanceData,
  ModuleDetailedLoadChartData,
  ModuleDetailedLoadData,
  ModuleDetailsTrendsData,
  ModuleLoadTimeChartData,
  ModuleLoadTimeData,
} from 'src/app/common/interfaces/chartStates';
import {
  metricOptions,
  timeArray,
  timeLabels,
} from 'src/app/common/interfaces/common';
import { PerformanceState } from 'src/app/common/interfaces/commonStates';
import { MetricsService } from '../services/metrics.service';
import { Store, select } from '@ngrx/store';
import { metricData } from '../store/metrics.selectors';
import { setTenant, setTime } from '../store/metrics.actions';
import {
  averageLoadTime,
  bestModulePage,
  loadTimeTrends,
  moduleClickPerformanceTrends,
  moduleDetailedLoadTrends,
  moduleLoadTimeTrends,
  worstModulePage,
} from './store/performance.actions';
import {
  selectAverageLoadTime,
  selectBestModule,
  selectLoadTimeTrends,
  selectModuleClickPerformanceTrends,
  selectModuleDetailedLoadTrends,
  selectModuleLoadTimeTrends,
  selectWorstModule,
} from './store/performance.selectors';

@Component({
  selector: 'app-performance',
  templateUrl: './performance.component.html',
  styleUrls: ['./performance.component.scss'],
})
export class PerformanceComponent implements OnInit {
  loadingState = {
    avgLoadTime: false,
    bestModule: false,
    worstModule: false,
    loadTimeTrends: false,
    moduleLoadTimeTrends: false,
    moduleClickPerformanceTrends: false,
    moduleDetailedLoadTrends: false,
  };

  tenants$!: Observable<metricOptions[]>;
  selectedTenant!: metricOptions;

  time!: metricOptions[];
  selectedTime!: metricOptions;

  avgLoadTime$!: Observable<number | null>;
  avgLoadTimePercentage$!: Observable<number | null>;
  bestModule$!: Observable<string | null>;
  bestModuleLoadTime$!: Observable<number | null>;
  worstModule$!: Observable<string | null>;
  worstModuleLoadTime$!: Observable<number | null>;
  loadTimeOptions$!: Observable<ChartDataState<LoadTimeChartData>>;
  moduleLoadTimeOptions$!: Observable<ChartDataState<ModuleLoadTimeChartData>>;
  clickPerformanceOptions$!: Observable<
    ChartDataState<ModuleClickPerformanceChartData>
  >;
  moduleDetailsOptions$!: Observable<
    ChartDataState<ModuleDetailedLoadChartData>
  >;

  loadChartType: boolean = true;
  private destroy$: Subject<void> = new Subject<void>();



  constructor(
    private store: Store<PerformanceState>,
    private metricService: MetricsService,
    private cookieService: CookieService
  ) {
    this.time = timeArray;
  }

  clickPerformanceData = [
    { headerName: 'Module', field: 'moduleName' },
    { headerName: 'Date', field: 'date' },
    { headerName: 'Page Visits', field: 'views' },
    { headerName: 'Load Time (in seconds)', field: 'avgPerformance' }
  ];
  ngOnInit(): void {
    this.store.pipe(select(metricData), takeUntil(this.destroy$)).subscribe((state) => {
      this.selectedTenant = {
        label: state.tenant ?? '',
        value: state.tenant ?? '',
      };
      this.selectedTime = {
        label: timeLabels[state.time] ?? state.time ?? '',
        value: state.time ?? '',
      };
    });

    this.fetchTenants();
    this.dispatchActions();
    this.retrieveData();
  }

  onTenantChange(value: string) {
    this.cookieService.set('TENANT', value, 2, '/');
    this.store.dispatch(setTenant({ tenant: value }));
    this.dispatchActions();
    this.retrieveData();
  }

  onTimeChange(value: string) {
    this.cookieService.set('TIME', value, 2, '/');
    this.store.dispatch(setTime({ time: value }));
    this.dispatchActions();
    this.retrieveData();
  }

  private dispatchActions(): void {
    this.store.dispatch(averageLoadTime());
    this.store.dispatch(bestModulePage());
    this.store.dispatch(worstModulePage());
    this.store.dispatch(loadTimeTrends());
    this.store.dispatch(moduleLoadTimeTrends());
    this.store.dispatch(moduleClickPerformanceTrends());
    this.store.dispatch(moduleDetailedLoadTrends());
  }

  private fetchTenants(): void {
    this.tenants$ = this.metricService.getAllTenants().pipe(
      takeUntil(this.destroy$)
    );
  }

  private retrieveData(): void {
    this.retrieveAverageLoadTime();
    this.retrieveBestModule();
    this.retreiveWorstModule();
    this.retrieveLoadTimeTrends();
    this.retrieveModuleLoadTimeTrends();
    this.retrieveModuleClickPerformanceTrends();
    this.retrieveModuleDetailedLoadTrends();
  }

  private retrieveAverageLoadTime(): void {
    this.avgLoadTime$ = this.store.pipe(
      select(selectAverageLoadTime),
      map((state) => state.data.averageLoadTime)
    );
    this.avgLoadTimePercentage$ = this.store.pipe(
      select(selectAverageLoadTime),
      map((state) => state.data.percentageChange)
    );
    this.subscribeToLoadingState('avgLoadTime', selectAverageLoadTime);
  }

  private retrieveBestModule(): void {
    this.bestModule$ = this.store.pipe(
      select(selectBestModule),
      map((state) => state.data.module)
    );
    this.bestModuleLoadTime$ = this.store.pipe(
      select(selectBestModule),
      map((state) => state.data.loadTime)
    );
    this.subscribeToLoadingState('bestModule', selectBestModule);
  }

  private retreiveWorstModule(): void {
    this.worstModule$ = this.store.pipe(
      select(selectWorstModule),
      map((state) => state.data.module)
    );

    this.worstModuleLoadTime$ = this.store.pipe(
      select(selectWorstModule),
      map((state) => state.data.loadTime)
    );
    this.subscribeToLoadingState('worstModule', selectWorstModule);
  }

  private retrieveLoadTimeTrends(): void {
    this.loadTimeOptions$ = this.store.pipe(select(selectLoadTimeTrends));

    this.subscribeToLoadingState('loadTimeTrends', selectLoadTimeTrends);
  }

  private retrieveModuleLoadTimeTrends(): void {
    this.moduleLoadTimeOptions$ = this.store.pipe(
      select(selectModuleLoadTimeTrends)
    );
    this.subscribeToLoadingState(
      'moduleLoadTimeTrends',
      selectModuleLoadTimeTrends
    );
  }

  private retrieveModuleClickPerformanceTrends(): void {
    this.clickPerformanceOptions$ = this.store.pipe(
      select(selectModuleClickPerformanceTrends)
    );
    this.subscribeToLoadingState(
      'moduleClickPerformanceTrends',
      selectModuleClickPerformanceTrends
    );
  }

  private retrieveModuleDetailedLoadTrends(): void {
    this.moduleDetailsOptions$ = this.store.pipe(
      select(selectModuleDetailedLoadTrends)
    );
    this.subscribeToLoadingState(
      'moduleDetailedLoadTrends',
      selectModuleDetailedLoadTrends
    );
  }

  handleChartType(chartType: string) {
    switch (chartType) {
      case 'avgLoadTime':
        this.loadChartType = !this.loadChartType;
        break;

      default:
        break;
    }
  }

  private subscribeToLoadingState(
    prop: keyof (typeof PerformanceComponent)['prototype']['loadingState'],
    selector: any
  ): void {
    this.store
      .pipe(
        select(selector),
        tap((state: any) => (this.loadingState[prop] = state.loading)),
        takeUntil(this.destroy$)
      )
      .subscribe();
    }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

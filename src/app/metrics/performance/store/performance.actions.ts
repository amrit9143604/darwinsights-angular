import { createAction, props } from '@ngrx/store';
import {
  LoadTimeData,
  ModuleClickPerformanceData,
  ModuleDetailedLoadData,
  ModuleLoadTimeData,
} from 'src/app/common/interfaces/chartStates';

export const averageLoadTime = createAction(
  '[Metric Performance Component] AverageLoadTime'
);
export const averageLoadTimeSuccess = createAction(
  '[Metric Performance Component] TotalViewsSuccess',
  props<{ averageLoadTime: number | null; percentageChange: number | null }>()
);
export const averageLoadTimeFailure = createAction(
  '[Metric Performance Component] TotalViewsFailure',
  props<{ error: string }>()
);

export const bestModulePage = createAction(
  '[Metric Performance Component] BestModulePage'
);
export const bestModulePageSuccess = createAction(
  '[Metric Performance Component] BestModulePageSuccess',
  props<{ module: string | null; loadTime: number | null }>()
);
export const bestModulePageFailure = createAction(
  '[Metric Performance Component] BestModulePageFailure',
  props<{ error: string }>()
);

export const worstModulePage = createAction(
  '[Metric Performance Component] WorstModulePage'
);
export const worstModulePageSuccess = createAction(
  '[Metric Performance Component] WorstModulePageSuccess',
  props<{ module: string | null; loadTime: number | null }>()
);
export const worstModulePageFailure = createAction(
  '[Metric Performance Component] WorstModulePageFailure',
  props<{ error: string }>()
);

export const loadTimeTrends = createAction(
  '[Metric Performance Component] LoadTimeTrends'
);
export const loadTimeTrendsSuccess = createAction(
  '[Metric Performance Component] LoadTimeTrendsSuccess',
  props<{ performanceTrends: LoadTimeData[] }>()
);
export const loadTimeTrendsFailure = createAction(
  '[Metric Performance Component] LoadTimeTrendsFailure',
  props<{ error: string }>()
);

export const moduleLoadTimeTrends = createAction(
  '[Metric Performance Component] ModuleLoadTimeTrends'
);
export const moduleLoadTimeTrendsSuccess = createAction(
  '[Metric Performance Component] ModuleLoadTimeTrendsSuccess',
  props<{ moduleAverageLoadTime: ModuleLoadTimeData[] }>()
);
export const moduleLoadTimeTrendsFailure = createAction(
  '[Metric Performance Component] ModuleLoadTimeTrendsFailure',
  props<{ error: string }>()
);

export const moduleClickPerformanceTrends = createAction(
  '[Metric Performance Component] ModuleClickPerformanceTrends'
);
export const moduleClickPerformanceTrendsSuccess = createAction(
  '[Metric Performance Component] ModuleClickPerformanceTrendsSuccess',
  props<{ clickPerformanceTrends: ModuleClickPerformanceData[] }>()
);
export const moduleClickPerformanceTrendsFailure = createAction(
  '[Metric Performance Component] ModuleClickPerformanceTrendsFailure',
  props<{ error: string }>()
);

export const moduleDetailedLoadTrends = createAction(
    '[Metric Performance Component] ModuleDetailedLoadTrends'
  );
  export const moduleDetailedLoadTrendsSuccess = createAction(
    '[Metric Performance Component] ModuleDetailedLoadTrendsSuccess',
    props<{ moduleDetailedMetrics: ModuleDetailedLoadData[] }>()
  );
  export const moduleDetailedLoadTrendsFailure = createAction(
    '[Metric Performance Component] ModuleDetailedLoadTrendsFailure',
    props<{ error: string }>()
  );

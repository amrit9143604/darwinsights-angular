import { createSelector, createFeatureSelector } from '@ngrx/store';
import {
  CardAverageLoadTimeData,
  CardDataState,
  CardModulePage,
  CardMostVisitedModule,
  CardUniqueUsersInMostVisitedModule,
  CardUsersData,
  CardUsersSessionsData,
  CardViewsData,
} from 'src/app/common/interfaces/cardStates';
import {
  ChartDataState,
  LoadTimeChartData,
  ModuleClickPerformanceChartData,
  ModuleDetailedLoadChartData,
  ModuleLoadTimeChartData,
} from 'src/app/common/interfaces/chartStates';
import {
  DashboardState,
  PerformanceState,
  VisitsState,
} from 'src/app/common/interfaces/commonStates';

const selectPerformanceState =
  createFeatureSelector<PerformanceState>('performance');

export const selectAverageLoadTime = createSelector(
  selectPerformanceState,
  (state: PerformanceState): CardDataState<CardAverageLoadTimeData> =>
    state.averageLoadTime
);

export const selectBestModule = createSelector(
  selectPerformanceState,
  (state: PerformanceState): CardDataState<CardModulePage> =>
    state.bestModulePage
);

export const selectWorstModule = createSelector(
  selectPerformanceState,
  (state: PerformanceState): CardDataState<CardModulePage> =>
    state.worstModulePage
);

export const selectLoadTimeTrends = createSelector(
  selectPerformanceState,
  (state: PerformanceState): ChartDataState<LoadTimeChartData> => {
    return state.loadTimeTrends;
  }
);

export const selectModuleLoadTimeTrends = createSelector(
  selectPerformanceState,
  (state: PerformanceState): ChartDataState<ModuleLoadTimeChartData> => {
    return state.moduleLoadTimeTrends;
  }
);

export const selectModuleClickPerformanceTrends = createSelector(
  selectPerformanceState,
  (state: PerformanceState): ChartDataState<ModuleClickPerformanceChartData> => {
    return state.moduleClickPerformanceTrends;
  }
);


export const selectModuleDetailedLoadTrends = createSelector(
    selectPerformanceState,
    (state: PerformanceState): ChartDataState<ModuleDetailedLoadChartData> => {
      return state.moduleDetailedLoadTrends;
    }
  );
  
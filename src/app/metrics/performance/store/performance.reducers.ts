import { createReducer, on } from '@ngrx/store';
import {
  ChartDataState,
  LoadTimeChartData,
  ModuleClickPerformanceChartData,
  ModuleDetailedLoadChartData,
  ModuleLoadTimeChartData,
} from 'src/app/common/interfaces/chartStates';
import {
  CardDataState,
  CardAverageLoadTimeData,
  CardModulePage,
} from 'src/app/common/interfaces/cardStates';
import * as PerformanceActions from './performance.actions';

interface CombinedState {
  averageLoadTime: CardDataState<CardAverageLoadTimeData>;
  bestModulePage: CardDataState<CardModulePage>;
  worstModulePage: CardDataState<CardModulePage>;
  loadTimeTrends: ChartDataState<LoadTimeChartData>;
  moduleLoadTimeTrends: ChartDataState<ModuleLoadTimeChartData>;
  moduleClickPerformanceTrends: ChartDataState<ModuleClickPerformanceChartData>;
  moduleDetailedLoadTrends: ChartDataState<ModuleDetailedLoadChartData>;
}

const initialAverageLoadTimeState: CardDataState<CardAverageLoadTimeData> = {
  data: { averageLoadTime: null, percentageChange: null },
  loading: false,
  error: null,
};
const initialBestModulePageState: CardDataState<CardModulePage> = {
  data: { module: null, loadTime: null },
  loading: false,
  error: null,
};

const initialWorstModulePageState: CardDataState<CardModulePage> = {
  data: { module: null, loadTime: null },
  loading: false,
  error: null,
};

const initialLoadTimeTrendsState: ChartDataState<LoadTimeChartData> = {
  data: { performanceTrends: [] },
  loading: false,
  error: null,
};

const initialModuleLoadTimeTrendsState: ChartDataState<ModuleLoadTimeChartData> =
  {
    data: { moduleAverageLoadTime: [] },
    loading: false,
    error: null,
  };

const initialModuleClickPerformanceTrendsState: ChartDataState<ModuleClickPerformanceChartData> =
  {
    data: { clickPerformanceTrends: [] },
    loading: false,
    error: null,
  };

const initialModuleDetailedLoadTrendsState: ChartDataState<ModuleDetailedLoadChartData> =
  {
    data: { moduleDetailedMetrics: [] },
    loading: false,
    error: null,
  };

const initialState: CombinedState = {
  averageLoadTime: initialAverageLoadTimeState,
  bestModulePage: initialBestModulePageState,
  worstModulePage: initialWorstModulePageState,
  loadTimeTrends: initialLoadTimeTrendsState,
  moduleLoadTimeTrends: initialModuleLoadTimeTrendsState,
  moduleClickPerformanceTrends: initialModuleClickPerformanceTrendsState,
  moduleDetailedLoadTrends: initialModuleDetailedLoadTrendsState,
};

export const performanceReducer = createReducer(
  initialState,
  on(PerformanceActions.averageLoadTime, (state) => ({
    ...state,
    averageLoadTime: { ...state.averageLoadTime, loading: true },
  })),
  on(
    PerformanceActions.averageLoadTimeSuccess,
    (state, { averageLoadTime, percentageChange }) => {
      return {
        ...state,
        averageLoadTime: {
          ...state.averageLoadTime,
          loading: false,
          data: { averageLoadTime, percentageChange },
        },
      };
    }
  ),
  on(PerformanceActions.averageLoadTimeFailure, (state, { error }) => ({
    ...state,
    averageLoadTime: {
      data: { averageLoadTime: null, percentageChange: null },
      loading: false,
      error,
    },
  })),

  on(PerformanceActions.bestModulePage, (state) => ({
    ...state,
    bestModulePage: { ...state.bestModulePage, loading: true },
  })),
  on(
    PerformanceActions.bestModulePageSuccess,
    (state, { module, loadTime }) => {
      return {
        ...state,
        bestModulePage: {
          ...state.bestModulePage,
          loading: false,
          data: { module, loadTime },
        },
      };
    }
  ),
  on(PerformanceActions.bestModulePageFailure, (state, { error }) => ({
    ...state,
    averageLoadTime: {
      data: { averageLoadTime: null, percentageChange: null },
      loading: false,
      error,
    },
  })),

  on(PerformanceActions.worstModulePage, (state) => ({
    ...state,
    worstModulePage: { ...state.worstModulePage, loading: true },
  })),
  on(
    PerformanceActions.worstModulePageSuccess,
    (state, { module, loadTime }) => {
      return {
        ...state,
        worstModulePage: {
          ...state.worstModulePage,
          loading: false,
          data: { module, loadTime },
        },
      };
    }
  ),
  on(PerformanceActions.worstModulePageFailure, (state, { error }) => ({
    ...state,
    worstModulePage: {
      data: { loadTime: null, module: null },
      loading: false,
      error,
    },
  })),

  on(PerformanceActions.loadTimeTrends, (state) => ({
    ...state,
    loadTimeTrends: { ...state.loadTimeTrends, loading: true },
  })),
  on(
    PerformanceActions.loadTimeTrendsSuccess,
    (state, { performanceTrends }) => ({
      ...state,
      loadTimeTrends: {
        ...state.loadTimeTrends,
        loading: false,
        data: { performanceTrends },
      },
    })
  ),
  on(PerformanceActions.loadTimeTrendsFailure, (state, { error }) => ({
    ...state,
    loadTimeTrends: { data: { performanceTrends: [] }, loading: false, error },
  })),


  on(PerformanceActions.moduleLoadTimeTrends, (state) => ({
    ...state,
    moduleLoadTimeTrends: { ...state.moduleLoadTimeTrends, loading: true },
  })),
  on(
    PerformanceActions.moduleLoadTimeTrendsSuccess,
    (state, { moduleAverageLoadTime }) => ({
      ...state,
      moduleLoadTimeTrends: {
        ...state.moduleLoadTimeTrends,
        loading: false,
        data: { moduleAverageLoadTime },
      },
    })
  ),
  on(PerformanceActions.moduleLoadTimeTrendsFailure, (state, { error }) => ({
    ...state,
    moduleLoadTimeTrends: {
      data: { moduleAverageLoadTime: [] },

      loading: false,
      error,
    },
  })),

  on(PerformanceActions.moduleClickPerformanceTrends, (state) => ({
    ...state,
    moduleClickPerformanceTrends: {
      ...state.moduleClickPerformanceTrends,
      loading: true,
    },
  })),
  on(
    PerformanceActions.moduleClickPerformanceTrendsSuccess,
    (state, { clickPerformanceTrends }) => ({
      ...state,
      moduleClickPerformanceTrends: {
        ...state.moduleClickPerformanceTrends,
        loading: false,
        data: { clickPerformanceTrends },
      },
    })
  ),
  on(
    PerformanceActions.moduleClickPerformanceTrendsFailure,
    (state, { error }) => ({
      ...state,
      moduleClickPerformanceTrends: {
        data: { clickPerformanceTrends: [] },
        loading: false,
        error,
      },
    })
  ),
  on(PerformanceActions.moduleDetailedLoadTrends, (state) => ({
    ...state,
    moduleDetailedLoadTrends: {
      ...state.moduleDetailedLoadTrends,
      loading: true,
    },
  })),
  on(
    PerformanceActions.moduleDetailedLoadTrendsSuccess,
    (state, { moduleDetailedMetrics }) => ({
      ...state,
      moduleDetailedLoadTrends: {
        ...state.moduleDetailedLoadTrends,
        loading: false,
        data: { moduleDetailedMetrics },
      },
    })
  ),
  on(
    PerformanceActions.moduleDetailedLoadTrendsFailure,
    (state, { error }) => ({
      ...state,
      moduleDetailedLoadTrends: {
        data: { moduleDetailedMetrics: [] },
        loading: false,
        error,
      },
    })
  )
);

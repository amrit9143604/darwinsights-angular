import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { map, switchMap, catchError, mergeMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import * as PerformanceActions from './performance.actions';
import { Action } from '@ngrx/store';
import { PerformanceService } from '../services/performance.service';

@Injectable()
export class PerformanceEffects {
  constructor(
    private actions$: Actions,
    private performanceService: PerformanceService
  ) {}

  createEffectForAction$ = (
    actionType: string,
    serviceFn: () => Observable<any>,
    successFn: (payload: any) => Action,
    failureFn: (payload: any) => Action
  ): Observable<Action> =>
    createEffect(() =>
      this.actions$.pipe(
        ofType(actionType),
        switchMap(() =>
          serviceFn().pipe(
            map(successFn),
            catchError((error) => of(failureFn({ error })))
          )
        )
      )
    );

  averageLoadTime$ = this.createEffectForAction$(
    PerformanceActions.averageLoadTime.type,
    this.performanceService.getAverageLoadTime.bind(this.performanceService),
    (payload) => PerformanceActions.averageLoadTimeSuccess(payload),
    (payload) => PerformanceActions.averageLoadTimeFailure(payload)
  );

  bestModule$ = this.createEffectForAction$(
    PerformanceActions.bestModulePage.type,
    this.performanceService.getBestModule.bind(this.performanceService),
    (payload) => PerformanceActions.bestModulePageSuccess(payload),
    (payload) => PerformanceActions.bestModulePageFailure(payload)
  );


  worstModule$ = this.createEffectForAction$(
    PerformanceActions.worstModulePage.type,
    this.performanceService.getWorstModule.bind(this.performanceService),
    (payload) => PerformanceActions.worstModulePageSuccess(payload),
    (payload) => PerformanceActions.worstModulePageFailure(payload)
  );

  loadTimeTrends$ = this.createEffectForAction$(
    PerformanceActions.loadTimeTrends.type,
    this.performanceService.getLoadTimeTrends.bind(this.performanceService),
    (payload) => PerformanceActions.loadTimeTrendsSuccess(payload),
    (payload) => PerformanceActions.loadTimeTrendsFailure(payload)
  );

  moduleLoadTimeTrends$ = this.createEffectForAction$(
    PerformanceActions.moduleLoadTimeTrends.type,
    this.performanceService.getModuleLoadTrends.bind(this.performanceService),
    (payload) => PerformanceActions.moduleLoadTimeTrendsSuccess(payload),
    (payload) => PerformanceActions.moduleLoadTimeTrendsFailure(payload)
  );

  moduleClickPerformanceTrends$ = this.createEffectForAction$(
    PerformanceActions.moduleClickPerformanceTrends.type,
    this.performanceService.getModuleClicksLoadsTrends.bind(this.performanceService),
    (payload) => PerformanceActions.moduleClickPerformanceTrendsSuccess(payload),
    (payload) => PerformanceActions.moduleClickPerformanceTrendsFailure(payload)
  );

  
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerformanceRoutingModule } from './performance-routing.module';
import { PerformanceComponent } from './performance.component';
import { PerformanceEffects } from './store/performance.effects';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { performanceReducer } from './store/performance.reducers';
import { EffectsModule } from '@ngrx/effects';


@NgModule({
  declarations: [
    PerformanceComponent
  ],
  imports: [
    CommonModule,
    PerformanceRoutingModule,
    SharedModule,
    FormsModule,
    DropdownModule,
    StoreModule.forFeature('performance', performanceReducer),
    EffectsModule.forFeature([PerformanceEffects]),
  ]
})
export class PerformanceModule { }

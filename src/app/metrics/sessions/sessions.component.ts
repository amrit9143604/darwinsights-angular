import { Component, OnInit } from '@angular/core';
import { SessionsState } from 'src/app/common/interfaces/commonStates';
import { MetricsService } from '../services/metrics.service';
import { Store, select } from '@ngrx/store';
import { CookieService } from 'ngx-cookie-service';
import {
  metricOptions,
  timeArray,
  timeLabels,
} from 'src/app/common/interfaces/common';
import { Observable, Subject, Subscription, map, takeUntil, tap } from 'rxjs';
import {
  ChartDataState,
  SessionTimeChartData,
  ModuleSessionChartData,
} from 'src/app/common/interfaces/chartStates';
import { setTenant, setTime } from '../store/metrics.actions';
import { metricData } from '../store/metrics.selectors';
import {
  usersSessionTime,
  sessionTimeTrends,
  moduleSessionTrends,
  bestWorstModule,
} from './store/sessions.actions';
import {
  selectUsersSessionTime,
  selectSessionTimeTrends,
  selectModuleSessionTrends,
  selectBestWorstModule,
} from './store/sessions.selectors';

@Component({
  selector: 'app-sessions',
  templateUrl: './sessions.component.html',
  styleUrls: ['./sessions.component.scss'],
})
export class SessionsComponent implements OnInit {
  loadingState = {
    session: false,
    sessionTimeTrends: false,
    moduleSessionTrends: false,
    usersSessionTime: false,
    highestUsedModule: false,
    lowestUsedModule: false,
  };

  tenants$!: Observable<metricOptions[]>;
  selectedTenant!: metricOptions;

  time!: metricOptions[];
  selectedTime!: metricOptions;

  usersSessionTime$!: Observable<number | null>;
  usersSessionPercentage$!: Observable<number | null>;
  highestUsedModule$!: Observable<string | null>;
  lowestUsedModule$!: Observable<string | null>;
  highestUsage$!: Observable<string | null>;
  lowestUsage$!: Observable<string | null>;
  sessionTimeOptions$!: Observable<ChartDataState<SessionTimeChartData>>;
  moduleSessionOptions$!: Observable<ChartDataState<ModuleSessionChartData>>;

  sessionTimeChartType: boolean = true;
  private destroy$: Subject<void> = new Subject<void>();


  constructor(
    private store: Store<SessionsState>,
    private metricService: MetricsService,
    private cookieService: CookieService
  ) {
    this.time = timeArray;
  }

  ngOnInit(): void {
    this.store.pipe(select(metricData), takeUntil(this.destroy$)).subscribe((state) => {
      this.selectedTenant = {
        label: state.tenant ?? '',
        value: state.tenant ?? '',
      };
      this.selectedTime = {
        label: timeLabels[state.time] ?? state.time ?? '',
        value: state.time ?? '',
      };
    });

    this.fetchTenants();
    this.dispatchActions();
    this.retrieveData();
  }

  onTenantChange(value: string) {
    this.cookieService.set('TENANT', value, 2, '/');
    this.store.dispatch(setTenant({ tenant: value }));
    this.dispatchActions();
    this.retrieveData();
  }

  onTimeChange(value: string) {
    this.cookieService.set('TIME', value, 2, '/');
    this.store.dispatch(setTime({ time: value }));
    this.dispatchActions();
    this.retrieveData();
  }

  private dispatchActions(): void {
    this.store.dispatch(usersSessionTime());
    this.store.dispatch(sessionTimeTrends());
    this.store.dispatch(moduleSessionTrends());
    this.store.dispatch(bestWorstModule());
  }

  private fetchTenants(): void {
    this.tenants$ = this.metricService.getAllTenants().pipe(
      takeUntil(this.destroy$)
    );
  }

  private retrieveData(): void {
    this.retrieveUsersSessionTime();
    this.retrieveSessionTimeTrends();
    this.retrieveModuleSessionTrends();
    this.retrieveBestWorstModule();
  }

  private retrieveUsersSessionTime(): void {
    this.usersSessionTime$ = this.store.pipe(
      select(selectUsersSessionTime),
      map((state) => state.data.averageSessionTime)
    );
    this.usersSessionPercentage$ = this.store.pipe(
      select(selectUsersSessionTime),
      map((state) => state.data.sessionPercentageChange)
    );
    this.subscribeToLoadingState('session', selectUsersSessionTime);
  }
  private retrieveSessionTimeTrends(): void {
    this.sessionTimeOptions$ = this.store.pipe(select(selectSessionTimeTrends));
    this.subscribeToLoadingState('sessionTimeTrends', selectSessionTimeTrends);
  }

  private retrieveBestWorstModule(): void {
    this.highestUsedModule$ = this.store.pipe(
      select(selectBestWorstModule),
      map((state) => state.data.highestUsedModule)
    );

    this.lowestUsedModule$ = this.store.pipe(
      select(selectBestWorstModule),
      map((state) => state.data.lowestUsedModule)
    );

    this.highestUsage$ = this.store.pipe(
      select(selectBestWorstModule),
      map((state) => state.data.highestUsage)
    );

    this.lowestUsage$ = this.store.pipe(
      select(selectBestWorstModule),
      map((state) => state.data.lowestUsage)
    );

    this.subscribeToLoadingState('session', selectUsersSessionTime);
  }

  private retrieveModuleSessionTrends(): void {
    this.moduleSessionOptions$ = this.store.pipe(
      select(selectModuleSessionTrends)
    );
    this.subscribeToLoadingState(
      'moduleSessionTrends',
      selectModuleSessionTrends
    );
  }

  handleChartType(chartType: string) {
    switch (chartType) {
      case 'sessionTime':
        this.sessionTimeChartType = !this.sessionTimeChartType;
        break;
      default:
        break;
    }
  }

  private subscribeToLoadingState(
    prop: keyof (typeof SessionsComponent)['prototype']['loadingState'],
    selector: any
  ): void {
    this.store
      .pipe(
        select(selector),
        tap((state: any) => (this.loadingState[prop] = state.loading)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

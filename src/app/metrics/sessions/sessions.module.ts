import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SessionsRoutingModule } from './sessions-routing.module';
import { SessionsComponent } from './sessions.component';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { sessionReducer } from './store/sessions.reducers';
import { SessionEffects } from './store/sessions.effects';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    SessionsComponent
  ],
  imports: [
    CommonModule,
    SessionsRoutingModule,
    SharedModule,
    FormsModule,
    DropdownModule,
    StoreModule.forFeature('sessions', sessionReducer),
    EffectsModule.forFeature([SessionEffects]),
  ]
})
export class SessionsModule { }

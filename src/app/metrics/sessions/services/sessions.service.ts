import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { metricData } from '../../store/metrics.selectors';
import {
  Observable,
  Subject,
  Subscription,
  catchError,
  map,
  takeUntil,
  throwError,
} from 'rxjs';
import {
  SESSIONS_CARD_AVERAGE_SESSION_TIME,
  SESSIONS_CARD_MODULE_DATA,
  SESSIONS_CHART_MODULE_SESSION_TIME_TRENDS,
  SESSIONS_CHART_SESSION_TIME_TRENDS,
  SESSIONS_TABLE_DATA,
} from 'src/app/common/apis/constants';
import {
  CardModuleBestWorstData,
  CardModulePage,
  CardUsersSessionsData,
} from 'src/app/common/interfaces/cardStates';
import {
  ModuleSessionChartData,
  SessionTimeChartData,
} from 'src/app/common/interfaces/chartStates';

@Injectable({
  providedIn: 'root',
})
export class SessionsService {
  private tenantId!: string | null;
  private range!: string | null;
  private body!: { tenantId: string | null; range: string | null };
  private destroy$: Subject<void> = new Subject<void>();

  constructor(private http: HttpClient, private store: Store) {
    this.store.pipe(select(metricData), takeUntil(this.destroy$)).subscribe((state) => {
      this.tenantId = state.tenant;
      this.range = state.time;
      this.body = { tenantId: this.tenantId, range: this.range };
    });
  }

  private handleError(error: string) {
    console.error('An error occurred:', error);
    return throwError('Something went wrong');
  }

  private post(url: string): Observable<Object> {
    return this.http.post(url, this.body);
  }

  getUsersSessionTime(): Observable<CardUsersSessionsData> {
    return this.post(SESSIONS_CARD_AVERAGE_SESSION_TIME).pipe(
      map((response: any) => ({
        averageSessionTime: response.averageSessionTime,
        sessionPercentageChange: response.sessionPercentageChange,
      })),
      catchError(this.handleError)
    );
  }

  getModuleData(): Observable<CardModuleBestWorstData> {
    return this.post(SESSIONS_CARD_MODULE_DATA).pipe(
      map((response: any) => ({
        highestUsedModule: response.bestWorstModules.highestUsedModule,
        highestUsage: response.bestWorstModules.highestUsage,
        lowestUsedModule: response.bestWorstModules.lowestUsedModule,
        lowestUsage: response.bestWorstModules.lowestUsage,
      })),
      catchError(this.handleError)
    );
  }

  getSessionTimeTrends(): Observable<SessionTimeChartData> {
    return this.post(SESSIONS_CHART_SESSION_TIME_TRENDS).pipe(
      map((response: any) => ({
        sessionTrends: response.sessionTrends,
      })),
      catchError(this.handleError)
    );
  }

  getModuleSessionTrends(): Observable<ModuleSessionChartData> {
    return this.post(SESSIONS_CHART_MODULE_SESSION_TIME_TRENDS).pipe(
      map((response: any) => ({
        result: response.moduleSessionsTime.result,
      })),
      catchError(this.handleError)
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

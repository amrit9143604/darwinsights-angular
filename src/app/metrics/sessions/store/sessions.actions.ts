import { createAction, props } from '@ngrx/store';
import {
  BrowserData,
  LoadTimeData,
  ModuleSessionData,
  PlatformData,
  SessionData,
  UsersData,
  VisitsData,
} from 'src/app/common/interfaces/chartStates';

export const usersSessionTime = createAction(
  '[Metric Sessions Component] UsersSessionTime'
);
export const usersSessionTimeSuccess = createAction(
  '[Metric Sessions Component] UsersSessionTimeSuccess',
  props<{
    averageSessionTime: number | null;
    sessionPercentageChange: number | null;
  }>()
);
export const usersSessionTimeFailure = createAction(
  '[Metric Sessions Component] UsersSessionTimeFailure',
  props<{ error: string }>()
);

export const bestWorstModule = createAction(
  '[Metric Sessions Component] BestWorstModule'
);
export const bestWorstModuleSuccess = createAction(
  '[Metric Sessions Component] BestWorstModuleSuccess',
  props<{
    highestUsedModule: string | null;
    lowestUsedModule: string | null;
    highestUsage: string | null;
    lowestUsage: string | null;
  }>()
);
export const bestWorstModuleFailure = createAction(
  '[Metric Sessions Component] BestWorstModuleFailure',
  props<{ error: string }>()
);

export const sessionTimeTrends = createAction(
  '[Metric Sessions Component] SessionTimeTrends'
);
export const sessionTimeTrendsSuccess = createAction(
  '[Metric Sessions Component] SessionTimeTrendsSuccess',
  props<{ sessionTrends: SessionData[] }>()
);
export const sessionTimeTrendsFailure = createAction(
  '[Metric Sessions Component] SessionTimeTrendsFailure',
  props<{ error: string }>()
);

export const moduleSessionTrends = createAction(
  '[Metric Sessions Component] ModuleSessionTrends'
);
export const moduleSessionTrendsSuccess = createAction(
  '[Metric Sessions Component] ModuleSessionTrendsSuccess',
  props<{ result: ModuleSessionData[] }>()
);
export const moduleSessionTrendsFailure = createAction(
  '[Metric Sessions Component] ModuleSessionTrendsFailure',
  props<{ error: string }>()
);

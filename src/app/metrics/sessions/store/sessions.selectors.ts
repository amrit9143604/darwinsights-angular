import { createSelector, createFeatureSelector } from '@ngrx/store';
import { CardAverageLoadTimeData, CardDataState, CardModuleBestWorstData, CardUsersData, CardUsersSessionsData, CardViewsData } from 'src/app/common/interfaces/cardStates';
import { BrowserChartData, ChartDataState, LoadTimeChartData, ModuleSessionChartData, PlatformChartData, SessionTimeChartData, UniqueUsersChartData, VisitsChartData } from 'src/app/common/interfaces/chartStates';
import { DashboardState, SessionsState } from 'src/app/common/interfaces/commonStates';


const selectSessionsState = createFeatureSelector<SessionsState>('sessions');


export const selectUsersSessionTime = createSelector(
    selectSessionsState,
  (state: SessionsState): CardDataState<CardUsersSessionsData> => state.usersSessionTime
);


export const selectSessionTimeTrends = createSelector(
    selectSessionsState,
  (state: SessionsState): ChartDataState<SessionTimeChartData> => {
    // console.log('Session Time Trends State:', state.sessionTrends);
    return state.sessionTrends;
  }
);

export const selectBestWorstModule = createSelector(
  selectSessionsState,
(state: SessionsState): ChartDataState<CardModuleBestWorstData> => {
  // console.log('Session Time Trends State:', state.sessionTrends);
  return state.bestWorstModule;
}
);

export const selectModuleSessionTrends = createSelector(
    selectSessionsState,
  (state: SessionsState): ChartDataState<ModuleSessionChartData> => {
    return state.moduleSessionTrends;
  }
);


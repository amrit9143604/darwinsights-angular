import { createReducer, on } from '@ngrx/store';
import {
  BrowserChartData,
  ChartDataState,
  LoadTimeChartData,
  ModuleSessionChartData,
  PlatformChartData,
  SessionTimeChartData,
  UniqueUsersChartData,
  VisitsChartData,
} from 'src/app/common/interfaces/chartStates';
import {
  CardDataState,
  CardAverageLoadTimeData,
  CardUsersData,
  CardUsersSessionsData,
  CardViewsData,
  CardModuleBestWorstData,
} from 'src/app/common/interfaces/cardStates';
import * as SessionsActions from './sessions.actions';

interface CombinedState {
  usersSessionTime: CardDataState<CardUsersSessionsData>;
  sessionTrends: ChartDataState<SessionTimeChartData>;
  moduleSessionTrends: ChartDataState<ModuleSessionChartData>;
  bestWorstModule: CardDataState<CardModuleBestWorstData>;
}

const initialUserSessionsState: CardDataState<CardUsersSessionsData> = {
  data: { averageSessionTime: null, sessionPercentageChange: null },
  loading: false,
  error: null,
};

const initialBestWorstModuleState: CardDataState<CardModuleBestWorstData> = {
  data: {
    highestUsedModule: '',
    lowestUsedModule: '',
    highestUsage: '',
    lowestUsage: '',
  },
  loading: false,
  error: null,
};

const initialSessionTimeTrendsState: ChartDataState<SessionTimeChartData> = {
  data: { sessionTrends: [] },
  loading: false,
  error: null,
};

const initialModuleSessionTrendsState: ChartDataState<ModuleSessionChartData> =
  {
    data: { result: [] },
    loading: false,
    error: null,
  };

const initialState: CombinedState = {
  usersSessionTime: initialUserSessionsState,
  sessionTrends: initialSessionTimeTrendsState,
  moduleSessionTrends: initialModuleSessionTrendsState,
  bestWorstModule: initialBestWorstModuleState,
};

export const sessionReducer = createReducer(
  initialState,

  on(SessionsActions.usersSessionTime, (state) => ({
    ...state,
    usersSessionTime: { ...state.usersSessionTime, loading: true },
  })),
  on(
    SessionsActions.usersSessionTimeSuccess,
    (state, { averageSessionTime, sessionPercentageChange }) => ({
      ...state,
      usersSessionTime: {
        ...state.usersSessionTime,
        loading: false,
        data: { averageSessionTime, sessionPercentageChange },
      },
    })
  ),
  on(SessionsActions.usersSessionTimeFailure, (state, { error }) => ({
    ...state,
    usersSessionTime: {
      data: { averageSessionTime: null, sessionPercentageChange: null },
      loading: false,
      error,
    },
  })),

  on(SessionsActions.bestWorstModule, (state) => ({
    ...state,
    bestWorstModule: { ...state.bestWorstModule, loading: true },
  })),
  on(
    SessionsActions.bestWorstModuleSuccess,
    (
      state,
      { highestUsedModule, highestUsage, lowestUsedModule, lowestUsage }
    ) => ({
      ...state,
      bestWorstModule: {
        ...state.bestWorstModule,
        loading: false,
        data: {
          highestUsedModule: highestUsedModule ?? '',
          highestUsage: highestUsage ?? '',
          lowestUsedModule: lowestUsedModule ?? '',
          lowestUsage: lowestUsage ?? '',
        },
      },
    })
  ),
  on(SessionsActions.bestWorstModuleFailure, (state, { error }) => ({
    ...state,
    bestWorstModule: {
      data: {
        highestUsage: '',
        highestUsedModule: '',
        lowestUsage: '',
        lowestUsedModule: '',
      },
      loading: false,
      error,
    },
  })),

  on(SessionsActions.sessionTimeTrends, (state) => ({
    ...state,
    sessionTrends: { ...state.sessionTrends, loading: true },
  })),
  on(SessionsActions.sessionTimeTrendsSuccess, (state, { sessionTrends }) => ({
    ...state,
    sessionTrends: {
      ...state.sessionTrends,
      loading: false,
      data: { sessionTrends },
    },
  })),
  on(SessionsActions.sessionTimeTrendsFailure, (state, { error }) => ({
    ...state,
    sessionTrends: { data: { sessionTrends: [] }, loading: false, error },
  })),

  on(SessionsActions.moduleSessionTrends, (state) => ({
    ...state,
    moduleSessionTrends: { ...state.moduleSessionTrends, loading: true },
  })),
  on(SessionsActions.moduleSessionTrendsSuccess, (state, { result }) => ({
    ...state,
    moduleSessionTrends: {
      ...state.moduleSessionTrends,
      loading: false,
      data: { result },
    },
  })),
  on(SessionsActions.moduleSessionTrendsFailure, (state, { error }) => ({
    ...state,
    moduleSessionTrends: {
      data: { result: [] },
      loading: false,
      error,
    },
  }))
);

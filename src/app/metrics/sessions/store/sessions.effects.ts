import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { map, switchMap, catchError, mergeMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import * as SessionsActions from './sessions.actions';
import { Action } from '@ngrx/store';
import { SessionsService } from '../services/sessions.service';

@Injectable()
export class SessionEffects {
  constructor(
    private actions$: Actions,
    private sessionService: SessionsService
  ) {}

  createEffectForAction$ = (
    actionType: string,
    serviceFn: () => Observable<any>,
    successFn: (payload: any) => Action,
    failureFn: (payload: any) => Action
  ): Observable<Action> =>
    createEffect(() =>
      this.actions$.pipe(
        ofType(actionType),
        switchMap(() =>
          serviceFn().pipe(
            map(successFn),
            catchError((error) => of(failureFn({ error })))
          )
        )
      )
    );

  usersSessionTime$ = this.createEffectForAction$(
    SessionsActions.usersSessionTime.type,
    this.sessionService.getUsersSessionTime.bind(this.sessionService),
    (payload) => SessionsActions.usersSessionTimeSuccess(payload),
    (payload) => SessionsActions.usersSessionTimeFailure(payload)
  );

  bestWorstModule$ = this.createEffectForAction$(
    SessionsActions.bestWorstModule.type,
    this.sessionService.getModuleData.bind(this.sessionService),
    (payload) => SessionsActions.bestWorstModuleSuccess(payload),
    (payload) => SessionsActions.bestWorstModuleFailure(payload)
  );

  sessionTimeTrends$ = this.createEffectForAction$(
    SessionsActions.sessionTimeTrends.type,
    this.sessionService.getSessionTimeTrends.bind(this.sessionService),
    (payload) => SessionsActions.sessionTimeTrendsSuccess(payload),
    (payload) => SessionsActions.sessionTimeTrendsFailure(payload)
  );

  moduleSessionTrends$ = this.createEffectForAction$(
    SessionsActions.moduleSessionTrends.type,
    this.sessionService.getModuleSessionTrends.bind(this.sessionService),
    (payload) => SessionsActions.moduleSessionTrendsSuccess(payload),
    (payload) => SessionsActions.moduleSessionTrendsFailure(payload)
  );
}

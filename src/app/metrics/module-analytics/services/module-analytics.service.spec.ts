import { TestBed } from '@angular/core/testing';

import { ModuleAnalyticsService } from './module-analytics.service';

describe('ModuleAnalyticsService', () => {
  let service: ModuleAnalyticsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModuleAnalyticsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { metricData } from '../../store/metrics.selectors';
import {
  Observable,
  Subject,
  Subscription,
  catchError,
  map,
  takeUntil,
  tap,
  throwError,
} from 'rxjs';
import {
  MODULE_CARD_AVERAGE_LOAD_TIME,
  MODULE_CARD_AVERAGE_SESSION_TIME,
  MODULE_CARD_TOTAL_VIEWS,
  MODULE_CARD_UNIQUE_USERS,
  MODULE_CHART_PERFORMANCE_TRENDS,
  MODULE_CHART_PLATFORM_COUNT,
  MODULE_CHART_SESSION_TRENDS,
  MODULE_CHART_USERS_TRENDS,
  MODULE_CHART_VISITS_TRENDS,
  OVERVIEW_ALL_CLIENTS,
  OVERVIEW_ALL_MODULES,
} from 'src/app/common/apis/constants';
import { metricOptions } from 'src/app/common/interfaces/common';
import { CardAverageLoadTimeData, CardUsersData, CardUsersSessionsData, CardViewsData } from 'src/app/common/interfaces/cardStates';
import { LoadTimeChartData, ModuleSessionChartData, ModuleSessionTimeChartData, PlatformChartData, SessionTimeChartData, UniqueUsersChartData, VisitsChartData } from 'src/app/common/interfaces/chartStates';

@Injectable({
  providedIn: 'root',
})
export class ModuleAnalyticsService {
  private tenantId!: string | null;
  private range!: string | null;
  private moduleName!: string | null;
  private body!: {
    tenantId: string | null;
    range: string | null;
    moduleName: string | null;
  };
  private destroy$: Subject<void> = new Subject<void>();


  constructor(private http: HttpClient, private store: Store) {
    this.store
      .pipe(select(metricData), takeUntil(this.destroy$))
      .subscribe((state) => {
        this.tenantId = state.tenant;
        this.range = state.time;
        this.moduleName = state.module;
        this.body = {
          tenantId: this.tenantId,
          range: this.range,
          moduleName: this.moduleName,
        };
      });

  }

  private handleError(error: string) {
    console.error('An error occurred:', error);
    return throwError('Something went wrong');
  }

  private post(url: string): Observable<Object> {
    return this.http.post(url, this.body);
  }

  getAllModules(): Observable<metricOptions[]> {
    return this.post(OVERVIEW_ALL_MODULES).pipe(
      // tap((response: any) => console.log('Response from getAllTenants:', response)),
      map((response: any) =>
        response.map((item: any) => ({
          label: item ? item.toUpperCase() : null,
          value: item ? item : null,
        }))
      ),
      // tap((response: any) => console.log('Response from getAllTenants:', response)),
      catchError(this.handleError)
    );
  }

  getTotalViews(): Observable<CardViewsData> {
    return this.post(MODULE_CARD_TOTAL_VIEWS).pipe(
      map((response: any) => ({
        totalViews: response.totalVisits,
        percentageChange: response.totalVisitsPercentageChange,
      })),
      catchError(this.handleError)
    );
  }

  getUniqueUsers(): Observable<CardUsersData> {
    return this.post(MODULE_CARD_UNIQUE_USERS).pipe(
      map((response: any) => ({
        totalUsers: response.uniqueUsers,
        percentageChange: response.UniqueUsersPercentageChange,
      })),
      catchError(this.handleError)
    );
  }

  getAverageLoadTime(): Observable<CardAverageLoadTimeData> {
    return this.post(MODULE_CARD_AVERAGE_LOAD_TIME).pipe(
      map((response: any) => ({
        averageLoadTime: response.averageLoadTime,
        percentageChange: response.averageLoadTimePercentageChange,
      })),
      catchError(this.handleError)
    );
  }

  getUsersSessionTime(): Observable<CardUsersSessionsData> {
    return this.post(MODULE_CARD_AVERAGE_SESSION_TIME).pipe(
      map((response: any) => ({
        averageSessionTime: response.moduleAvgSessionTime.averageSessionTime,
        sessionPercentageChange: response.moduleAvgSessionTimePercentageChange,
      })),
      catchError(this.handleError)
    );
  }

  getVisitsTrends(): Observable<VisitsChartData> {
    return this.post(MODULE_CHART_VISITS_TRENDS).pipe(
      map((response: any) => ({
        clickTrends: response.clickTrends,
      })),
      catchError(this.handleError)
    );
  }

  getUniqueUsersTrends(): Observable<UniqueUsersChartData> {
    return this.post(MODULE_CHART_USERS_TRENDS).pipe(
      map((response: any) => ({
        userTrends: response.userTrends,
      })),
      catchError(this.handleError)
    );
  }

  getLoadTimeTrends(): Observable<LoadTimeChartData> {
    return this.post(MODULE_CHART_PERFORMANCE_TRENDS).pipe(
      map((response: any) => ({
        performanceTrends: response.performanceTrends,
      })),
      catchError(this.handleError)
    );
  }

  getSessionTimeTrends(): Observable<ModuleSessionTimeChartData> {
    return this.post(MODULE_CHART_SESSION_TRENDS).pipe(
      map((response: any) => ({
        moduleSessionTrends: response.moduleSessionTrends,
      })),
      catchError(this.handleError)
    );
  }

  getPlatformTrends(): Observable<PlatformChartData> {
    return this.post(MODULE_CHART_PLATFORM_COUNT).pipe(
      map((response: any) => ({
        result: response.platformCount.result,
      })),
      catchError(this.handleError)
    );
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

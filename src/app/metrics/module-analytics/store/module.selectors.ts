import { createSelector, createFeatureSelector } from '@ngrx/store';
import { CardAverageLoadTimeData, CardDataState, CardUsersData, CardUsersSessionsData, CardViewsData } from 'src/app/common/interfaces/cardStates';
import { BrowserChartData, ChartDataState, LoadTimeChartData, ModuleSessionChartData, ModuleSessionTimeChartData, ModuleVisitsChartData, PlatformChartData, SessionTimeChartData, UniqueUsersChartData } from 'src/app/common/interfaces/chartStates';
import { ModuleAnalyticsState } from 'src/app/common/interfaces/commonStates';


const selectModuleAnalyticsState = createFeatureSelector<ModuleAnalyticsState>('moduleAnalytics');

export const selectTotalViews = createSelector(
    selectModuleAnalyticsState,
  (state: ModuleAnalyticsState): CardDataState<CardViewsData> => state.totalViews
);

export const selectUniqueUsers = createSelector(
    selectModuleAnalyticsState,
  (state: ModuleAnalyticsState): CardDataState<CardUsersData> => state.uniqueUsers
);

export const selectAverageLoadTime = createSelector(
    selectModuleAnalyticsState,
  (state: ModuleAnalyticsState): CardDataState<CardAverageLoadTimeData> => state.averageLoadTime
);

export const selectUsersSessionTime = createSelector(
    selectModuleAnalyticsState,
  (state: ModuleAnalyticsState): CardDataState<CardUsersSessionsData> => state.usersSessionTime
);

export const selectVisitsTrends = createSelector(
  selectModuleAnalyticsState,
  (state: ModuleAnalyticsState): ChartDataState<ModuleVisitsChartData> => state.visitsTrends
);

export const selectUniqueUsersTrends = createSelector(
  selectModuleAnalyticsState,
  (state: ModuleAnalyticsState): ChartDataState<UniqueUsersChartData> => state.uniqueUsersTrends
);

export const selectLoadTimeTrends = createSelector(
  selectModuleAnalyticsState,
  (state: ModuleAnalyticsState): ChartDataState<LoadTimeChartData> =>{ 
    // console.log('Load Time Trends State:', state.loadTimeTrends);

    return state.loadTimeTrends}
);

export const selectSessionTimeTrends = createSelector(
  selectModuleAnalyticsState,
  (state: ModuleAnalyticsState): ChartDataState<ModuleSessionTimeChartData> => {
    // console.log('Session Time Trends State:', state.sessionTrends);
    return state.moduleSessionTrends;
  }
);

export const selectPlatformTrends = createSelector(
  selectModuleAnalyticsState,
  (state: ModuleAnalyticsState): ChartDataState<PlatformChartData> => {
    return state.platformTrends;
  }
);




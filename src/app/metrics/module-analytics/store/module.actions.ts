import { createAction, props } from '@ngrx/store';
import { BrowserData, LoadTimeData, ModuleSessionData, ModuleVisitsData, PlatformData, SessionData, UsersData } from 'src/app/common/interfaces/chartStates';


export const totalViews = createAction(
  '[Metric Moudule Analytics Component] TotalViews'
);
export const totalViewsSuccess = createAction(
  '[Metric Moudule Analytics Component] TotalViewsSuccess',
  props<{ totalViews: number | null; percentageChange: number | null }>()
);
export const totalViewsFailure = createAction(
  '[Metric Moudule Analytics Component] TotalViewsFailure',
  props<{ error: string }>()
);

export const uniqueUsers = createAction(
  '[Metric Moudule Analytics Component] UniqueUsers'
);
export const uniqueUsersSuccess = createAction(
  '[Metric Moudule Analytics Component] UniqueUsersSuccess',
  props<{ totalUsers: number | null; percentageChange: number | null }>()
);
export const uniqueUsersFailure = createAction(
  '[Metric Moudule Analytics Component] UniqueUsersFailure',
  props<{ error: string }>()
);

export const averageLoadTime = createAction(
  '[Metric Moudule Analytics Component] AverageLoadTime'
);
export const averageLoadTimeSuccess = createAction(
  '[Metric Moudule Analytics Component] AverageLoadTimeSuccess',
  props<{ averageLoadTime: number | null; percentageChange: number | null }>()
);
export const averageLoadTimeFailure = createAction(
  '[Metric Moudule Analytics Component] AverageLoadTimeFailure',
  props<{ error: string }>()
);

export const usersSessionTime = createAction(
  '[Metric Moudule Analytics Component] UsersSessionTime'
);
export const usersSessionTimeSuccess = createAction(
  '[Metric Moudule Analytics Component] UsersSessionTimeSuccess',
  props<{
    averageSessionTime: number | null;
    sessionPercentageChange: number | null;
  }>()
);
export const usersSessionTimeFailure = createAction(
  '[Metric Moudule Analytics Component] UsersSessionTimeFailure',
  props<{ error: string }>()
);

export const visitsTrends = createAction(
  '[Metric Moudule Analytics Component] VisitsTrends'
);
export const visitsTrendsSuccess = createAction(
  '[Metric Dashboard Component] VisitsTrendsSuccess',
  props<{ clickTrends: ModuleVisitsData[] }>()
);
export const visitsTrendsFailure = createAction(
  '[Metric Dashboard Component] VisitsTrendsFailure',
  props<{ error: string }>()
);

export const uniqueUsersTrends = createAction(
  '[Metric Moudule Analytics Component] UniqueUsersTrends'
);
export const uniqueUsersTrendsSuccess = createAction(
  '[Metric Moudule Analytics Component] UniqueUsersTrendsSuccess',
  props<{ userTrends: UsersData[] }>()
);
export const uniqueUsersTrendsFailure = createAction(
  '[Metric Moudule Analytics Component] UniqueUsersTrendsFailure',
  props<{ error: string }>()
);


export const loadTimeTrends = createAction(
  '[Metric Moudule Analytics Component] LoadTimeTrends'
);
export const loadTimeTrendsSuccess = createAction(
  '[Metric Moudule Analytics Component] LoadTimeTrendsSuccess',
  props<{ performanceTrends: LoadTimeData[] }>()
);
export const loadTimeTrendsFailure = createAction(
  '[Metric Moudule Analytics Component] LoadTimeTrendsFailure',
  props<{ error: string }>()
);


export const sessionTimeTrends = createAction(
  '[Metric Moudule Analytics Component] SessionTimeTrends'
);
export const sessionTimeTrendsSuccess = createAction(
  '[Metric Moudule Analytics Component] SessionTimeTrendsSuccess',
  props<{ moduleSessionTrends: SessionData[] }>()
);
export const sessionTimeTrendsFailure = createAction(
  '[Metric Moudule Analytics Component] SessionTimeTrendsFailure',
  props<{ error: string }>()
);

export const platformTrends = createAction(
  '[Metric Moudule Analytics Component] PlatformTrends'
);
export const platformTrendsSuccess = createAction(
  '[Metric Moudule Analytics Component] PlatformTrendsSuccess',
  props<{ result: PlatformData[] }>()
);
export const platformTrendsFailure = createAction(
  '[Metric Moudule Analytics Component] PlatformTrendsFailure',
  props<{ error: string }>()
);


import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { map, switchMap, catchError, mergeMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import * as ModuleActions from './module.actions';
import { Action } from '@ngrx/store';
import { ModuleAnalyticsService } from '../services/module-analytics.service';

@Injectable()
export class ModuleAnalyticsEffects {
  constructor(
    private actions$: Actions,
    private moduleService: ModuleAnalyticsService
  ) {}

  createEffectForAction$ = (
    actionType: string,
    serviceFn: () => Observable<any>,
    successFn: (payload: any) => Action,
    failureFn: (payload: any) => Action
  ): Observable<Action> =>
    createEffect(() =>
      this.actions$.pipe(
        ofType(actionType),
        switchMap(() =>
          serviceFn().pipe(
            map(successFn),
            catchError((error) => of(failureFn({ error })))
          )
        )
      )
    );

  totalViews$ = this.createEffectForAction$(
    ModuleActions.totalViews.type,
    this.moduleService.getTotalViews.bind(this.moduleService),
    (payload) => ModuleActions.totalViewsSuccess(payload),
    (payload) => ModuleActions.totalViewsFailure(payload)
  );

  uniqueUsers$ = this.createEffectForAction$(
    ModuleActions.uniqueUsers.type,
    this.moduleService.getUniqueUsers.bind(this.moduleService),
    (payload) => ModuleActions.uniqueUsersSuccess(payload),
    (payload) => ModuleActions.uniqueUsersFailure(payload)
  );

  averageLoadTime$ = this.createEffectForAction$(
    ModuleActions.averageLoadTime.type,
    this.moduleService.getAverageLoadTime.bind(this.moduleService),
    (payload) => ModuleActions.averageLoadTimeSuccess(payload),
    (payload) => ModuleActions.averageLoadTimeFailure(payload)
  );

  usersSessionTime$ = this.createEffectForAction$(
    ModuleActions.usersSessionTime.type,
    this.moduleService.getUsersSessionTime.bind(this.moduleService),
    (payload) => ModuleActions.usersSessionTimeSuccess(payload),
    (payload) => ModuleActions.usersSessionTimeFailure(payload)
  );

  visitsTrends$ = this.createEffectForAction$(
    ModuleActions.visitsTrends.type,
    this.moduleService.getVisitsTrends.bind(this.moduleService),
    (payload) => ModuleActions.visitsTrendsSuccess(payload),
    (payload) => ModuleActions.visitsTrendsFailure(payload)
  );

  uniqueUsersTrends$ = this.createEffectForAction$(
    ModuleActions.uniqueUsersTrends.type,
    this.moduleService.getUniqueUsersTrends.bind(this.moduleService),
    (payload) => ModuleActions.uniqueUsersTrendsSuccess(payload),
    (payload) => ModuleActions.uniqueUsersTrendsFailure(payload)
  );

  loadTimeTrends$ = this.createEffectForAction$(
    ModuleActions.loadTimeTrends.type,
    this.moduleService.getLoadTimeTrends.bind(this.moduleService),
    (payload) => ModuleActions.loadTimeTrendsSuccess(payload),
    (payload) => ModuleActions.loadTimeTrendsFailure(payload)
  );

  sessionTimeTrends$ = this.createEffectForAction$(
    ModuleActions.sessionTimeTrends.type,
    this.moduleService.getSessionTimeTrends.bind(this.moduleService),
    (payload) => ModuleActions.sessionTimeTrendsSuccess(payload),
    (payload) => ModuleActions.sessionTimeTrendsFailure(payload)
  );

  platformTrends$ = this.createEffectForAction$(
    ModuleActions.platformTrends.type,
    this.moduleService.getPlatformTrends.bind(this.moduleService),
    (payload) => ModuleActions.platformTrendsSuccess(payload),
    (payload) => ModuleActions.platformTrendsFailure(payload)
  );
}

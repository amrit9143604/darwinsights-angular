import { createReducer, on } from '@ngrx/store';
import {
  BrowserChartData,
  ChartDataState,
  LoadTimeChartData,
  ModuleSessionChartData,
  ModuleSessionTimeChartData,
  ModuleVisitsChartData,
  PlatformChartData,
  SessionTimeChartData,
  UniqueUsersChartData,
} from 'src/app/common/interfaces/chartStates';
import {
  CardDataState,
  CardAverageLoadTimeData,
  CardUsersData,
  CardUsersSessionsData,
  CardViewsData,
} from 'src/app/common/interfaces/cardStates';
import * as ModuleActions from './module.actions';

interface CombinedState {
  totalViews: CardDataState<CardViewsData>;
  usersSessionTime: CardDataState<CardUsersSessionsData>;
  averageLoadTime: CardDataState<CardAverageLoadTimeData>;
  uniqueUsers: CardDataState<CardUsersData>;
  visitsTrends: ChartDataState<ModuleVisitsChartData>;
  uniqueUsersTrends: ChartDataState<UniqueUsersChartData>;
  loadTimeTrends: ChartDataState<LoadTimeChartData>;
  moduleSessionTrends: ChartDataState<ModuleSessionTimeChartData>;
  platformTrends: ChartDataState<PlatformChartData>;
}

const initialTotalViewsState: CardDataState<CardViewsData> = {
  data: { totalViews: null, percentageChange: null },
  loading: false,
  error: null,
};
const initialUniqueUsersState: CardDataState<CardUsersData> = {
  data: { totalUsers: null, percentageChange: null },
  loading: false,
  error: null,
};
const initialAverageLoadTimeState: CardDataState<CardAverageLoadTimeData> = {
  data: { averageLoadTime: null, percentageChange: null },
  loading: false,
  error: null,
};
const initialUserSessionsState: CardDataState<CardUsersSessionsData> = {
  data: { averageSessionTime: null, sessionPercentageChange: null },
  loading: false,
  error: null,
};
const initialVisitsTrendsState: ChartDataState<ModuleVisitsChartData> = {
  data: { clickTrends: [] },
  loading: false,
  error: null,
};
const initialUniqueUsersTrendsState: ChartDataState<UniqueUsersChartData> = {
  data: { userTrends: [] },
  loading: false,
  error: null,
};
const initialLoadTimeTrendsState: ChartDataState<LoadTimeChartData> = {
  data: { performanceTrends: [] },
  loading: false,
  error: null,
};

const initialSessionTimeTrendsState: ChartDataState<ModuleSessionTimeChartData> =
  {
    data: { moduleSessionTrends: [] },
    loading: false,
    error: null,
  };

const initialPlatformTrendsState: ChartDataState<PlatformChartData> = {
  data: { result: [] },
  loading: false,
  error: null,
};

const initialState: CombinedState = {
  averageLoadTime: initialAverageLoadTimeState,
  uniqueUsers: initialUniqueUsersState,
  totalViews: initialTotalViewsState,
  usersSessionTime: initialUserSessionsState,
  visitsTrends: initialVisitsTrendsState,
  uniqueUsersTrends: initialUniqueUsersTrendsState,
  loadTimeTrends: initialLoadTimeTrendsState,
  moduleSessionTrends: initialSessionTimeTrendsState,
  platformTrends: initialPlatformTrendsState,
};

export const moduleAnalticsReducer = createReducer(
  initialState,
  on(ModuleActions.totalViews, (state) => ({
    ...state,
    totalViews: { ...state.totalViews, loading: true },
  })),
  on(
    ModuleActions.totalViewsSuccess,
    (state, { totalViews, percentageChange }) => {
      console.log('Total views updated:', { totalViews, percentageChange });
      return {
        ...state,
        totalViews: {
          ...state.totalViews,
          loading: false,
          data: { totalViews, percentageChange },
        },
      };
    }
  ),
  on(ModuleActions.totalViewsFailure, (state, { error }) => ({
    ...state,
    totalViews: {
      data: { totalViews: null, percentageChange: null },
      loading: false,
      error,
    },
  })),

  on(ModuleActions.uniqueUsers, (state) => ({
    ...state,
    uniqueUsers: { ...state.uniqueUsers, loading: true },
  })),
  on(
    ModuleActions.uniqueUsersSuccess,
    (state, { totalUsers, percentageChange }) => ({
      ...state,
      uniqueUsers: {
        ...state.uniqueUsers,
        loading: false,
        data: { totalUsers, percentageChange },
      },
    })
  ),
  on(ModuleActions.uniqueUsersFailure, (state, { error }) => ({
    ...state,
    uniqueUsers: {
      data: { totalUsers: null, percentageChange: null },
      loading: false,
      error,
    },
  })),

  on(ModuleActions.averageLoadTime, (state) => ({
    ...state,
    averageLoadTime: { ...state.averageLoadTime, loading: true },
  })),
  on(
    ModuleActions.averageLoadTimeSuccess,
    (state, { averageLoadTime, percentageChange }) => ({
      ...state,
      averageLoadTime: {
        ...state.averageLoadTime,
        loading: false,
        data: { averageLoadTime, percentageChange },
      },
    })
  ),
  on(ModuleActions.averageLoadTimeFailure, (state, { error }) => ({
    ...state,
    averageLoadTime: {
      data: { averageLoadTime: null, percentageChange: null },
      loading: false,
      error,
    },
  })),

  on(ModuleActions.usersSessionTime, (state) => ({
    ...state,
    usersSessionTime: { ...state.usersSessionTime, loading: true },
  })),
  on(
    ModuleActions.usersSessionTimeSuccess,
    (state, { averageSessionTime, sessionPercentageChange }) => ({
      ...state,
      usersSessionTime: {
        ...state.usersSessionTime,
        loading: false,
        data: { averageSessionTime, sessionPercentageChange },
      },
    })
  ),
  on(ModuleActions.usersSessionTimeFailure, (state, { error }) => ({
    ...state,
    usersSessionTime: {
      data: { averageSessionTime: null, sessionPercentageChange: null },
      loading: false,
      error,
    },
  })),

  on(ModuleActions.visitsTrends, (state) => ({
    ...state,
    visitsTrends: { ...state.visitsTrends, loading: true },
  })),
  on(ModuleActions.visitsTrendsSuccess, (state, { clickTrends }) => ({
    ...state,
    visitsTrends: {
      ...state.visitsTrends,
      loading: false,
      data: { clickTrends },
    },
  })),
  on(ModuleActions.visitsTrendsFailure, (state, { error }) => ({
    ...state,
    visitsTrends: { data: { clickTrends: [] }, loading: false, error },
  })),

  on(ModuleActions.uniqueUsersTrends, (state) => ({
    ...state,
    uniqueUsersTrends: { ...state.uniqueUsersTrends, loading: true },
  })),
  on(ModuleActions.uniqueUsersTrendsSuccess, (state, { userTrends }) => ({
    ...state,
    uniqueUsersTrends: {
      ...state.uniqueUsersTrends,
      loading: false,
      data: { userTrends },
    },
  })),
  on(ModuleActions.uniqueUsersTrendsFailure, (state, { error }) => ({
    ...state,
    uniqueUsersTrends: { data: { userTrends: [] }, loading: false, error },
  })),

  on(ModuleActions.loadTimeTrends, (state) => ({
    ...state,
    loadTimeTrends: { ...state.loadTimeTrends, loading: true },
  })),
  on(ModuleActions.loadTimeTrendsSuccess, (state, { performanceTrends }) => ({
    ...state,
    loadTimeTrends: {
      ...state.loadTimeTrends,
      loading: false,
      data: { performanceTrends },
    },
  })),
  on(ModuleActions.loadTimeTrendsFailure, (state, { error }) => ({
    ...state,
    loadTimeTrends: { data: { performanceTrends: [] }, loading: false, error },
  })),

  on(ModuleActions.sessionTimeTrends, (state) => ({
    ...state,
    moduleSessionTrends: { ...state.moduleSessionTrends, loading: true },
  })),
  on(
    ModuleActions.sessionTimeTrendsSuccess,
    (state, { moduleSessionTrends }) => ({
      ...state,
      moduleSessionTrends: {
        ...state.moduleSessionTrends,
        loading: false,
        data: { moduleSessionTrends },
      },
    })
  ),
  on(ModuleActions.sessionTimeTrendsFailure, (state, { error }) => ({
    ...state,
    moduleSessionTrends: {
      data: { moduleSessionTrends: [] },
      loading: false,
      error,
    },
  })),

  on(ModuleActions.platformTrends, (state) => ({
    ...state,
    platformTrends: { ...state.platformTrends, loading: true },
  })),
  on(ModuleActions.platformTrendsSuccess, (state, { result }) => ({
    ...state,
    platformTrends: {
      ...state.platformTrends,
      loading: false,
      data: { result },
    },
  })),
  on(ModuleActions.platformTrendsFailure, (state, { error }) => ({
    ...state,
    platformTrends: { data: { result: [] }, loading: false, error },
  }))
);

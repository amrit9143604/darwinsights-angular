import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModuleAnalyticsComponent } from './module-analytics.component';

const routes: Routes = [{ path: '', component: ModuleAnalyticsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModuleAnalyticsRoutingModule { }

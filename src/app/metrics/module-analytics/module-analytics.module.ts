import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModuleAnalyticsRoutingModule } from './module-analytics-routing.module';
import { ModuleAnalyticsComponent } from './module-analytics.component';
import { moduleAnalticsReducer } from './store/module.reducers';
import { ModuleAnalyticsEffects } from './store/module.effects';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    ModuleAnalyticsComponent
  ],
  imports: [
    CommonModule,
    ModuleAnalyticsRoutingModule,
    SharedModule,
    DropdownModule,
    FormsModule,
    StoreModule.forFeature('moduleAnalytics', moduleAnalticsReducer),
    EffectsModule.forFeature([ModuleAnalyticsEffects]),
  ]
})
export class ModuleAnalyticsModule { }

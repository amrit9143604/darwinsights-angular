import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subject, Subscription, map, takeUntil, tap } from 'rxjs';
import { setModule, setTenant, setTime } from '../store/metrics.actions';
import { metricData } from '../store/metrics.selectors';
import {
  metricOptions,
  timeArray,
  timeLabels,
} from 'src/app/common/interfaces/common';
import { CookieService } from 'ngx-cookie-service';
import {
  ChartDataState,
  LoadTimeChartData,
  ModuleSessionTimeChartData,
  ModuleVisitsChartData,
  PlatformChartData,
  SessionTimeChartData,
  UniqueUsersChartData,
} from 'src/app/common/interfaces/chartStates';
import {
  averageLoadTime,
  loadTimeTrends,
  platformTrends,
  sessionTimeTrends,
  totalViews,
  uniqueUsers,
  uniqueUsersTrends,
  usersSessionTime,
  visitsTrends,
} from './store/module.actions';
import { ModuleAnalyticsState } from 'src/app/common/interfaces/commonStates';
import { ModuleAnalyticsService } from './services/module-analytics.service';
import {
  selectAverageLoadTime,
  selectLoadTimeTrends,
  selectPlatformTrends,
  selectSessionTimeTrends,
  selectTotalViews,
  selectUniqueUsers,
  selectUniqueUsersTrends,
  selectUsersSessionTime,
  selectVisitsTrends,
} from './store/module.selectors';
import { MetricsService } from '../services/metrics.service';

@Component({
  selector: 'app-module-analytics',
  templateUrl: './module-analytics.component.html',
  styleUrls: ['./module-analytics.component.scss'],
})
export class ModuleAnalyticsComponent implements OnInit {
  loadingState = {
    views: false,
    users: false,
    avgTime: false,
    session: false,
    visitsTrends: false,
    uniqueUsersTrends: false,
    loadTimeTrends: false,
    sessionTimeTrends: false,
    platformTrends: false,
  };

  tenants$!: Observable<metricOptions[]>;
  selectedTenant!: metricOptions;

  modules$!: Observable<metricOptions[]>;
  selectedModule!: metricOptions;

  time!: metricOptions[];
  selectedTime!: metricOptions;

  totalViews$!: Observable<number | null>;
  totalViewsPercentage$!: Observable<number | null>;
  totalUsers$!: Observable<number | null>;
  totalUsersPercentage$!: Observable<number | null>;
  averageLoadTime$!: Observable<number | null>;
  loadTimePercentage$!: Observable<number | null>;
  usersSessionTime$!: Observable<number | null>;
  usersSessionPercentage$!: Observable<number | null>;
  visitsOptions$!: Observable<ChartDataState<ModuleVisitsChartData>>;
  uniqueUsersOptions$!: Observable<ChartDataState<UniqueUsersChartData>>;
  loadTimeOptions$!: Observable<ChartDataState<LoadTimeChartData>>;
  sessionTimeOptions$!: Observable<ChartDataState<ModuleSessionTimeChartData>>;
  platformOptions$!: Observable<ChartDataState<PlatformChartData>>;

  visitsChartType: boolean = true;
  uniqueUsersChartType: boolean = true;
  loadTimeChartType: boolean = true;
  sessionTimeChartType: boolean = true;

  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private store: Store<ModuleAnalyticsState>,
    private cookieService: CookieService,
    private metricService: MetricsService
  ) {
    this.time = timeArray;
  }

  ngOnInit(): void {
    this.store.pipe(select(metricData), takeUntil(this.destroy$)).subscribe((state) => {
      this.selectedTenant = {
        label: state.tenant ?? '',
        value: state.tenant ?? '',
      };
      this.selectedModule = {
        label: state.module?.toUpperCase() ?? '',
        value: state.module ?? '',
      };
      this.selectedTime = {
        label: timeLabels[state.time] ?? state.time ?? '',
        value: state.time ?? '',
      };
    });

    this.fetchTenants();
    this.fetchModules();
    this.dispatchActions();
    this.retrieveData();
  }

  onTenantChange(value: string) {
    this.cookieService.set('TENANT', value, 2, '/');
    this.store.dispatch(setTenant({ tenant: value }));
    this.dispatchActions();
    this.retrieveData();
  }

  onTimeChange(value: string) {
    this.cookieService.set('TIME', value, 2, '/');
    this.store.dispatch(setTime({ time: value }));
    this.dispatchActions();
    this.retrieveData();
  }

  onModuleChange(value: string) {
    console.log(value);
    this.cookieService.set('MODULE', value, 2, '/');
    this.store.dispatch(setModule({ module: value }));
    this.dispatchActions();
    this.retrieveData();
  }

  private dispatchActions(): void {
    this.store.dispatch(totalViews());
    this.store.dispatch(uniqueUsers());
    this.store.dispatch(averageLoadTime());
    this.store.dispatch(usersSessionTime());
    this.store.dispatch(visitsTrends());
    this.store.dispatch(uniqueUsersTrends());
    this.store.dispatch(loadTimeTrends());
    this.store.dispatch(sessionTimeTrends());
    this.store.dispatch(platformTrends());
  }

  private fetchTenants(): void {
    this.tenants$ = this.metricService.getAllTenants().pipe(
      takeUntil(this.destroy$)
    );
  }

  private fetchModules(): void {
    this.modules$ = this.metricService.getAllModules().pipe(
      takeUntil(this.destroy$)
    );
  }

  private retrieveData(): void {
    this.retrieveTotalViews();
    this.retrieveUniqueUsers();
    this.retrieveAverageLoadTime();
    this.retrieveUsersSessionTime();
    this.retrieveVisitsTrends();
    this.retrieveUniqueUsersTrends();
    this.retrieveLoadTimeTrends();
    this.retrieveSessionTimeTrends();
    this.retrievePlatformTrends();
  }

  private retrieveTotalViews(): void {
    this.totalViews$ = this.store.pipe(
      select(selectTotalViews),
      map((state) => state.data.totalViews)
    );
    this.totalViewsPercentage$ = this.store.pipe(
      select(selectTotalViews),
      map((state) => state.data.percentageChange)
    );
    this.subscribeToLoadingState('views', selectTotalViews);
  }

  private retrieveUniqueUsers(): void {
    this.totalUsers$ = this.store.pipe(
      select(selectUniqueUsers),
      map((state) => state.data.totalUsers)
    );
    this.totalUsersPercentage$ = this.store.pipe(
      select(selectUniqueUsers),
      map((state) => state.data.percentageChange)
    );
    this.subscribeToLoadingState('users', selectUniqueUsers);
  }

  private retrieveAverageLoadTime(): void {
    this.averageLoadTime$ = this.store.pipe(
      select(selectAverageLoadTime),
      map((state) => state.data.averageLoadTime)
    );
    this.loadTimePercentage$ = this.store.pipe(
      select(selectAverageLoadTime),
      map((state) => state.data.percentageChange)
    );
    this.subscribeToLoadingState('avgTime', selectAverageLoadTime);
  }

  private retrieveUsersSessionTime(): void {
    this.usersSessionTime$ = this.store.pipe(
      select(selectUsersSessionTime),
      map((state) => state.data.averageSessionTime)
    );
    this.usersSessionPercentage$ = this.store.pipe(
      select(selectUsersSessionTime),
      map((state) => state.data.sessionPercentageChange)
    );
    this.subscribeToLoadingState('session', selectUsersSessionTime);
  }

  private retrieveVisitsTrends(): void {
    this.visitsOptions$ = this.store.pipe(select(selectVisitsTrends));
    this.subscribeToLoadingState('visitsTrends', selectVisitsTrends);
  }

  private retrieveUniqueUsersTrends(): void {
    this.uniqueUsersOptions$ = this.store.pipe(select(selectUniqueUsersTrends));
    this.subscribeToLoadingState('uniqueUsersTrends', selectUniqueUsersTrends);
  }

  private retrieveLoadTimeTrends(): void {
    this.loadTimeOptions$ = this.store.pipe(select(selectLoadTimeTrends));
    this.subscribeToLoadingState('loadTimeTrends', selectLoadTimeTrends);
  }

  private retrieveSessionTimeTrends(): void {
    this.sessionTimeOptions$ = this.store.pipe(select(selectSessionTimeTrends));
    this.subscribeToLoadingState('sessionTimeTrends', selectSessionTimeTrends);
  }

  private retrievePlatformTrends(): void {
    this.platformOptions$ = this.store.pipe(select(selectPlatformTrends));
    this.subscribeToLoadingState('platformTrends', selectPlatformTrends);
  }

  private subscribeToLoadingState(
    prop: keyof (typeof ModuleAnalyticsComponent)['prototype']['loadingState'],
    selector: any
  ): void {
    this.store
      .pipe(
        select(selector),
        tap((state: any) => (this.loadingState[prop] = state.loading)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  handleChartType(chartType: string) {
    switch (chartType) {
      case 'visits':
        this.visitsChartType = !this.visitsChartType;
        break;
      case 'uniqueUsers':
        this.uniqueUsersChartType = !this.uniqueUsersChartType;
        break;
      case 'loadTime':
        this.loadTimeChartType = !this.loadTimeChartType;
        break;
      case 'sessionTime':
        this.sessionTimeChartType = !this.sessionTimeChartType;
        break;
      default:
        break;
    }
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

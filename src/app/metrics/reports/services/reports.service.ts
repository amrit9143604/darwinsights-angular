import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subject, Subscription, catchError, map, takeUntil, tap, throwError } from 'rxjs';
import { metricData } from '../../store/metrics.selectors';
import { REPORTS_GENERATE_SUMMARY, REPORTS_TABLE_DATA, SESSIONS_CHART_SESSION_TIME_TRENDS } from 'src/app/common/apis/constants';
import { ReportsTableData, SessionTimeChartData } from 'src/app/common/interfaces/chartStates';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  private userId!: string | null;
  private body!: { userId: string | null };
  private destroy$: Subject<void> = new Subject<void>();


  constructor(private http: HttpClient, private store: Store) {
     this.store.pipe(select(metricData), takeUntil(this.destroy$)).subscribe((state) => {
      this.userId = state.userId;
      this.body = { userId: this.userId };
    });

  }

  private handleError(error: string) {
    console.error('An error occurred:', error);
    return throwError('Something went wrong');
  }

  private post(url: string): Observable<Object> {
    return this.http.post(url, this.body);
  }


  getReportsData(): Observable<ReportsTableData> {
    return this.post(REPORTS_TABLE_DATA).pipe(
      map((response: any) => ({
        tableData: response,
      })),
      tap((data) => console.log(data)),
      catchError(this.handleError)
    );
  }

  private headers = new HttpHeaders({
    'Content-Type': 'text/plain;charset=UTF-8' 
  });
  
  generateReport({ name, tenant, moduleType, startMonth, endMonth, userId }: {
    name: string,
    tenant: string,
    moduleType: string,
    startMonth: string,
    endMonth: string
    userId: string
  }): Observable<any> {
    const body = {
      reportName: name,
      tenantId: tenant,
      summaryType: moduleType,
      startDate: startMonth,
      endDate: endMonth,
      userId: userId
    };

    return this.http.post(REPORTS_GENERATE_SUMMARY, body,  { headers: this.headers }).pipe(
      map((response: any) => ({
         response,
      })),
      tap((data) => console.log(data)),
      catchError(this.handleError)
    );
  }

  



  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

import { createReducer, on } from '@ngrx/store';
import {
  BrowserChartData,
  ChartDataState,
  LoadTimeChartData,
  ModuleChartData,
  ModuleDetailsTrendsChartData,
  ModuleSessionChartData,
  PlatformChartData,
  ReportsTableData,
  SessionTimeChartData,
  UniqueUsersChartData,
  UsersTrendsChartData,
  VisitsChartData,
} from 'src/app/common/interfaces/chartStates';
import {
  CardDataState,
  CardAverageLoadTimeData,
  CardUsersData,
  CardUsersSessionsData,
  CardViewsData,
  CardMostVisitedModule,
  CardUniqueUsersInMostVisitedModule,
} from 'src/app/common/interfaces/cardStates';
import * as ReportsActions from './reports.actions';

interface CombinedState {
  tableData: ChartDataState<ReportsTableData>;
}

const initialReportsTableState: ChartDataState<ReportsTableData> = {
  data: { tableData: [] },
  loading: false,
  error: null,
};

const initialState: CombinedState = {
  tableData: initialReportsTableState,
};

export const reportsReducer = createReducer(
  initialState,

  on(ReportsActions.reportTrends, (state) => ({
    ...state,
    tableData: { ...state.tableData, loading: true },
  })),
  on(ReportsActions.reportTrendsSuccess, (state, { tableData }) => ({
    ...state,
    tableData: {
      ...state.tableData,
      loading: false,
      data: { tableData },
    },
  })),
  on(ReportsActions.reportTrendsFailure, (state, { error }) => ({
    ...state,
    tableData: { data: { tableData: [] }, loading: false, error },
  }))
);

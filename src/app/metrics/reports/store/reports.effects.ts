import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { map, switchMap, catchError, mergeMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import * as ReportsActions from './reports.actions';
import { Action } from '@ngrx/store';
import { ReportsService } from '../services/reports.service';
@Injectable()
export class ReportsEffects {
  constructor(
    private actions$: Actions,
    private reportsService: ReportsService
  ) {}

  createEffectForAction$ = (
    actionType: string,
    serviceFn: () => Observable<any>,
    successFn: (payload: any) => Action,
    failureFn: (payload: any) => Action
  ): Observable<Action> =>
    createEffect(() =>
      this.actions$.pipe(
        ofType(actionType),
        switchMap(() =>
          serviceFn().pipe(
            map(successFn),
            catchError((error) => of(failureFn({ error })))
          )
        )
      )
    );

  reportsTrends$ = this.createEffectForAction$(
    ReportsActions.reportTrends.type,
    this.reportsService.getReportsData.bind(this.reportsService),
    (payload) => ReportsActions.reportTrendsSuccess(payload),
    (payload) => ReportsActions.reportTrendsFailure(payload)
  );


}

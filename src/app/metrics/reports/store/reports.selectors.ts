import { createSelector, createFeatureSelector } from '@ngrx/store';
import { ChartDataState, ReportsTableData } from 'src/app/common/interfaces/chartStates';
import { ReportsState } from 'src/app/common/interfaces/commonStates';

const selectReportsState = createFeatureSelector<ReportsState>('reports');

export const selectReportsTrends = createSelector(
    selectReportsState,
  (state: ReportsState): ChartDataState<ReportsTableData> => {
    return state.tableData;
  }
);

import { createAction, props } from '@ngrx/store';
import {
  ReportsData,
} from 'src/app/common/interfaces/chartStates';

export const reportTrends = createAction(
  '[Metric Reports Component] ReportTrends'
);
export const reportTrendsSuccess = createAction(
  '[Metric Reports Component] ReportTrendsSuccess',
  props<{ tableData: ReportsData[] }>()
);
export const reportTrendsFailure = createAction(
  '[Metric Reports Component] ReportTrendsFailure',
  props<{ error: string }>()
);

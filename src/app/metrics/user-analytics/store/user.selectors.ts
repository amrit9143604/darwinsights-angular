import { createSelector, createFeatureSelector } from '@ngrx/store';
import {
  ChartDataState,
  SessionTimeChartData,
  UsersRoutesData,
  VisitsChartData,
} from 'src/app/common/interfaces/chartStates';
import {
  UserAnalyticsState,
} from 'src/app/common/interfaces/commonStates';

const selectUsersState = createFeatureSelector<UserAnalyticsState>('user');

export const selectVisitsTrends = createSelector(
  selectUsersState,
  (state: UserAnalyticsState): ChartDataState<VisitsChartData> =>
    state.visitsTrends
);

export const selectSessionTimeTrends = createSelector(
  selectUsersState,
  (state: UserAnalyticsState): ChartDataState<SessionTimeChartData> => {
    return state.sessionTrends;
  }
);

export const selectRoutesTrends = createSelector(
  selectUsersState,
  (state: UserAnalyticsState): ChartDataState<UsersRoutesData> => {
    return state.routesTrends;
  }
);

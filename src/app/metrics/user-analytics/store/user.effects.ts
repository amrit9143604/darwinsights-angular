import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { map, catchError, switchMap, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import * as UsersActions from './user.actions';
import { UserAnalyticsService } from '../services/user-analytics.service';
import { Action } from '@ngrx/store';

@Injectable()
export class UserEffects {
  constructor(
    private actions$: Actions,
    private usersService: UserAnalyticsService
  ) {}

  private createEffectForAction$<R>(
    actionType: string,
    serviceFn: (user: string) => Observable<R>,
    successFn: (payload: R) => Action,
    failureFn: (payload: any) => Action
  ): Observable<any> {
    return createEffect(() =>
      this.actions$.pipe(
        ofType(actionType),
        switchMap(action =>
          serviceFn((action as any).user).pipe(
            map(successFn),
            catchError((error) => of(failureFn({ error })))
          )
        )
      )
    );
  }

  visitsTrends$ = this.createEffectForAction$(
    UsersActions.visitsTrends.type,
    (user: string) => this.usersService.getVisitsTrends(user),
    ({ clickTrends }) => UsersActions.visitsTrendsSuccess({ clickTrends }),
    (error) => UsersActions.visitsTrendsFailure({ error })
  );

  sessionTimeTrends$ = this.createEffectForAction$(
    UsersActions.sessionTimeTrends.type,
    (user: string) => this.usersService.getSessionTimeTrends(user),
    ({ sessionTrends }) => UsersActions.sessionTimeTrendsSuccess({ sessionTrends }),
    (error) => UsersActions.sessionTimeTrendsFailure({ error })
  );

  routesTrends$ = this.createEffectForAction$(
    UsersActions.routesTrends.type,
    (user: string) => this.usersService.getRoutesData(user),
    ({ tableData }) => UsersActions.routesTrendsSuccess({ tableData }),
    (error) => UsersActions.routesTrendsFailure({ error })
  );
}

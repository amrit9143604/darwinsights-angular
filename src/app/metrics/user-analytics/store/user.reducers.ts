import { createReducer, on } from '@ngrx/store';
import {
  ChartDataState,
  CompareChartPerformanceData,
  CompareChartVisitsData,
  LoadTimeChartData,
  SessionTimeChartData,
  UsersRoutesData,
  VisitsChartData,
} from 'src/app/common/interfaces/chartStates';
import * as UsersActions from './user.actions';

interface CombinedState {
  visitsTrends: ChartDataState<VisitsChartData>;
  sessionTrends: ChartDataState<SessionTimeChartData>;
  routesTrends: ChartDataState<UsersRoutesData>;
}

const initialVisitsTrendsState: ChartDataState<VisitsChartData> = {
  data: { clickTrends: [] },
  loading: false,
  error: null,
};

const initialLoadTimeTrendsState: ChartDataState<SessionTimeChartData> = {
  data: { sessionTrends: [] },
  loading: false,
  error: null,
};

const initialRoutesTrendsState: ChartDataState<UsersRoutesData> = {
  data: { tableData: [] },
  loading: false,
  error: null,
};

const initialState: CombinedState = {
  visitsTrends: initialVisitsTrendsState,
  sessionTrends: initialLoadTimeTrendsState,
  routesTrends: initialRoutesTrendsState,
};

export const userReducer = createReducer(
  initialState,

  on(UsersActions.visitsTrends, (state) => ({
    ...state,
    visitsTrends: { ...state.visitsTrends, loading: true },
  })),
  on(UsersActions.visitsTrendsSuccess, (state, { clickTrends }) => ({
    ...state,
    visitsTrends: {
      ...state.visitsTrends,
      loading: false,
      data: { clickTrends },
    },
  })),
  on(UsersActions.visitsTrendsFailure, (state, { error }) => ({
    ...state,
    visitsTrends: { data: { clickTrends: [] }, loading: false, error },
  })),

  on(UsersActions.sessionTimeTrends, (state) => ({
    ...state,
    sessionTrends: { ...state.sessionTrends, loading: true },
  })),
  on(UsersActions.sessionTimeTrendsSuccess, (state, { sessionTrends }) => ({
    ...state,
    sessionTrends: {
      ...state.sessionTrends,
      loading: false,
      data: { sessionTrends },
    },
  })),
  on(UsersActions.sessionTimeTrendsFailure, (state, { error }) => ({
    ...state,
    sessionTrends: { data: { sessionTrends: [] }, loading: false, error },
  })),

  on(UsersActions.routesTrends, (state) => ({
    ...state,
    routesTrends: { ...state.routesTrends, loading: true },
  })),
  on(UsersActions.routesTrendsSuccess, (state, { tableData }) => ({
    ...state,
    routesTrends: {
      ...state.routesTrends,
      loading: false,
      data: { tableData },
    },
  })),
  on(UsersActions.routesTrendsFailure, (state, { error }) => ({
    ...state,
    routesTrends: { data: { tableData: [] }, loading: false, error },
  }))
);

import { createAction, props } from '@ngrx/store';
import { RoutesData, SessionData, UsersRoutesData, VisitsData } from 'src/app/common/interfaces/chartStates';

export const visitsTrends = createAction(
  '[Metric Users Component] VisitsTrends',
  props<{ user: string }>()
);
export const visitsTrendsSuccess = createAction(
  '[Metric Users Component] VisitsTrendsSuccess',
  props<{ clickTrends: VisitsData[] }>()
);
export const visitsTrendsFailure = createAction(
  '[Metric Users Component] VisitsTrendsFailure',
  props<{ error: string }>()
);

export const sessionTimeTrends = createAction(
  '[Metric Users Component] SessionTimeTrends',
  props<{ user: string }>()
);
export const sessionTimeTrendsSuccess = createAction(
  '[Metric Users Component] SessionTimeTrendsSuccess',
  props<{ sessionTrends: SessionData[] }>()
);
export const sessionTimeTrendsFailure = createAction(
  '[Metric Users Component] SessionTimeTrendsFailure',
  props<{ error: string }>()
);


export const routesTrends = createAction(
    '[Metric Users Component] RoutesTrends',
    props<{ user: string }>()
  );
  export const routesTrendsSuccess = createAction(
    '[Metric Users Component] RoutesTrendsSuccess',
    props<{ tableData: RoutesData[] }>()
  );
  export const routesTrendsFailure = createAction(
    '[Metric Users Component] RoutesTrendsFailure',
    props<{ error: string }>()
  );
  
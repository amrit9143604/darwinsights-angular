import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
  Observable,
  Subject,
  Subscription,
  debounceTime,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs';
import {
  metricOptions,
  timeArray,
  timeLabels,
} from 'src/app/common/interfaces/common';
import { MetricsState } from '../store/metrics.reducers';
import { metricData } from '../store/metrics.selectors';
import { CookieService } from 'ngx-cookie-service';
import { MetricsService } from '../services/metrics.service';
import { setTenant, setTime } from '../store/metrics.actions';
import {
  ChartDataState,
  SessionTimeChartData,
  UsersRoutesData,
  VisitsChartData,
} from 'src/app/common/interfaces/chartStates';
import { ComparisonService } from '../comparison/services/comparison.service';
import { UserAnalyticsService } from './services/user-analytics.service';
import {
  routesTrends,
  sessionTimeTrends,
  visitsTrends,
} from './store/user.actions';
import {
  selectRoutesTrends,
  selectSessionTimeTrends,
  selectVisitsTrends,
} from './store/user.selectors';

@Component({
  selector: 'app-user-analytics',
  templateUrl: './user-analytics.component.html',
  styleUrls: ['./user-analytics.component.scss'],
})
export class UserAnalyticsComponent implements OnInit {
  loadingState = {
    visitsTrends: false,
    sessionTrends: false,
    routesTrends: false,
  };
  tenants$!: Observable<metricOptions[]>;
  selectedTenant!: metricOptions;

  time!: metricOptions[];
  selectedTime!: metricOptions;

  user!: string;
  showData: boolean = false;

  visitsOptions$!: Observable<ChartDataState<VisitsChartData>>;
  sessionOptions$!: Observable<ChartDataState<SessionTimeChartData>>;
  routesOptions$!: Observable<ChartDataState<UsersRoutesData>>;

  visitsChartType: boolean = true;
  sessionTimeChartType: boolean = true;
  private destroy$: Subject<void> = new Subject<void>();
  private inputChangeSubject: Subject<string> = new Subject<string>();

  constructor(
    private store: Store<MetricsState>,
    private cookieService: CookieService,
    private metricService: MetricsService
  ) {
    this.time = timeArray;
  }

  ngOnInit(): void {
    this.store
      .pipe(select(metricData), takeUntil(this.destroy$))
      .subscribe((state) => {
        this.selectedTenant = {
          label: state.tenant ?? '',
          value: state.tenant ?? '',
        };
        this.selectedTime = {
          label: timeLabels[state.time] ?? state.time ?? '',
          value: state.time ?? '',
        };
      });

    this.fetchTenants();

    this.inputChangeSubject
      .pipe(
        debounceTime(1000),
        tap((value) => (this.user = value)),
        switchMap(() => this.fetchDataForAllUsers()),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  onInputChange(event: any) {
    const value = event.target.value;
    this.inputChangeSubject.next(value);
  }

  moduleColumnData = [
    { headerName: 'Module', field: 'module' },
    { headerName: 'Route', field: 'route' },
    { headerName: 'Visits', field: 'visitCount' },
    { headerName: 'Platform', field: 'platform' },
  ];
  onTenantChange(value: string) {
    this.user = '';
    this.cookieService.set('TENANT', value, 2, '/');
    this.store.dispatch(setTenant({ tenant: value }));
    this.showData = false;
  }

  onTimeChange(value: string) {
    this.cookieService.set('TIME', value, 2, '/');
    this.store.dispatch(setTime({ time: value }));
    this.fetchDataForAllUsers();
  }

  fetchDataForAllUsers(): Observable<any> {
    if (!this.user.trim()) {
      return new Observable((observer) => observer.complete());
    }

    return new Observable((observer) => {
      observer.next();
      this.store.dispatch(visitsTrends({ user: this.user }));
      this.store.dispatch(sessionTimeTrends({ user: this.user }));
      this.store.dispatch(routesTrends({ user: this.user }));
      this.retrieveVisitsTrends();
      this.retrieveLoadTimeTrends();
      this.retrieveRoutesTrends();
      this.showData = true;
      observer.complete();
    });
  }

  private retrieveVisitsTrends(): void {
    this.visitsOptions$ = this.store.pipe(select(selectVisitsTrends));
    this.subscribeToLoadingState('visitsTrends', selectVisitsTrends);
  }

  private retrieveLoadTimeTrends(): void {
    this.sessionOptions$ = this.store.pipe(select(selectSessionTimeTrends), tap(data => console.log(data)));
    this.subscribeToLoadingState('sessionTrends', selectSessionTimeTrends);
  }

  private retrieveRoutesTrends(): void {
    this.routesOptions$ = this.store.pipe(select(selectRoutesTrends))
    this.subscribeToLoadingState('routesTrends', selectRoutesTrends);
  }

  private subscribeToLoadingState(
    prop: keyof (typeof UserAnalyticsComponent)['prototype']['loadingState'],
    selector: any
  ): void {
    this.store
      .pipe(
        select(selector),
        tap((state: any) => (this.loadingState[prop] = state.loading)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }
  private fetchTenants(): void {
    this.tenants$ = this.metricService
      .getAllTenants()
      .pipe(takeUntil(this.destroy$));
  }

  handleChartType(chartType: string) {
    switch (chartType) {
      case 'visits':
        this.visitsChartType = !this.visitsChartType;
        break;

      case 'sessionTime':
        this.sessionTimeChartType = !this.sessionTimeChartType;
        break;
      default:
        break;
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

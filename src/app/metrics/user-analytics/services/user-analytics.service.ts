import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject, Subscription, catchError, map, takeUntil, throwError } from 'rxjs';
import {
  DASHBOARD_CHART_VISITS_TRENDS,
  DASHBOARD_CHART_SESSION_TRENDS,
  USER_ANALYTICS_CHART_VISITS_TRENDS,
  USER_ANALYTICS_CHART_SESSION_TRENDS,
  USER_ANALYTICS_CHART_TABLE_DATA,
} from 'src/app/common/apis/constants';
import {
  VisitsChartData,
  SessionTimeChartData,
  UsersRoutesData,
} from 'src/app/common/interfaces/chartStates';
import { metricData } from '../../store/metrics.selectors';
import { Store, select } from '@ngrx/store';

@Injectable({
  providedIn: 'root',
})
export class UserAnalyticsService {
  private tenantId!: string | null;
  private range!: string | null;
  private body!: { tenantId: string | null; range: string | null };
  private destroy$: Subject<void> = new Subject<void>();


  constructor(private http: HttpClient, private store: Store) {
    this.store
      .pipe(select(metricData), takeUntil(this.destroy$))
      .subscribe((state) => {
        this.tenantId = state.tenant;
        this.range = state.time;
        this.body = { tenantId: this.tenantId, range: this.range };
      });
  }

  private handleError(error: string) {
    console.error('An error occurred:', error);
    return throwError('Something went wrong');
  }

  private post(url: string, body: Object): Observable<Object> {
    return this.http.post(url, body);
  }

  getVisitsTrends(user: string): Observable<VisitsChartData> {
    const bodyWithUsers = { ...this.body, userId: user };
    return this.post(USER_ANALYTICS_CHART_VISITS_TRENDS, bodyWithUsers).pipe(
      map((response: any) => ({
        clickTrends: response,
      })),
      catchError(this.handleError)
    );
  }

  getSessionTimeTrends(user: string): Observable<SessionTimeChartData> {
    const bodyWithUsers = { ...this.body, userId: user };

    return this.post(USER_ANALYTICS_CHART_SESSION_TRENDS, bodyWithUsers).pipe(
      map((response: any) => ({
        sessionTrends: response,
      })),
      catchError(this.handleError)
    );
  }

  getRoutesData(user: string): Observable<UsersRoutesData> {
    const bodyWithUsers = { ...this.body, userId: user };

    return this.post(USER_ANALYTICS_CHART_TABLE_DATA, bodyWithUsers).pipe(
      map((response: any) => ({
        tableData: response,
      })),
      catchError(this.handleError)
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

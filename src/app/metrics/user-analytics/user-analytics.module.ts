import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserAnalyticsRoutingModule } from './user-analytics-routing.module';
import { UserAnalyticsComponent } from './user-analytics.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserEffects } from './store/user.effects';
import { userReducer } from './store/user.reducers';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

@NgModule({
  declarations: [UserAnalyticsComponent],
  imports: [
    CommonModule,
    UserAnalyticsRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownModule,
    StoreModule.forFeature('user', userReducer),
    EffectsModule.forFeature([UserEffects]),
  ],
})
export class UserAnalyticsModule {}

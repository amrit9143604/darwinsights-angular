import { createReducer, on } from '@ngrx/store';
import {
  ChartDataState,
  CompareChartPerformanceData,
  CompareChartVisitsData,
  LoadTimeChartData,
  SessionTimeChartData,
  VisitsChartData,
} from 'src/app/common/interfaces/chartStates';
import * as ComparisonActions from './comparison.actions';

interface CombinedState {
  visitsTrends: ChartDataState<VisitsChartData>;
  sessionTrends: ChartDataState<SessionTimeChartData>;
}

const initialVisitsTrendsState: ChartDataState<VisitsChartData> = {
  data: { clickTrends: [] },
  loading: false,
  error: null,
};

const initialLoadTimeTrendsState: ChartDataState<SessionTimeChartData> = {
  data: { sessionTrends: [] },
  loading: false,
  error: null,
};

const initialState: CombinedState = {
  visitsTrends: initialVisitsTrendsState,
  sessionTrends: initialLoadTimeTrendsState,
};

export const comparisonReducer = createReducer(
  initialState,

  on(ComparisonActions.visitsTrends, (state) => ({
    ...state,
    visitsTrends: { ...state.visitsTrends, loading: true },
  })),
  on(ComparisonActions.visitsTrendsSuccess, (state, { clickTrends }) => ({
    ...state,
    visitsTrends: {
      ...state.visitsTrends,
      loading: false,
      data: { clickTrends },
    },
  })),
  on(ComparisonActions.visitsTrendsFailure, (state, { error }) => ({
    ...state,
    visitsTrends: { data: { clickTrends: [] }, loading: false, error },
  })),

  on(ComparisonActions.sessionTimeTrends, (state) => ({
    ...state,
    sessionTrends: { ...state.sessionTrends, loading: true },
  })),
  on(
    ComparisonActions.sessionTimeTrendsSuccess,
    (state, { sessionTrends }) => ({
      ...state,
      sessionTrends: {
        ...state.sessionTrends,
        loading: false,
        data: { sessionTrends },
      },
    })
  ),
  on(ComparisonActions.sessionTimeTrendsFailure, (state, { error }) => ({
    ...state,
    sessionTrends: { data: { sessionTrends: [] }, loading: false, error },
  }))
);

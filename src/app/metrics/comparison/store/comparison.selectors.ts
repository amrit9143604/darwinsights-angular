import { createSelector, createFeatureSelector } from '@ngrx/store';
import {
  CardAverageLoadTimeData,
  CardDataState,
  CardUsersData,
  CardUsersSessionsData,
  CardViewsData,
} from 'src/app/common/interfaces/cardStates';
import {
  BrowserChartData,
  ChartDataState,
  CompareChartPerformanceData,
  CompareChartVisitsData,
  LoadTimeChartData,
  ModuleSessionChartData,
  PlatformChartData,
  SessionTimeChartData,
  UniqueUsersChartData,
  VisitsChartData,
} from 'src/app/common/interfaces/chartStates';
import {
  ComparisonState,
  DashboardState,
} from 'src/app/common/interfaces/commonStates';

const selectComparisonState =
  createFeatureSelector<ComparisonState>('comparison');

export const selectVisitsTrends = createSelector(
  selectComparisonState,
  (state: ComparisonState): ChartDataState<VisitsChartData> =>
    state.visitsTrends
);

export const selectSessionTimeTrends = createSelector(
  selectComparisonState,
  (state: ComparisonState): ChartDataState<SessionTimeChartData> => {
    return state.sessionTrends;
  }
);

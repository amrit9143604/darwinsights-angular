import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { map, catchError, switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import * as ComparisonActions from './comparison.actions';
import { ComparisonService } from '../services/comparison.service';
import { Action } from '@ngrx/store';

@Injectable()
export class ComparisonEffects {
  constructor(
    private actions$: Actions,
    private comparisonService: ComparisonService
  ) {}

  private createEffectForAction$<R>(
    actionType: string,
    serviceFn: (tenants: string[]) => Observable<R>,
    successFn: (payload: R) => Action,
    failureFn: (payload: any) => Action
  ): Observable<any> {
    return createEffect(() =>
      this.actions$.pipe(
        ofType(actionType),
        switchMap(action =>
          serviceFn((action as any).tenants).pipe(
            map(successFn),
            catchError((error) => of(failureFn({ error })))
          )
        )
      )
    );
  }

  visitsTrends$ = this.createEffectForAction$(
    ComparisonActions.visitsTrends.type,
    (tenants: string[]) => this.comparisonService.getVisitsTrends(tenants),
    ({ clickTrends }) => ComparisonActions.visitsTrendsSuccess({ clickTrends }),
    (error) => ComparisonActions.visitsTrendsFailure({ error })
  );

  sessionTrends$ = this.createEffectForAction$(
    ComparisonActions.sessionTimeTrends.type,
    (tenants: string[]) => this.comparisonService.getSessionTrends(tenants),
    ({ sessionTrends }) => ComparisonActions.sessionTimeTrendsSuccess({ sessionTrends }),
    (error) => ComparisonActions.sessionTimeTrendsFailure({ error })
  );
}

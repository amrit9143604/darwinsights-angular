import { createAction, props } from '@ngrx/store';
import { SessionData, VisitsData } from 'src/app/common/interfaces/chartStates';

export const visitsTrends = createAction(
  '[Metric Comparison Component] VisitsTrends',
  props<{ tenants: string[] }>()
);
export const visitsTrendsSuccess = createAction(
  '[Metric Comparison Component] VisitsTrendsSuccess',
  props<{ clickTrends: VisitsData[] }>()
);
export const visitsTrendsFailure = createAction(
  '[Metric Comparison Component] VisitsTrendsFailure',
  props<{ error: string }>()
);

export const sessionTimeTrends = createAction(
  '[Metric Comparison Component] SessionTimeTrends',
  props<{ tenants: string[] }>()
);
export const sessionTimeTrendsSuccess = createAction(
  '[Metric Comparison Component] SessionTimeTrendsSuccess',
  props<{ sessionTrends: SessionData[] }>()
);
export const sessionTimeTrendsFailure = createAction(
  '[Metric Comparison Component] SessionTimeTrendsFailure',
  props<{ error: string }>()
);

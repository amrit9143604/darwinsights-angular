import { Component, OnInit } from '@angular/core';
import { ComparisonService } from './services/comparison.service';
import {
  metricOptions,
  timeArray,
  timeLabels,
} from 'src/app/common/interfaces/common';
import { Observable, Subject, Subscription, takeUntil, tap } from 'rxjs';
import { MetricsService } from '../services/metrics.service';
import { CookieService } from 'ngx-cookie-service';
import { metricData } from '../store/metrics.selectors';
import { Store, select } from '@ngrx/store';
import { setTenant, setTime } from '../store/metrics.actions';
import {
  ChartDataState,
  CompareChartPerformanceData,
  CompareChartVisitsData,
  LoadTimeChartData,
  SessionTimeChartData,
  VisitsChartData,
} from 'src/app/common/interfaces/chartStates';
import {  sessionTimeTrends, visitsTrends } from './store/comparison.actions';
import {
  selectSessionTimeTrends,
  selectVisitsTrends,
} from './store/comparison.selectors';

@Component({
  selector: 'app-comparison',
  templateUrl: './comparison.component.html',
  styleUrls: ['./comparison.component.scss'],
})
export class ComparisonComponent implements OnInit {
  loadingState = {
    visitsTrends: false,
    sessionTrends: false,
  };

  yKeys: string[] = [];
  yNames: string[] = [];
  chartData: any[] = [];

  visitsOptions$!: Observable<ChartDataState<VisitsChartData>>;
  sessionOptions$!: Observable<ChartDataState<SessionTimeChartData>>;

  xKey!: string;

  tenants$!: Observable<metricOptions[]>;
  selectedTenant!: metricOptions;

  time!: metricOptions[];
  selectedTime!: metricOptions;

  selectedTenantIds: string[] = [];

  visitsChartType: boolean = true;
  sessionChartType: boolean = true;
  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private comparisonService: ComparisonService,
    private metricService: MetricsService,
    private cookieService: CookieService,
    private store: Store
  ) {
    this.time = timeArray;
  }

  ngOnInit(): void {
    this.store
      .pipe(select(metricData), takeUntil(this.destroy$))
      .subscribe((state) => {
        this.selectedTime = {
          label: timeLabels[state.time] ?? state.time ?? '',
          value: state.time ?? '',
        };
      }),
      // this.fetchDataForSelectedTenants()
      takeUntil(this.destroy$)

    this.fetchTenants();
  }

  onTenantChange(tenantId: string) {
    if (this.selectedTenantIds.includes(tenantId)) {
      console.log("object");
      this.selectedTenantIds = this.selectedTenantIds.filter(
        (id) => id !== tenantId
      );
      const index = this.yKeys.indexOf(tenantId);
      if (index !== -1) {
        this.yKeys.splice(index, 1);
        this.yNames.splice(index, 1);
      }
    } else {
      this.selectedTenantIds = [...this.selectedTenantIds, tenantId];
      console.log('Selected Tenant IDs:', this.selectedTenantIds);
      this.yKeys.push(tenantId);
      this.yNames.push(tenantId);
    }
    this.fetchDataForSelectedTenants();

  }

  onTimeChange(value: string) {
    this.cookieService.set('TIME', value, 2, '/');
    this.store.dispatch(setTime({ time: value }));
    this.fetchDataForSelectedTenants();
  }

  private fetchTenants(): void {
    this.tenants$ = this.metricService
      .getCompareTenants()
      .pipe(takeUntil(this.destroy$));
  }
  private fetchDataForSelectedTenants(): void {
    // console.log(this.selectedTenantIds);
    this.store.dispatch(visitsTrends({ tenants: this.selectedTenantIds }));
    this.store.dispatch(sessionTimeTrends({ tenants: this.selectedTenantIds }));
    this.retrieveVisitsTrends();
    this.retrieveLoadTimeTrends();
  }

  private retrieveVisitsTrends(): void {
    this.visitsOptions$ = this.store.pipe(select(selectVisitsTrends));
    this.subscribeToLoadingState('visitsTrends', selectVisitsTrends);
  }

  private retrieveLoadTimeTrends(): void {
    this.sessionOptions$ = this.store.pipe(select(selectSessionTimeTrends));
    this.subscribeToLoadingState('sessionTrends', selectSessionTimeTrends);
  }

  private subscribeToLoadingState(
    prop: keyof (typeof ComparisonComponent)['prototype']['loadingState'],
    selector: any
  ): void {
    this.store
      .pipe(
        select(selector),
        tap((state: any) => (this.loadingState[prop] = state.loading)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  handleChartType(chartType: string) {
    switch (chartType) {
      case 'visits':
        this.visitsChartType = !this.visitsChartType;
        break;

      case 'session':
        this.sessionChartType = !this.sessionChartType;
        break;

      default:
        break;
    }
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

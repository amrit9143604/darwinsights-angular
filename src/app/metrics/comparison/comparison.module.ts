import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComparisonRoutingModule } from './comparison-routing.module';
import { ComparisonComponent } from './comparison.component';
import { MultiSelectModule } from 'primeng/multiselect';
import { SharedModule } from 'src/app/shared/shared.module';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { ComparisonEffects } from './store/comparison.effects';
import { comparisonReducer } from './store/comparison.reducers';
import { EffectsModule } from '@ngrx/effects';

@NgModule({
  declarations: [ComparisonComponent],
  imports: [
    CommonModule,
    ComparisonRoutingModule,
    MultiSelectModule,
    SharedModule,
    DropdownModule,
    FormsModule,
    StoreModule.forFeature('comparison', comparisonReducer),
    EffectsModule.forFeature([ComparisonEffects]),
  ],
})
export class ComparisonModule {}

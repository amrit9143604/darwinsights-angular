import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
  Observable,
  Subject,
  Subscription,
  catchError,
  map,
  takeUntil,
  throwError,
} from 'rxjs';
import { metricData } from '../../store/metrics.selectors';
import {
  COMPARE_TENANTS_CHART_SESSION_TRENDS,
  COMPARE_TENANTS_CHART_VISITS_TRENDS,
} from 'src/app/common/apis/constants';
import {
  CompareChartPerformanceData,
  CompareChartVisitsData,
  LoadTimeChartData,
  SessionTimeChartData,
  VisitsChartData,
} from 'src/app/common/interfaces/chartStates';

@Injectable({
  providedIn: 'root',
})
export class ComparisonService {
  private range!: string | null;
  private body!: { range: string | null };
  private destroy$: Subject<void> = new Subject<void>();

  constructor(private http: HttpClient, private store: Store) {
    this.store
      .pipe(select(metricData), takeUntil(this.destroy$))
      .subscribe((state) => {
        this.range = state.time;
        this.body = { range: this.range };
      });
  }

  private handleError(error: string) {
    console.error('An error occurred:', error);
    return throwError('Something went wrong');
  }

  private post(url: string, body: Object): Observable<Object> {
    return this.http.post(url, body);
  }

  getVisitsTrends(tenants: string[]): Observable<VisitsChartData> {
    const bodyWithTenants = { ...this.body, tenants: tenants };
    return this.post(COMPARE_TENANTS_CHART_VISITS_TRENDS, bodyWithTenants).pipe(
      map((response: any) => ({
        clickTrends: response.clickTrends,
      })),
      catchError(this.handleError)
    );
  }
  getSessionTrends(tenants: string[]): Observable<SessionTimeChartData> {
    const bodyWithTenants = { ...this.body, tenants: tenants };
    return this.post(
      COMPARE_TENANTS_CHART_SESSION_TRENDS,
      bodyWithTenants
    ).pipe(
      map((response: any) => ({
        sessionTrends: response.sessionTrends,
      })),
      catchError(this.handleError)
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

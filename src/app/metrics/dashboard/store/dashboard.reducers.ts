import { createReducer, on } from '@ngrx/store';
import {
  BrowserChartData,
  ChartDataState,
  LoadTimeChartData,
  ModuleSessionChartData,
  PlatformChartData,
  SessionTimeChartData,
  UniqueUsersChartData,
  VisitsChartData,
} from 'src/app/common/interfaces/chartStates';
import {
  CardDataState,
  CardAverageLoadTimeData,
  CardUsersData,
  CardUsersSessionsData,
  CardViewsData,
} from 'src/app/common/interfaces/cardStates';
import * as DashboardActions from './dashboard.actions';

interface CombinedState {
  totalViews: CardDataState<CardViewsData>;
  usersSessionTime: CardDataState<CardUsersSessionsData>;
  averageLoadTime: CardDataState<CardAverageLoadTimeData>;
  uniqueUsers: CardDataState<CardUsersData>;
  visitsTrends: ChartDataState<VisitsChartData>;
  uniqueUsersTrends: ChartDataState<UniqueUsersChartData>;
  loadTimeTrends: ChartDataState<LoadTimeChartData>;
  sessionTrends: ChartDataState<SessionTimeChartData>;
  platformTrends: ChartDataState<PlatformChartData>;
  browserTrends: ChartDataState<BrowserChartData>;
  moduleSessionTrends: ChartDataState<ModuleSessionChartData>;
}

const initialTotalViewsState: CardDataState<CardViewsData> = {
  data: { totalViews: null, percentageChange: null },
  loading: false,
  error: null,
};
const initialUniqueUsersState: CardDataState<CardUsersData> = {
  data: { totalUsers: null, percentageChange: null },
  loading: false,
  error: null,
};
const initialAverageLoadTimeState: CardDataState<CardAverageLoadTimeData> = {
  data: { averageLoadTime: null, percentageChange: null },
  loading: false,
  error: null,
};
const initialUserSessionsState: CardDataState<CardUsersSessionsData> = {
  data: { averageSessionTime: null, sessionPercentageChange: null },
  loading: false,
  error: null,
};
const initialVisitsTrendsState: ChartDataState<VisitsChartData> = {
  data: { clickTrends: [] },
  loading: false,
  error: null,
};
const initialUniqueUsersTrendsState: ChartDataState<UniqueUsersChartData> = {
  data: { userTrends: [] },
  loading: false,
  error: null,
};
const initialLoadTimeTrendsState: ChartDataState<LoadTimeChartData> = {
  data: { performanceTrends: [] },
  loading: false,
  error: null,
};

const initialSessionTimeTrendsState: ChartDataState<SessionTimeChartData> = {
  data: { sessionTrends: [] },
  loading: false,
  error: null,
};

const initialPlatformTrendsState: ChartDataState<PlatformChartData> = {
  data: { result: [] },
  loading: false,
  error: null,
};

const initialBrowserTrendsState: ChartDataState<BrowserChartData> = {
  data: { result: [] },
  loading: false,
  error: null,
};

const initialModuleSessionTrendsState: ChartDataState<ModuleSessionChartData> =
  {
    data: { result: [] },
    loading: false,
    error: null,
  };

const initialState: CombinedState = {
  averageLoadTime: initialAverageLoadTimeState,
  uniqueUsers: initialUniqueUsersState,
  totalViews: initialTotalViewsState,
  usersSessionTime: initialUserSessionsState,
  visitsTrends: initialVisitsTrendsState,
  uniqueUsersTrends: initialUniqueUsersTrendsState,
  loadTimeTrends: initialLoadTimeTrendsState,
  sessionTrends: initialSessionTimeTrendsState,
  platformTrends: initialPlatformTrendsState,
  browserTrends: initialBrowserTrendsState,
  moduleSessionTrends: initialModuleSessionTrendsState,
};

export const dashboardReducer = createReducer(
  initialState,
  on(DashboardActions.totalViews, (state) => ({
    ...state,
    totalViews: { ...state.totalViews, loading: true },
  })),
  on(
    DashboardActions.totalViewsSuccess,
    (state, { totalViews, percentageChange }) => {
      console.log('Total views updated:', { totalViews, percentageChange });
      return {
        ...state,
        totalViews: {
          ...state.totalViews,
          loading: false,
          data: { totalViews, percentageChange },
        },
      };
    }
  ),
  on(DashboardActions.totalViewsFailure, (state, { error }) => ({
    ...state,
    totalViews: {
      data: { totalViews: null, percentageChange: null },
      loading: false,
      error,
    },
  })),

  on(DashboardActions.uniqueUsers, (state) => ({
    ...state,
    uniqueUsers: { ...state.uniqueUsers, loading: true },
  })),
  on(
    DashboardActions.uniqueUsersSuccess,
    (state, { totalUsers, percentageChange }) => ({
      ...state,
      uniqueUsers: {
        ...state.uniqueUsers,
        loading: false,
        data: { totalUsers, percentageChange },
      },
    })
  ),
  on(DashboardActions.uniqueUsersFailure, (state, { error }) => ({
    ...state,
    uniqueUsers: {
      data: { totalUsers: null, percentageChange: null },
      loading: false,
      error,
    },
  })),

  on(DashboardActions.averageLoadTime, (state) => ({
    ...state,
    averageLoadTime: { ...state.averageLoadTime, loading: true },
  })),
  on(
    DashboardActions.averageLoadTimeSuccess,
    (state, { averageLoadTime, percentageChange }) => ({
      ...state,
      averageLoadTime: {
        ...state.averageLoadTime,
        loading: false,
        data: { averageLoadTime, percentageChange },
      },
    })
  ),
  on(DashboardActions.averageLoadTimeFailure, (state, { error }) => ({
    ...state,
    averageLoadTime: {
      data: { averageLoadTime: null, percentageChange: null },
      loading: false,
      error,
    },
  })),

  on(DashboardActions.usersSessionTime, (state) => ({
    ...state,
    usersSessionTime: { ...state.usersSessionTime, loading: true },
  })),
  on(
    DashboardActions.usersSessionTimeSuccess,
    (state, { averageSessionTime, sessionPercentageChange }) => ({
      ...state,
      usersSessionTime: {
        ...state.usersSessionTime,
        loading: false,
        data: { averageSessionTime, sessionPercentageChange },
      },
    })
  ),
  on(DashboardActions.usersSessionTimeFailure, (state, { error }) => ({
    ...state,
    usersSessionTime: {
      data: { averageSessionTime: null, sessionPercentageChange: null },
      loading: false,
      error,
    },
  })),

  on(DashboardActions.visitsTrends, (state) => ({
    ...state,
    visitsTrends: { ...state.visitsTrends, loading: true },
  })),
  on(DashboardActions.visitsTrendsSuccess, (state, { clickTrends }) => ({
    ...state,
    visitsTrends: {
      ...state.visitsTrends,
      loading: false,
      data: { clickTrends },
    },
  })),
  on(DashboardActions.visitsTrendsFailure, (state, { error }) => ({
    ...state,
    visitsTrends: { data: { clickTrends: [] }, loading: false, error },
  })),

  on(DashboardActions.uniqueUsersTrends, (state) => ({
    ...state,
    uniqueUsersTrends: { ...state.uniqueUsersTrends, loading: true },
  })),
  on(DashboardActions.uniqueUsersTrendsSuccess, (state, { userTrends }) => ({
    ...state,
    uniqueUsersTrends: {
      ...state.uniqueUsersTrends,
      loading: false,
      data: { userTrends },
    },
  })),
  on(DashboardActions.uniqueUsersTrendsFailure, (state, { error }) => ({
    ...state,
    uniqueUsersTrends: { data: { userTrends: [] }, loading: false, error },
  })),

  on(DashboardActions.loadTimeTrends, (state) => ({
    ...state,
    loadTimeTrends: { ...state.loadTimeTrends, loading: true },
  })),
  on(
    DashboardActions.loadTimeTrendsSuccess,
    (state, { performanceTrends }) => ({
      ...state,
      loadTimeTrends: {
        ...state.loadTimeTrends,
        loading: false,
        data: { performanceTrends },
      },
    })
  ),
  on(DashboardActions.loadTimeTrendsFailure, (state, { error }) => ({
    ...state,
    loadTimeTrends: { data: { performanceTrends: [] }, loading: false, error },
  })),

  on(DashboardActions.sessionTimeTrends, (state) => ({
    ...state,
    sessionTrends: { ...state.sessionTrends, loading: true },
  })),
  on(DashboardActions.sessionTimeTrendsSuccess, (state, { sessionTrends }) => ({
    ...state,
    sessionTrends: {
      ...state.sessionTrends,
      loading: false,
      data: { sessionTrends },
    },
  })),
  on(DashboardActions.sessionTimeTrendsFailure, (state, { error }) => ({
    ...state,
    sessionTrends: { data: { sessionTrends: [] }, loading: false, error },
  })),

  on(DashboardActions.platformTrends, (state) => ({
    ...state,
    platformTrends: { ...state.platformTrends, loading: true },
  })),
  on(DashboardActions.platformTrendsSuccess, (state, { result }) => ({
    ...state,
    platformTrends: {
      ...state.platformTrends,
      loading: false,
      data: { result },
    },
  })),
  on(DashboardActions.platformTrendsFailure, (state, { error }) => ({
    ...state,
    platformTrends: { data: { result: [] }, loading: false, error },
  })),

  on(DashboardActions.browserTrends, (state) => ({
    ...state,
    browserTrends: { ...state.browserTrends, loading: true },
  })),
  on(DashboardActions.browserTrendsSuccess, (state, { result }) => ({
    ...state,
    browserTrends: {
      ...state.browserTrends,
      loading: false,
      data: { result },
    },
  })),
  on(DashboardActions.browserTrendsFailure, (state, { error }) => ({
    ...state,
    browserTrends: { data: { result: [] }, loading: false, error },
  })),

  on(DashboardActions.moduleSessionTrends, (state) => ({
    ...state,
    moduleSessionTrends: { ...state.moduleSessionTrends, loading: true },
  })),
  on(DashboardActions.moduleSessionTrendsSuccess, (state, { result }) => ({
    ...state,
    moduleSessionTrends: {
      ...state.moduleSessionTrends,
      loading: false,
      data: { result },
    },
  })),
  on(DashboardActions.moduleSessionTrendsFailure, (state, { error }) => ({
    ...state,
    moduleSessionTrends: { data: { result: [] }, loading: false, error },
  }))
);

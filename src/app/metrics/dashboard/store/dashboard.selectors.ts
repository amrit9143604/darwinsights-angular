import { createSelector, createFeatureSelector } from '@ngrx/store';
import { CardAverageLoadTimeData, CardDataState, CardUsersData, CardUsersSessionsData, CardViewsData } from 'src/app/common/interfaces/cardStates';
import { BrowserChartData, ChartDataState, LoadTimeChartData, ModuleSessionChartData, PlatformChartData, SessionTimeChartData, UniqueUsersChartData, VisitsChartData } from 'src/app/common/interfaces/chartStates';
import { DashboardState } from 'src/app/common/interfaces/commonStates';


const selectDashboardState = createFeatureSelector<DashboardState>('dashboard');

export const selectTotalViews = createSelector(
  selectDashboardState,
  (state: DashboardState): CardDataState<CardViewsData> => state.totalViews
);

export const selectUniqueUsers = createSelector(
  selectDashboardState,
  (state: DashboardState): CardDataState<CardUsersData> => state.uniqueUsers
);

export const selectAverageLoadTime = createSelector(
  selectDashboardState,
  (state: DashboardState): CardDataState<CardAverageLoadTimeData> => state.averageLoadTime
);

export const selectUsersSessionTime = createSelector(
  selectDashboardState,
  (state: DashboardState): CardDataState<CardUsersSessionsData> => state.usersSessionTime
);

export const selectVisitsTrends = createSelector(
  selectDashboardState,
  (state: DashboardState): ChartDataState<VisitsChartData> => state.visitsTrends
);

export const selectUniqueUsersTrends = createSelector(
  selectDashboardState,
  (state: DashboardState): ChartDataState<UniqueUsersChartData> => state.uniqueUsersTrends
);

export const selectLoadTimeTrends = createSelector(
  selectDashboardState,
  (state: DashboardState): ChartDataState<LoadTimeChartData> =>{ 
    // console.log('Load Time Trends State:', state.loadTimeTrends);

    return state.loadTimeTrends}
);

export const selectSessionTimeTrends = createSelector(
  selectDashboardState,
  (state: DashboardState): ChartDataState<SessionTimeChartData> => {
    // console.log('Session Time Trends State:', state.sessionTrends);
    return state.sessionTrends;
  }
);

export const selectPlatformTrends = createSelector(
  selectDashboardState,
  (state: DashboardState): ChartDataState<PlatformChartData> => {
    return state.platformTrends;
  }
);

export const selectBrowserTrends = createSelector(
  selectDashboardState,
  (state: DashboardState): ChartDataState<BrowserChartData> => {
    return state.browserTrends;
  }
);

export const selectModuleSessionTrends = createSelector(
  selectDashboardState,
  (state: DashboardState): ChartDataState<ModuleSessionChartData> => {
    return state.moduleSessionTrends;
  }
);


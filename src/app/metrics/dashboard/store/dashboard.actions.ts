import { createAction, props } from '@ngrx/store';
import { BrowserData, LoadTimeData, ModuleSessionData, PlatformData, SessionData, UsersData, VisitsData } from 'src/app/common/interfaces/chartStates';


export const totalViews = createAction(
  '[Metric Dashboard Component] TotalViews'
);
export const totalViewsSuccess = createAction(
  '[Metric Dashboard Component] TotalViewsSuccess',
  props<{ totalViews: number | null; percentageChange: number | null }>()
);
export const totalViewsFailure = createAction(
  '[Metric Dashboard Component] TotalViewsFailure',
  props<{ error: string }>()
);

export const uniqueUsers = createAction(
  '[Metric Dashboard Component] UniqueUsers'
);
export const uniqueUsersSuccess = createAction(
  '[Metric Dashboard Component] UniqueUsersSuccess',
  props<{ totalUsers: number | null; percentageChange: number | null }>()
);
export const uniqueUsersFailure = createAction(
  '[Metric Dashboard Component] UniqueUsersFailure',
  props<{ error: string }>()
);

export const averageLoadTime = createAction(
  '[Metric Dashboard Component] AverageLoadTime'
);
export const averageLoadTimeSuccess = createAction(
  '[Metric Dashboard Component] AverageLoadTimeSuccess',
  props<{ averageLoadTime: number | null; percentageChange: number | null }>()
);
export const averageLoadTimeFailure = createAction(
  '[Metric Dashboard Component] AverageLoadTimeFailure',
  props<{ error: string }>()
);

export const usersSessionTime = createAction(
  '[Metric Dashboard Component] UsersSessionTime'
);
export const usersSessionTimeSuccess = createAction(
  '[Metric Dashboard Component] UsersSessionTimeSuccess',
  props<{
    averageSessionTime: number | null;
    sessionPercentageChange: number | null;
  }>()
);
export const usersSessionTimeFailure = createAction(
  '[Metric Dashboard Component] UsersSessionTimeFailure',
  props<{ error: string }>()
);

export const visitsTrends = createAction(
  '[Metric Dashboard Component] VisitsTrends'
);
export const visitsTrendsSuccess = createAction(
  '[Metric Dashboard Component] VisitsTrendsSuccess',
  props<{ clickTrends: VisitsData[] }>()
);
export const visitsTrendsFailure = createAction(
  '[Metric Dashboard Component] VisitsTrendsFailure',
  props<{ error: string }>()
);

export const uniqueUsersTrends = createAction(
  '[Metric Dashboard Component] UniqueUsersTrends'
);
export const uniqueUsersTrendsSuccess = createAction(
  '[Metric Dashboard Component] UniqueUsersTrendsSuccess',
  props<{ userTrends: UsersData[] }>()
);
export const uniqueUsersTrendsFailure = createAction(
  '[Metric Dashboard Component] UniqueUsersTrendsFailure',
  props<{ error: string }>()
);


export const loadTimeTrends = createAction(
  '[Metric Dashboard Component] LoadTimeTrends'
);
export const loadTimeTrendsSuccess = createAction(
  '[Metric Dashboard Component] LoadTimeTrendsSuccess',
  props<{ performanceTrends: LoadTimeData[] }>()
);
export const loadTimeTrendsFailure = createAction(
  '[Metric Dashboard Component] LoadTimeTrendsFailure',
  props<{ error: string }>()
);


export const sessionTimeTrends = createAction(
  '[Metric Dashboard Component] SessionTimeTrends'
);
export const sessionTimeTrendsSuccess = createAction(
  '[Metric Dashboard Component] SessionTimeTrendsSuccess',
  props<{ sessionTrends: SessionData[] }>()
);
export const sessionTimeTrendsFailure = createAction(
  '[Metric Dashboard Component] SessionTimeTrendsFailure',
  props<{ error: string }>()
);

export const platformTrends = createAction(
  '[Metric Dashboard Component] PlatformTrends'
);
export const platformTrendsSuccess = createAction(
  '[Metric Dashboard Component] PlatformTrendsSuccess',
  props<{ result: PlatformData[] }>()
);
export const platformTrendsFailure = createAction(
  '[Metric Dashboard Component] PlatformTrendsFailure',
  props<{ error: string }>()
);


export const browserTrends = createAction(
  '[Metric Dashboard Component] BrowserTrends'
);
export const browserTrendsSuccess = createAction(
  '[Metric Dashboard Component] BrowserTrendsSuccess',
  props<{ result: BrowserData[] }>()
);
export const browserTrendsFailure = createAction(
  '[Metric Dashboard Component] BrowserTrendsFailure',
  props<{ error: string }>()
);

export const moduleSessionTrends = createAction(
  '[Metric Dashboard Component] ModuleSessionTrends'
);
export const moduleSessionTrendsSuccess = createAction(
  '[Metric Dashboard Component] ModuleSessionTrendsSuccess',
  props<{ result: ModuleSessionData[] }>()
);
export const moduleSessionTrendsFailure = createAction(
  '[Metric Dashboard Component] ModuleSessionTrendsFailure',
  props<{ error: string }>()
);

import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { map, catchError, mergeMap, switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { DashboardService } from '../services/dashboard.service';
import * as DashboardActions from './dashboard.actions';
import { Action } from '@ngrx/store';

@Injectable()
export class DashboardEffects {
  constructor(
    private actions$: Actions,
    private dashboardService: DashboardService
  ) {}

  private createEffectForAction$<R>(
    actionType: string,
    serviceFn: () => Observable<R>,
    successFn: (payload: R) => Action,
    failureFn: (payload: any) => Action
  ): Observable<Action> {
    return createEffect(() =>
      this.actions$.pipe(
        ofType(actionType),
        switchMap (() =>
          serviceFn().pipe(
            map(successFn),
            catchError((error) => of(failureFn({ error })))
          )
        )
      )
    );
  }

  totalViews$ = this.createEffectForAction$(
    DashboardActions.totalViews.type,
    () => this.dashboardService.getTotalViews(),
    (payload) => DashboardActions.totalViewsSuccess(payload),
    (payload) => DashboardActions.totalViewsFailure(payload)
  );

  uniqueUsers$ = this.createEffectForAction$(
    DashboardActions.uniqueUsers.type,
    () => this.dashboardService.getUniqueUsers(),
    (payload) => DashboardActions.uniqueUsersSuccess(payload),
    (payload) => DashboardActions.uniqueUsersFailure(payload)
  );

  averageLoadTime$ = this.createEffectForAction$(
    DashboardActions.averageLoadTime.type,
    () => this.dashboardService.getAverageLoadTime(),
    (payload) => DashboardActions.averageLoadTimeSuccess(payload),
    (payload) => DashboardActions.averageLoadTimeFailure(payload)
  );

  usersSessionTime$ = this.createEffectForAction$(
    DashboardActions.usersSessionTime.type,
    () => this.dashboardService.getUsersSessionTime(),
    (payload) => DashboardActions.usersSessionTimeSuccess(payload),
    (payload) => DashboardActions.usersSessionTimeFailure(payload)
  );

  visitsTrends$ = this.createEffectForAction$(
    DashboardActions.visitsTrends.type,
    () => this.dashboardService.getVisitsTrends(),
    (payload) => DashboardActions.visitsTrendsSuccess(payload),
    (payload) => DashboardActions.visitsTrendsFailure(payload)
  );

  uniqueUsersTrends$ = this.createEffectForAction$(
    DashboardActions.uniqueUsersTrends.type,
    () => this.dashboardService.getUniqueUsersTrends(),
    (payload) => DashboardActions.uniqueUsersTrendsSuccess(payload),
    (payload) => DashboardActions.uniqueUsersTrendsFailure(payload)
  );

  loadTimeTrends$ = this.createEffectForAction$(
    DashboardActions.loadTimeTrends.type,
    () => this.dashboardService.getLoadTimeTrends(),
    (payload) => DashboardActions.loadTimeTrendsSuccess(payload),
    (payload) => DashboardActions.loadTimeTrendsFailure(payload)
  );

  sessionTimeTrends$ = this.createEffectForAction$(
    DashboardActions.sessionTimeTrends.type,
    () => this.dashboardService.getSessionTimeTrends(),
    (payload) => DashboardActions.sessionTimeTrendsSuccess(payload),
    (payload) => DashboardActions.sessionTimeTrendsFailure(payload)
  );

  platformTrends$ = this.createEffectForAction$(
    DashboardActions.platformTrends.type,
    () => this.dashboardService.getPlatformTrends(),
    (payload) => DashboardActions.platformTrendsSuccess(payload),
    (payload) => DashboardActions.platformTrendsFailure(payload)
  );

  browserTrends$ = this.createEffectForAction$(
    DashboardActions.browserTrends.type,
    () => this.dashboardService.getBrowserTrends(),
    (payload) => DashboardActions.browserTrendsSuccess(payload),
    (payload) => DashboardActions.browserTrendsFailure(payload)
  );

  moduleSessionTrends$ = this.createEffectForAction$(
    DashboardActions.moduleSessionTrends.type,
    () => this.dashboardService.getModuleSessionTrends(),
    (payload) => DashboardActions.moduleSessionTrendsSuccess(payload),
    (payload) => DashboardActions.moduleSessionTrendsFailure(payload)
  );
}

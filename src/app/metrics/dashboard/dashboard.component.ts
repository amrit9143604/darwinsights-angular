import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
  selectAverageLoadTime,
  selectBrowserTrends,
  selectLoadTimeTrends,
  selectModuleSessionTrends,
  selectPlatformTrends,
  selectSessionTimeTrends,
  selectTotalViews,
  selectUniqueUsers,
  selectUniqueUsersTrends,
  selectUsersSessionTime,
  selectVisitsTrends,
} from './store/dashboard.selectors';
import {
  BrowserChartData,
  ChartDataState,
  LoadTimeChartData,
  ModuleSessionChartData,
  PlatformChartData,
  SessionTimeChartData,
  UniqueUsersChartData,
  VisitsChartData,
} from 'src/app/common/interfaces/chartStates';
import { Observable, Subject, map, tap, takeUntil } from 'rxjs';
import { AgChartOptions } from 'ag-charts-community';
import { totalViews, uniqueUsers, averageLoadTime, usersSessionTime, visitsTrends, uniqueUsersTrends, loadTimeTrends, sessionTimeTrends, platformTrends, browserTrends, moduleSessionTrends } from './store/dashboard.actions';
import { DashboardState } from 'src/app/common/interfaces/commonStates';
import { FormsModule } from '@angular/forms';
import { DashboardService } from './services/dashboard.service';
import { metricOptions, timeArray, timeLabels } from 'src/app/common/interfaces/common';
import { CookieService } from 'ngx-cookie-service';
import { setData, setTenant, setTime } from '../store/metrics.actions';
import { metricData } from '../store/metrics.selectors';
import { MetricsService } from '../services/metrics.service';
import { MetricsState } from '../store/metrics.reducers';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  loadingState = {
    views: false,
    users: false,
    avgTime: false,
    session: false,
    visitsTrends: false,
    uniqueUsersTrends: false,
    loadTimeTrends: false,
    sessionTimeTrends: false,
    platformTrends: false,
    browserTrends: false,
    moduleSessionTrends: false,
  };

  tenants$!: Observable<metricOptions[]>;
  selectedTenant!: metricOptions;

  time!: metricOptions[];
  selectedTime!: metricOptions;

  totalViews$!: Observable<number | null>;
  totalViewsPercentage$!: Observable<number | null>;
  totalUsers$!: Observable<number | null>;
  totalUsersPercentage$!: Observable<number | null>;
  averageLoadTime$!: Observable<number | null>;
  loadTimePercentage$!: Observable<number | null>;
  usersSessionTime$!: Observable<number | null>;
  usersSessionPercentage$!: Observable<number | null>;
  visitsOptions$!: Observable<ChartDataState<VisitsChartData>>;
  uniqueUsersOptions$!: Observable<ChartDataState<UniqueUsersChartData>>;
  loadTimeOptions$!: Observable<ChartDataState<LoadTimeChartData>>;
  sessionTimeOptions$!: Observable<ChartDataState<SessionTimeChartData>>;
  platformOptions$!: Observable<ChartDataState<PlatformChartData>>;
  browserOptions$!: Observable<ChartDataState<BrowserChartData>>;
  moduleSessionOptions$!: Observable<ChartDataState<ModuleSessionChartData>>;

  visitsChartType = true;
  uniqueUsersChartType = true;
  loadTimeChartType = true;
  sessionTimeChartType = true;

  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private store: Store<MetricsState>,
    private metricService: MetricsService,
    private cookieService: CookieService
  ) {
    this.time = timeArray;
  }

  ngOnInit(): void {
    this.store.pipe(select(metricData), takeUntil(this.destroy$)).subscribe((state) => {
      this.selectedTenant = {
        label: state.tenant ?? '',
        value: state.tenant ?? '',
      };
      this.selectedTime = {
        label: timeLabels[state.time] ?? state.time ?? '',
        value: state.time ?? '',
      };
    });

    this.fetchTenants();
    this.dispatchActions();
    this.retrieveData();
  }

  onTenantChange(value: string) {
    this.cookieService.set('TENANT', value, 2, '/');
    this.store.dispatch(setTenant({ tenant: value }));
    this.dispatchActions();
    this.retrieveData();
  }

  onTimeChange(value: string) {
    this.cookieService.set('TIME', value, 2, '/');
    this.store.dispatch(setTime({ time: value }));
    this.dispatchActions();
    this.retrieveData();
  }

  private dispatchActions(): void {
    this.store.dispatch(totalViews());
    this.store.dispatch(uniqueUsers());
    this.store.dispatch(averageLoadTime());
    this.store.dispatch(usersSessionTime());
    this.store.dispatch(visitsTrends());
    this.store.dispatch(uniqueUsersTrends());
    this.store.dispatch(loadTimeTrends());
    this.store.dispatch(sessionTimeTrends());
    this.store.dispatch(platformTrends());
    this.store.dispatch(browserTrends());
    this.store.dispatch(moduleSessionTrends());
  }

  private fetchTenants(): void {
    this.tenants$ = this.metricService.getAllTenants().pipe(
      takeUntil(this.destroy$)
    );
  }
  
  private retrieveData(): void {
    this.retrieveTotalViews();
    this.retrieveUniqueUsers();
    this.retrieveAverageLoadTime();
    this.retrieveUsersSessionTime();
    this.retrieveVisitsTrends();
    this.retrieveUniqueUsersTrends();
    this.retrieveLoadTimeTrends();
    this.retrieveSessionTimeTrends();
    this.retrievePlatformTrends();
    this.retrieveBrowserTrends();
    this.retrieveModuleSessionTrends()
  }

  private retrieveTotalViews(): void {
    this.totalViews$ = this.store.pipe(
      select(selectTotalViews),
      map((state) => state.data.totalViews)
    );
    this.totalViewsPercentage$ = this.store.pipe(
      select(selectTotalViews),
      map((state) => state.data.percentageChange)
    );
    this.subscribeToLoadingState('views', selectTotalViews);
  }

  private retrieveUniqueUsers(): void {
    this.totalUsers$ = this.store.pipe(
      select(selectUniqueUsers), tap((data) => console.log(data)),
      map((state) => state.data.totalUsers)
    );
    this.totalUsersPercentage$ = this.store.pipe(
      select(selectUniqueUsers),
      map((state) => state.data.percentageChange)
    );
    this.subscribeToLoadingState('users', selectUniqueUsers);
  }

  private retrieveAverageLoadTime(): void {
    this.averageLoadTime$ = this.store.pipe(
      select(selectAverageLoadTime),
      map((state) => state.data.averageLoadTime)
    );
    this.loadTimePercentage$ = this.store.pipe(
      select(selectAverageLoadTime),
      map((state) => state.data.percentageChange)
    );
    this.subscribeToLoadingState('avgTime', selectAverageLoadTime);
  }

  private retrieveUsersSessionTime(): void {
    this.usersSessionTime$ = this.store.pipe(
      select(selectUsersSessionTime),
      map((state) => state.data.averageSessionTime)
    );
    this.usersSessionPercentage$ = this.store.pipe(
      select(selectUsersSessionTime),
      map((state) => state.data.sessionPercentageChange)
    );
    this.subscribeToLoadingState('session', selectUsersSessionTime);
  }

  private retrieveVisitsTrends(): void {
    this.visitsOptions$ = this.store.pipe(select(selectVisitsTrends));
    this.subscribeToLoadingState('visitsTrends', selectVisitsTrends);
  }

  private retrieveUniqueUsersTrends(): void {
    this.uniqueUsersOptions$ = this.store.pipe(select(selectUniqueUsersTrends));
    this.subscribeToLoadingState('uniqueUsersTrends', selectUniqueUsersTrends);
  }

  private retrieveLoadTimeTrends(): void {
    this.loadTimeOptions$ = this.store.pipe(select(selectLoadTimeTrends));
    this.subscribeToLoadingState('loadTimeTrends', selectLoadTimeTrends);
  }

  private retrieveSessionTimeTrends(): void {
    this.sessionTimeOptions$ = this.store.pipe(select(selectSessionTimeTrends));
    this.subscribeToLoadingState('sessionTimeTrends', selectSessionTimeTrends);
  }

  private retrievePlatformTrends(): void {
    this.platformOptions$ = this.store.pipe(select(selectPlatformTrends));
    this.subscribeToLoadingState('platformTrends', selectPlatformTrends);
  }

  private retrieveBrowserTrends(): void {
    this.browserOptions$ = this.store.pipe(select(selectBrowserTrends));
    this.subscribeToLoadingState('browserTrends', selectBrowserTrends);
  }

  private retrieveModuleSessionTrends(): void {
    this.moduleSessionOptions$ = this.store.pipe(
      select(selectModuleSessionTrends)
    );
    this.subscribeToLoadingState(
      'moduleSessionTrends',
      selectModuleSessionTrends
    );
  }

  handleChartType(chartType: string) {
    switch (chartType) {
      case 'visits':
        this.visitsChartType = !this.visitsChartType;
        break;
      case 'uniqueUsers':
        this.uniqueUsersChartType = !this.uniqueUsersChartType;
        break;
      case 'loadTime':
        this.loadTimeChartType = !this.loadTimeChartType;
        break;
      case 'sessionTime':
        this.sessionTimeChartType = !this.sessionTimeChartType;
        break;
      default:
        break;
    }
  }

  private subscribeToLoadingState(
    prop: keyof (typeof DashboardComponent)['prototype']['loadingState'],
    selector: any
  ): void {
    this.store
      .pipe(
        select(selector),
        tap((state: any) => (this.loadingState[prop] = state.loading)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subject, Subscription, concat, of, throwError } from 'rxjs';
import { catchError, map, startWith, takeUntil, tap } from 'rxjs/operators';
import {
  DASHBOARD_CARD_AVERAGE_LOAD_TIME,
  DASHBOARD_CARD_AVERAGE_SESSION_TIME,
  DASHBOARD_CARD_TOTAL_VIEWS,
  DASHBOARD_CARD_UNIQUE_USERS,
  DASHBOARD_CHART_BROWSER_COUNT,
  DASHBOARD_CHART_MODULE_SESSION_TIME,
  DASHBOARD_CHART_PERFORMANCE_TRENDS,
  DASHBOARD_CHART_PLATFORM_COUNT,
  DASHBOARD_CHART_SESSION_TRENDS,
  DASHBOARD_CHART_USERS_TRENDS,
  DASHBOARD_CHART_VISITS_TRENDS,
} from 'src/app/common/apis/constants';
import { metricData } from '../../store/metrics.selectors';
import { CardAverageLoadTimeData, CardUsersData, CardUsersSessionsData, CardViewsData } from 'src/app/common/interfaces/cardStates';
import { BrowserChartData, LoadTimeChartData, ModuleSessionChartData, PlatformChartData, SessionTimeChartData, UniqueUsersChartData, VisitsChartData } from 'src/app/common/interfaces/chartStates';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  private tenantId!: string | null;
  private range!: string | null;
  private body!: { tenantId: string | null, range: string | null};
  private destroy$: Subject<void> = new Subject<void>();


  constructor(private http: HttpClient, private store: Store) {
    this.store.pipe(select(metricData), takeUntil(this.destroy$)).subscribe((state) => {
      this.tenantId = state.tenant;
      this.range = state.time;
      this.body = { tenantId: this.tenantId, range: this.range };
    });
  }


  private handleError(error: string) {
    console.error('An error occurred:', error);
    return throwError('Something went wrong');
  }

  private post(url: string): Observable<Object> {
    return this.http.post(url, this.body);
  }
  

  getTotalViews(): Observable<CardViewsData> {
    return this.post(DASHBOARD_CARD_TOTAL_VIEWS).pipe(
      map((response: any) => ({
        totalViews: response.totalViews,
        percentageChange: response.percentageChange,
      })),
      catchError(this.handleError)
    );
  }

  getUniqueUsers(): Observable<CardUsersData> {
    return this.post(DASHBOARD_CARD_UNIQUE_USERS).pipe(
      map((response: any) => ({
        totalUsers: response.totalUsers,
        percentageChange: response.percentageChange,
      })),
      catchError(this.handleError)
    );
  }

  getAverageLoadTime(): Observable<CardAverageLoadTimeData> {
    return this.post(DASHBOARD_CARD_AVERAGE_LOAD_TIME).pipe(
      map((response: any) => ({
        averageLoadTime: response.averageLoadTime,
        percentageChange: response.percentageChange,
      })),
      catchError(this.handleError)
    );
  }

  getUsersSessionTime(): Observable<CardUsersSessionsData> {
    return this.post(DASHBOARD_CARD_AVERAGE_SESSION_TIME).pipe(
      map((response: any) => ({
        averageSessionTime: response.averageSessionTime,
        sessionPercentageChange: response.sessionPercentageChange,
      })),
      catchError(this.handleError)
    );
  }

  getVisitsTrends(): Observable<VisitsChartData> {
    return this.post(DASHBOARD_CHART_VISITS_TRENDS).pipe(
      map((response: any) => ({
        clickTrends: response.clickTrends,
      })),
      catchError(this.handleError)
    );
  }

  getUniqueUsersTrends(): Observable<UniqueUsersChartData> {
    return this.post(DASHBOARD_CHART_USERS_TRENDS).pipe(
      map((response: any) => ({
        userTrends: response.userTrends,
      })),
      catchError(this.handleError)
    );
  }

  getLoadTimeTrends(): Observable<LoadTimeChartData> {
    return this.post(DASHBOARD_CHART_PERFORMANCE_TRENDS).pipe(
      map((response: any) => ({
        performanceTrends: response.performanceTrends,
      })),
      catchError(this.handleError)
    );
  }

  getSessionTimeTrends(): Observable<SessionTimeChartData> {
    return this.post(DASHBOARD_CHART_SESSION_TRENDS).pipe(
      map((response: any) => ({
        sessionTrends: response.sessionTrends,
      })),
      catchError(this.handleError)
    );
  }

  getPlatformTrends(): Observable<PlatformChartData> {
    return this.post(DASHBOARD_CHART_PLATFORM_COUNT).pipe(
      map((response: any) => ({
        result: response.platformCount.result,
      })),
      catchError(this.handleError)
    );
  }

  getBrowserTrends(): Observable<BrowserChartData> {
    return this.post(DASHBOARD_CHART_BROWSER_COUNT).pipe(
      map((response: any) => ({
        result: response.browserCount.result,
      })),
      catchError(this.handleError)
    );
  }

  getModuleSessionTrends(): Observable<ModuleSessionChartData> {
    return this.post(DASHBOARD_CHART_MODULE_SESSION_TIME).pipe(
      map((response: any) => ({
        result: response.moduleSessionsTime.result,
      })),
      catchError(this.handleError)
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

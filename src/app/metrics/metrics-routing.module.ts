import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MetricsComponent } from './metrics.component';

const routes: Routes = [
  {
    path: 'metrics',
    redirectTo: 'metrics/dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: MetricsComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      },
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
        title: 'Dashboard | Darwinsights',
      },
      {
        path: 'visits',
        loadChildren: () =>
          import('./visits/visits.module').then((m) => m.VisitsModule),
        title: 'Visits Events | Darwinsights',
      },
      {
        path: 'performance',
        loadChildren: () =>
          import('./performance/performance.module').then(
            (m) => m.PerformanceModule
          ),
        title: 'Performance Events | Darwinsights',
      },
      {
        path: 'sessions',
        loadChildren: () =>
          import('./sessions/sessions.module').then((m) => m.SessionsModule),
        title: 'User Sessions | Darwinsights',
      },
      {
        path: 'comparison',
        loadChildren: () =>
          import('./comparison/comparison.module').then(
            (m) => m.ComparisonModule
          ),
        title: 'Compare Tenants | Darwinsights',
      },
      {
        path: 'module-analytics',
        loadChildren: () =>
          import('./module-analytics/module-analytics.module').then(
            (m) => m.ModuleAnalyticsModule
          ),
        title: 'Module Analytics | Darwinsights',
      },
      {
        path: 'user-analytics',
        loadChildren: () =>
          import('./user-analytics/user-analytics.module').then(
            (m) => m.UserAnalyticsModule
          ),
        title: 'User Analytics | Darwinsights',
      },
      {
        path: 'reports',
        loadChildren: () =>
          import('./reports/reports.module').then((m) => m.ReportsModule),
        title: 'Reports | Darwinsights',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MetricsRoutingModule {}

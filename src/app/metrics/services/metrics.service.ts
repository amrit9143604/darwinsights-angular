import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { metricData } from '../store/metrics.selectors';
import {
  Observable,
  Subject,
  Subscription,
  catchError,
  map,
  takeUntil,
  throwError,
} from 'rxjs';
import {
  OVERVIEW_ALL_CLIENTS,
  OVERVIEW_ALL_MODULES,
} from 'src/app/common/apis/constants';
import { metricOptions } from 'src/app/common/interfaces/common';

@Injectable({
  providedIn: 'root',
})
export class MetricsService implements OnDestroy {
  private tenantId!: string | null;
  private range!: string | null;
  private body!: { tenantId: string | null; range: string | null };
  private destroy$: Subject<void> = new Subject<void>();

  constructor(private http: HttpClient, private store: Store) {
    this.store
      .pipe(select(metricData), takeUntil(this.destroy$))
      .subscribe((state) => {
        this.tenantId = state.tenant;
        this.range = state.time;
        this.body = { tenantId: this.tenantId, range: this.range };
      });
  }

  private handleError(error: string) {
    console.error('An error occurred:', error);
    return throwError('Something went wrong');
  }

  private post(url: string): Observable<Object> {
    return this.http.post(url, this.body);
  }

  getAllTenants(): Observable<metricOptions[]> {
    return this.post(OVERVIEW_ALL_CLIENTS).pipe(
      map((response: any) => {
        const mappedResponse = response.map((item: any) => ({
          label: item ? item.toUpperCase() : null,
          value: item ? item.toUpperCase() : null,
        }));
        mappedResponse.unshift({
          label: 'OVERALL',
          value: null,
        });
        return mappedResponse;
      }),
      catchError(this.handleError)
    );
  }

  getAllModules(): Observable<metricOptions[]> {
    return this.post(OVERVIEW_ALL_MODULES).pipe(
      // tap((response: any) => console.log('Response from getAllTenants:', response)),
      map((response: any) =>
        response.map((item: any) => ({
          label: item ? item.toUpperCase() : null,
          value: item ? item : null,
        }))
      ),
      // tap((response: any) => console.log('Response from getAllTenants:', response)),
      catchError(this.handleError)
    );
  }
  getCompareTenants(): Observable<metricOptions[]> {
    return this.post(OVERVIEW_ALL_CLIENTS).pipe(
      // tap((response: any) => console.log('Response from getAllTenants:', response)),
      map((response: any) =>
        response.map((item: any) => ({
          label: item ? item.toUpperCase() : null,
          value: item ? item.toUpperCase() : null,
        }))
      ),
      // tap((response: any) => console.log('Response from getAllTenants:', response)),
      catchError(this.handleError)
    );
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

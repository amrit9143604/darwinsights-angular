import { createSelector, createFeatureSelector } from '@ngrx/store';
import { MetricsState } from './metrics.reducers';
 
export const selectMetricData = createFeatureSelector<MetricsState>('metrics');
 
export const metricData = createSelector(
    selectMetricData,
  (state: MetricsState) => state 
);


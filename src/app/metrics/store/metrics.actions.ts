import { createAction, props } from '@ngrx/store';

export const setData = createAction(
  '[Main Metrics Component] Set Data',
  props<{
    userId: string;
    tenant: string | null;
    time: string;
    module: string;
  }>()
);

export const setTenant = createAction(
  '[Main Metrics Component] Set Tenant',
  props<{
    tenant: string;
  }>()
);

export const setTime = createAction(
  '[Main Metrics Component] Set Time',
  props<{
    time: string;
  }>()
);

export const setModule = createAction(
  '[Main Metrics Component] Set Module',
  props<{
    module: string;
  }>()
);

import { createReducer, on } from '@ngrx/store';
import {
  loginSuccess,
  logout,
  registerSuccess,
} from '../../auth/store/auth.actions';
import { setData, setModule, setTenant, setTime } from './metrics.actions';

export interface MetricsState {
  userId: string;
  tenant: string | null;
  time: string;
  module: string | null;
}

export const initialState: MetricsState = {
  userId: '',
  tenant: null,
  time: '',
  module: '',
};

export const metricsReducer = createReducer(
  initialState,
  on(loginSuccess, (state, action) => ({
    ...state,
    userId: action.userId,
    tenant: action.tenant,
    time: action.time,
    module: action.module,
  })),

  on(registerSuccess, (state, action) => ({
    ...state,
    userId: action.userId,
    tenant: action.tenant,
    time: action.time,
    module: action.module,
  })),

  on(logout, (state, action) => ({
    ...state,
    userId: '',
    tenant: '',
    time: '',
    module: '',
  })),

  on(setData, (state, action) => {
    // console.log(action.token);
    return {
      ...state,
      userId: action.userId,
      tenant: action.tenant,
      time: action.time,
      module: action.module,
    };
  }),

  on(setTenant, (state, { tenant }) => {
    // console.log(tenant);
    return {
      ...state,
      tenant: tenant,
    };
  }),

  on(setTime, (state, { time }) => {
    console.log(time);
    return {
      ...state,
      time: time,
    };
  }),

  on(setModule, (state, { module }) => {
    console.log(module);
    return {
      ...state,
      module: module,
    };
  })
);

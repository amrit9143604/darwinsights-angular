import { Component } from '@angular/core';
import { AppService } from '../services/app.service';
import { Store } from '@ngrx/store';
import { MetricsState } from './store/metrics.reducers';
import { setData } from './store/metrics.actions';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { logout } from '../auth/store/auth.actions';

@Component({
  selector: 'app-metrics',
  templateUrl: './metrics.component.html',
  styleUrls: ['./metrics.component.scss'],
})
export class MetricsComponent {
  userId!: string | null;
  constructor(
    private store: Store,
    private router: Router,
    private cookieService: CookieService) {
    const token = this.cookieService.get('JWT_AUTH');
    const userId = this.cookieService.get('USER_ID');
    const tenant = this.cookieService.get('TENANT');
    const time = this.cookieService.get('TIME');
    const module = this.cookieService.get('MODULE');

    if (token && userId && tenant && time) {
      this.store.dispatch(
        setData({ userId: userId, tenant: tenant, time: time, module: module })
      );
    }
  }

  ngOnInit(): void {
    this.userId = this.cookieService.get('USER_ID');
  }

  onLogout(): void {
    this.cookieService.deleteAll( '/');
    this.store.dispatch(logout());
    this.router.navigateByUrl('/auth/login');
  }

  
}

import { createSelector, createFeatureSelector } from '@ngrx/store';
import {
  CardAverageLoadTimeData,
  CardDataState,
  CardMostVisitedModule,
  CardUniqueUsersInMostVisitedModule,
  CardUsersData,
  CardUsersSessionsData,
  CardViewsData,
} from 'src/app/common/interfaces/cardStates';
import {
  BrowserChartData,
  ChartDataState,
  LoadTimeChartData,
  ModuleChartData,
  ModuleDetailsTrendsChartData,
  ModuleSessionChartData,
  PlatformChartData,
  SessionTimeChartData,
  UniqueUsersChartData,
  UsersTrendsChartData,
  VisitsChartData,
} from 'src/app/common/interfaces/chartStates';
import {
  DashboardState,
  VisitsState,
} from 'src/app/common/interfaces/commonStates';

const selectVisitsState = createFeatureSelector<VisitsState>('visits');

export const selectTotalViews = createSelector(
  selectVisitsState,
  (state: VisitsState): CardDataState<CardViewsData> => state.totalViews
);

export const selectUniqueUsers = createSelector(
  selectVisitsState,
  (state: VisitsState): CardDataState<CardUsersData> => state.uniqueUsers
);

export const selectMostUsedModule = createSelector(
  selectVisitsState,
  (state: VisitsState): CardDataState<CardMostVisitedModule> =>
    state.mostUsedModule
);

export const selectUniqueUsersInMostUsedModule = createSelector(
  selectVisitsState,
  (state: VisitsState): CardDataState<CardUniqueUsersInMostVisitedModule> =>
    state.uniqueUsersInMostVisitedModule
);

export const selectVisitsTrends = createSelector(
  selectVisitsState,
  (state: VisitsState): ChartDataState<VisitsChartData> => state.visitsTrends
);

export const selectPlatformTrends = createSelector(
  selectVisitsState,
  (state: VisitsState): ChartDataState<PlatformChartData> => {
    return state.platformTrends;
  }
);

export const selectModuleTrends = createSelector(
  selectVisitsState,
  (state: VisitsState): ChartDataState<ModuleChartData> => state.moduleTrends
);

export const selectUserTrends = createSelector(
  selectVisitsState,
  (state: VisitsState): ChartDataState<UsersTrendsChartData> => {
    // console.log('Load Time Trends State:', state.loadTimeTrends);

    return state.visitedUsers;
  }
);

export const selectModuleDetailedTrends = createSelector(
  selectVisitsState,
  (state: VisitsState): ChartDataState<ModuleDetailsTrendsChartData> => {
    // console.log('Session Time Trends State:', state.sessionTrends);
    return state.moduleDetailsTrends;
  }
);

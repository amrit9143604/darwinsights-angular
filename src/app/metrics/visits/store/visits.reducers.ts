import { createReducer, on } from '@ngrx/store';
import {
  BrowserChartData,
  ChartDataState,
  LoadTimeChartData,
  ModuleChartData,
  ModuleDetailsTrendsChartData,
  ModuleSessionChartData,
  PlatformChartData,
  SessionTimeChartData,
  UniqueUsersChartData,
  UsersTrendsChartData,
  VisitsChartData,
} from 'src/app/common/interfaces/chartStates';
import {
  CardDataState,
  CardAverageLoadTimeData,
  CardUsersData,
  CardUsersSessionsData,
  CardViewsData,
  CardMostVisitedModule,
  CardUniqueUsersInMostVisitedModule,
} from 'src/app/common/interfaces/cardStates';
import * as VisitsActions from './visits.actions';

interface CombinedState {
  totalViews: CardDataState<CardViewsData>;
  uniqueUsers: CardDataState<CardUsersData>;
  mostUsedModule: CardDataState<CardMostVisitedModule>;
  uniqueUsersInMostVisitedModule: CardDataState<CardUniqueUsersInMostVisitedModule>;
  visitsTrends: ChartDataState<VisitsChartData>;
  platformTrends: ChartDataState<PlatformChartData>;
  moduleTrends: ChartDataState<ModuleChartData>;
  visitedUsers: ChartDataState<UsersTrendsChartData>;
  moduleDetailsTrends: ChartDataState<ModuleDetailsTrendsChartData>;
}

const initialTotalViewsState: CardDataState<CardViewsData> = {
  data: { totalViews: null, percentageChange: null },
  loading: false,
  error: null,
};
const initialUniqueUsersState: CardDataState<CardUsersData> = {
  data: { totalUsers: null, percentageChange: null },
  loading: false,
  error: null,
};
const initialMostUsedModuleState: CardDataState<CardMostVisitedModule> = {
  data: { mostVisitedModule: null, views: null },
  loading: false,
  error: null,
};
const initialUniqueUsersInMostVisitedModuleState: CardDataState<CardUniqueUsersInMostVisitedModule> =
  {
    data: { mostVisitedModule: null, uniqueUserCount: null },
    loading: false,
    error: null,
  };
const initialVisitsTrendsState: ChartDataState<VisitsChartData> = {
  data: { clickTrends: [] },
  loading: false,
  error: null,
};

const initialPlatformTrendsState: ChartDataState<PlatformChartData> = {
  data: { result: [] },
  loading: false,
  error: null,
};

const initialModuleTrends: ChartDataState<ModuleChartData> = {
  data: { visitedModules: [] },
  loading: false,
  error: null,
};
const initialUsersModule: ChartDataState<UsersTrendsChartData> = {
  data: { visitedUsers: [] },
  loading: false,
  error: null,
};

const initialModuleDetailsTrendsState: ChartDataState<ModuleDetailsTrendsChartData> =
  {
    data: { moduleDetailedMetrics: [] },
    loading: false,
    error: null,
  };

const initialState: CombinedState = {
  uniqueUsers: initialUniqueUsersState,
  totalViews: initialTotalViewsState,
  mostUsedModule: initialMostUsedModuleState,
  uniqueUsersInMostVisitedModule: initialUniqueUsersInMostVisitedModuleState,
  visitsTrends: initialVisitsTrendsState,
  moduleTrends: initialModuleTrends,
  platformTrends: initialPlatformTrendsState,
  visitedUsers: initialUsersModule,
  moduleDetailsTrends: initialModuleDetailsTrendsState,
};

export const visitsReducer = createReducer(
  initialState,
  on(VisitsActions.totalViews, (state) => ({
    ...state,
    totalViews: { ...state.totalViews, loading: true },
  })),
  on(
    VisitsActions.totalViewsSuccess,
    (state, { totalViews, percentageChange }) => {
      //   console.log('Total views updated:', { totalViews, percentageChange });
      return {
        ...state,
        totalViews: {
          ...state.totalViews,
          loading: false,
          data: { totalViews, percentageChange },
        },
      };
    }
  ),
  on(VisitsActions.totalViewsFailure, (state, { error }) => ({
    ...state,
    totalViews: {  data: { totalViews: null, percentageChange: null }, loading: false, error },
  })),

  on(VisitsActions.uniqueUsers, (state) => ({
    ...state,
    uniqueUsers: { ...state.uniqueUsers, loading: true },
  })),
  on(
    VisitsActions.uniqueUsersSuccess,
    (state, { totalUsers, percentageChange }) => ({
      ...state,
      uniqueUsers: {
        ...state.uniqueUsers,
        loading: false,
        data: { totalUsers, percentageChange },
      },
    })
  ),
  on(VisitsActions.uniqueUsersFailure, (state, { error }) => ({
    ...state,
    uniqueUsers: {  data: { totalUsers: null, percentageChange: null }, loading: false, error },
  })),

  on(VisitsActions.mostUsedModule, (state) => ({
    ...state,
    mostUsedModule: { ...state.mostUsedModule, loading: true },
  })),
  on(
    VisitsActions.mostUsedModuleSuccess,
    (state, { mostVisitedModule, views }) => ({
      ...state,
      mostUsedModule: {
        ...state.mostUsedModule,
        loading: false,
        data: { mostVisitedModule, views },
      },
    })
  ),
  on(VisitsActions.mostUsedModuleFailure, (state, { error }) => ({
    ...state,
    mostUsedModule: {  data: { mostVisitedModule: null, views: null }, loading: false, error },
  })),

  on(VisitsActions.uniqueUserCountInMostVisitedModule, (state) => ({
    ...state,
    uniqueUsersInMostVisitedModule: {
      ...state.uniqueUsersInMostVisitedModule,
      loading: true,
    },
  })),
  on(
    VisitsActions.uniqueUserCountInMostVisitedModuleSuccess,
    (state, { mostVisitedModule, uniqueUserCount }) => ({
      ...state,
      uniqueUsersInMostVisitedModule: {
        ...state.uniqueUsersInMostVisitedModule,
        loading: false,
        data: { mostVisitedModule, uniqueUserCount },
      },
    })
  ),
  on(
    VisitsActions.uniqueUserCountInMostVisitedModuleFailure,
    (state, { error }) => ({
      ...state,
      uniqueUsersInMostVisitedModule: {
        data: { mostVisitedModule: null, uniqueUserCount: null },
        loading: false,
        error,
      },
    })
  ),

  on(VisitsActions.visitsTrends, (state) => ({
    ...state,
    visitsTrends: { ...state.visitsTrends, loading: true },
  })),
  on(VisitsActions.visitsTrendsSuccess, (state, { clickTrends }) => ({
    ...state,
    visitsTrends: {
      ...state.visitsTrends,
      loading: false,
      data: { clickTrends },
    },
  })),
  on(VisitsActions.visitsTrendsFailure, (state, { error }) => ({
    ...state,
    visitsTrends: { data: { clickTrends: [] }, loading: false, error },
  })),

  on(VisitsActions.platformTrends, (state) => ({
    ...state,
    platformTrends: { ...state.platformTrends, loading: true },
  })),
  on(VisitsActions.platformTrendsSuccess, (state, { result }) => ({
    ...state,
    platformTrends: {
      ...state.platformTrends,
      loading: false,
      data: { result },
    },
  })),
  on(VisitsActions.platformTrendsFailure, (state, { error }) => ({
    ...state,
    platformTrends: { data: { result: [] }, loading: false, error },
  })),

  on(VisitsActions.moduleTrends, (state) => ({
    ...state,
    moduleTrends: { ...state.moduleTrends, loading: true },
  })),
  on(VisitsActions.moduleTrendsSuccess, (state, { visitedModules }) => ({
    ...state,
    moduleTrends: {
      ...state.moduleTrends,
      loading: false,
      data: { visitedModules },
    },
  })),
  on(VisitsActions.moduleTrendsFailure, (state, { error }) => ({
    ...state,
    moduleTrends: { data: { visitedModules: [] }, loading: false, error },
  })),

  on(VisitsActions.usersTrends, (state) => ({
    ...state,
    visitedUsers: { ...state.visitedUsers, loading: true },
  })),
  on(VisitsActions.usersTrendsSuccess, (state, { visitedUsers }) => ({
    ...state,
    visitedUsers: {
      ...state.visitedUsers,
      loading: false,
      data: { visitedUsers },
    },
  })),
  on(VisitsActions.usersTrendsFailure, (state, { error }) => ({
    ...state,
    visitedUsers: { data: { visitedUsers: [] }, loading: false, error },
  })),

  on(VisitsActions.moduleDetailedTrends, (state) => ({
    ...state,
    moduleDetailsTrends: { ...state.moduleDetailsTrends, loading: true },
  })),
  on(
    VisitsActions.moduleDetailedTrendsSuccess,
    (state, { moduleDetailedMetrics }) => ({
      ...state,
      moduleDetailsTrends: {
        ...state.moduleDetailsTrends,
        loading: false,
        data: { moduleDetailedMetrics },
      },
    })
  ),
  on(VisitsActions.moduleDetailedTrendsFailure, (state, { error }) => ({
    ...state,
    moduleDetailsTrends: {
      data: { moduleDetailedMetrics: [] },
      loading: false,
      error,
    },
  }))
);

import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { map, switchMap, catchError, mergeMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import * as VisitsActions from './visits.actions';
import { Action } from '@ngrx/store';
import { VisitsService } from '../services/visits.service';

@Injectable()
export class VisitsEffects {
  constructor(
    private actions$: Actions,
    private visitsService: VisitsService
  ) {}

  createEffectForAction$ = (
    actionType: string,
    serviceFn: () => Observable<any>,
    successFn: (payload: any) => Action,
    failureFn: (payload: any) => Action
  ): Observable<Action> =>
    createEffect(() =>
      this.actions$.pipe(
        ofType(actionType),
        switchMap(() =>
          serviceFn().pipe(
            map(successFn),
            catchError((error) => of(failureFn({ error })))
          )
        )
      )
    );

  totalViews$ = this.createEffectForAction$(
    VisitsActions.totalViews.type,
    this.visitsService.getTotalViews.bind(this.visitsService),
    (payload) => VisitsActions.totalViewsSuccess(payload),
    (payload) => VisitsActions.totalViewsFailure(payload)
  );

  uniqueUsers$ = this.createEffectForAction$(
    VisitsActions.uniqueUsers.type,
    this.visitsService.getUniqueUsers.bind(this.visitsService),
    (payload) => VisitsActions.uniqueUsersSuccess(payload),
    (payload) => VisitsActions.uniqueUsersFailure(payload)
  );

  mostUsedModule$ = this.createEffectForAction$(
    VisitsActions.mostUsedModule.type,
    this.visitsService.getMostUsedModule.bind(this.visitsService),
    (payload) => VisitsActions.mostUsedModuleSuccess(payload),
    (payload) => VisitsActions.mostUsedModuleFailure(payload)
  );

  uniqueUsersInMostUsedModule$ = this.createEffectForAction$(
    VisitsActions.uniqueUserCountInMostVisitedModule.type,
    this.visitsService.getUniqueUserCountInMostVisitedModule.bind(
      this.visitsService
    ),
    (payload) =>
      VisitsActions.uniqueUserCountInMostVisitedModuleSuccess(payload),
    (payload) =>
      VisitsActions.uniqueUserCountInMostVisitedModuleFailure(payload)
  );

  visitsTrends$ = this.createEffectForAction$(
    VisitsActions.visitsTrends.type,
    this.visitsService.getVisitsTrends.bind(this.visitsService),
    (payload) => VisitsActions.visitsTrendsSuccess(payload),
    (payload) => VisitsActions.visitsTrendsFailure(payload)
  );

  platformTrends$ = this.createEffectForAction$(
    VisitsActions.platformTrends.type,
    this.visitsService.getPlatformTrends.bind(this.visitsService),
    (payload) => VisitsActions.platformTrendsSuccess(payload),
    (payload) => VisitsActions.platformTrendsFailure(payload)
  );

  usersTrends$ = this.createEffectForAction$(
    VisitsActions.usersTrends.type,
    this.visitsService.getUsersTrends.bind(this.visitsService),
    (payload) => VisitsActions.usersTrendsSuccess(payload),
    (payload) => VisitsActions.usersTrendsFailure(payload)
  );

  moduleTrends$ = this.createEffectForAction$(
    VisitsActions.moduleTrends.type,
    this.visitsService.getModuleTrends.bind(this.visitsService),
    (payload) => VisitsActions.moduleTrendsSuccess(payload),
    (payload) => VisitsActions.moduleTrendsFailure(payload)
  );

  moduleDetailedTrends$ = this.createEffectForAction$(
    VisitsActions.moduleDetailedTrends.type,
    this.visitsService.getModuleDetailedMetrics.bind(this.visitsService),
    (payload) => VisitsActions.moduleDetailedTrendsSuccess(payload),
    (payload) => VisitsActions.moduleDetailedTrendsFailure(payload)
  );
}

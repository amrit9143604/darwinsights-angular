import { createAction, props } from '@ngrx/store';
import { BrowserData, LoadTimeData, ModuleData, ModuleDetailsTrendsData, ModuleSessionData, PlatformData, SessionData, UsersData, UsersTrendsData, VisitsData } from 'src/app/common/interfaces/chartStates';


export const totalViews = createAction(
  '[Metric Visits Component] TotalViews'
);
export const totalViewsSuccess = createAction(
  '[Metric Visits Component] TotalViewsSuccess',
  props<{ totalViews: number | null; percentageChange: number | null }>()
);
export const totalViewsFailure = createAction(
  '[Metric Visits Component] TotalViewsFailure',
  props<{ error: string }>()
);

export const uniqueUsers = createAction(
  '[Metric Visits Component] UniqueUsers'
);
export const uniqueUsersSuccess = createAction(
  '[Metric Visits Component] UniqueUsersSuccess',
  props<{ totalUsers: number | null; percentageChange: number | null }>()
);
export const uniqueUsersFailure = createAction(
  '[Metric Visits Component] UniqueUsersFailure',
  props<{ error: string }>()
);

export const mostUsedModule = createAction(
  '[Metric Visits Component] MostUsedModule'
);
export const mostUsedModuleSuccess = createAction(
  '[Metric Visits Component] MostUsedModuleSuccess',
  props<{ mostVisitedModule: string | null; views: number | null }>()
);
export const mostUsedModuleFailure = createAction(
  '[Metric Visits Component] MostUsedModuleFailure',
  props<{ error: string }>()
);

export const uniqueUserCountInMostVisitedModule = createAction(
  '[Metric Visits Component] UniqueUserCountInMostVisitedModule'
);
export const uniqueUserCountInMostVisitedModuleSuccess = createAction(
  '[Metric Visits Component] UniqueUserCountInMostVisitedModuleSuccess',
  props<{
    mostVisitedModule: string | null;
    uniqueUserCount : number | null;
  }>()
);
export const uniqueUserCountInMostVisitedModuleFailure = createAction(
  '[Metric Visits Component] UniqueUserCountInMostVisitedModuleFailure',
  props<{ error: string }>()
);

export const visitsTrends = createAction(
  '[Metric Visits Component] VisitsTrends'
);
export const visitsTrendsSuccess = createAction(
  '[Metric Visits Component] VisitsTrendsSuccess',
  props<{ clickTrends: VisitsData[] }>()
);
export const visitsTrendsFailure = createAction(
  '[Metric Visits Component] VisitsTrendsFailure',
  props<{ error: string }>()
);

export const platformTrends = createAction(
    '[Metric Visits Component] PlatformTrends'
  );
  export const platformTrendsSuccess = createAction(
    '[Metric Visits Component] PlatformTrendsSuccess',
    props<{ result: PlatformData[] }>()
  );
  export const platformTrendsFailure = createAction(
    '[Metric Dashboard Component] PlatformTrendsFailure',
    props<{ error: string }>()
  );
  

export const usersTrends = createAction(
  '[Metric Dashboard Component] UsersTrends'
);
export const usersTrendsSuccess = createAction(
  '[Metric Visits Component] UsersTrendsSuccess',
  props<{ visitedUsers: UsersTrendsData[] }>()
);
export const usersTrendsFailure = createAction(
  '[Metric Visits Component] UsersTrendsFailure',
  props<{ error: string }>()
);


export const moduleTrends = createAction(
  '[Metric Visits Component] ModuleTrends'
);
export const moduleTrendsSuccess = createAction(
  '[Metric Visits Component] ModuleTrendsSuccess',
  props<{ visitedModules: ModuleData[] }>()
);
export const moduleTrendsFailure = createAction(
  '[Metric Visits Component] ModuleTrendsFailure',
  props<{ error: string }>()
);


export const moduleDetailedTrends = createAction(
  '[Metric Visits Component] ModuleDetailedTrends'
);
export const moduleDetailedTrendsSuccess = createAction(
  '[Metric Visits Component] ModuleDetailedTrendsSuccess',
  props<{ moduleDetailedMetrics: ModuleDetailsTrendsData[] }>()
);
export const moduleDetailedTrendsFailure = createAction(
  '[Metric Visits Component] ModuleDetailedTrendsFailure',
  props<{ error: string }>()
);

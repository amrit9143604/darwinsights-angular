import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subject, Subscription } from 'rxjs';
import { map, takeUntil, tap } from 'rxjs/operators';
import {
  ChartDataState,
  ModuleChartData,
  ModuleDetailedLoadData,
  ModuleDetailsTrendsChartData,
  PlatformChartData,
  UsersTrendsChartData,
  UsersTrendsData,
  VisitsChartData,
} from 'src/app/common/interfaces/chartStates';
import {
  metricOptions,
  timeArray,
  timeLabels,
} from 'src/app/common/interfaces/common';
import { VisitsState } from 'src/app/common/interfaces/commonStates';
import { VisitsService } from './services/visits.service';
import { CookieService } from 'ngx-cookie-service';
import { metricData } from '../store/metrics.selectors';
import { setTenant, setTime } from '../store/metrics.actions';
import {
  moduleDetailedTrends,
  moduleTrends,
  mostUsedModule,
  platformTrends,
  totalViews,
  uniqueUserCountInMostVisitedModule,
  uniqueUsers,
  usersTrends,
  visitsTrends,
} from './store/visits.actions';
import { uniqueUsersTrends } from '../dashboard/store/dashboard.actions';
import {
  selectModuleDetailedTrends,
  selectModuleTrends,
  selectMostUsedModule,
  selectPlatformTrends,
  selectTotalViews,
  selectUniqueUsers,
  selectUniqueUsersInMostUsedModule,
  selectUserTrends,
  selectVisitsTrends,
} from './store/visits.selectors';
import { MetricsService } from '../services/metrics.service';

@Component({
  selector: 'app-visits',
  templateUrl: './visits.component.html',
  styleUrls: ['./visits.component.scss'],
})
export class VisitsComponent implements OnInit, OnDestroy {
  loadingState = {
    views: false,
    users: false,
    mostUsedModule: false,
    uniqueUsersInMostVisitedModule: false,
    visitsTrends: false,
    platformTrends: false,
    moduleTrends: false,
    userTrends: false,
    moduleDetailsTrends: false,
  };

  tenants$!: Observable<metricOptions[]>;
  selectedTenant!: metricOptions;

  time!: metricOptions[];
  selectedTime!: metricOptions;

  totalViews$!: Observable<number | null>;
  totalViewsPercentage$!: Observable<number | null>;
  totalUsers$!: Observable<number | null>;
  totalUsersPercentage$!: Observable<number | null>;
  mostUsedModule$!: Observable<string | null>;
  mostUsedModuleViews$!: Observable<number | null>;
  uniqueUsersInMostUsedModule$!: Observable<string | null>;
  uniqueUsersInMostUsedModuleCount$!: Observable<number | null>;
  visitsOptions$!: Observable<ChartDataState<VisitsChartData>>;
  platformOptions$!: Observable<ChartDataState<PlatformChartData>>;
  moduleTrendsOptions$!: Observable<ChartDataState<ModuleChartData>>;
  userTrendsOptions$!: Observable<ChartDataState<UsersTrendsChartData>>;
  moduleDetailsTrendsOptions$!: Observable<
    ChartDataState<ModuleDetailsTrendsChartData>
  >;

  userRowData!: UsersTrendsData[];
  moduleRowData!: ModuleDetailedLoadData[];

  userColumnData = [
    { headerName: 'User ID', field: 'userId' },
    { headerName: 'Views', field: 'count' },
    { headerName: 'IP Address', field: 'ip' },
  ];

  moduleColumnData = [
    { headerName: 'Module', field: 'moduleName' },
    { headerName: 'Route', field: 'route' },
    { headerName: 'Page Visits', field: 'totalClicks' },
    { headerName: 'Platform', field: 'platform' },
    { headerName: 'Average Load Time', field: 'avgLoadTime' },
  ];
  visitsChartType: boolean = true;
  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private store: Store<VisitsState>,
    private metricService: MetricsService,
    private cookieService: CookieService
  ) {
    this.time = timeArray;
  }
  ngOnInit(): void {
    this.store
      .pipe(select(metricData), takeUntil(this.destroy$))
      .subscribe((state) => {
        this.selectedTenant = {
          label: state.tenant ?? '',
          value: state.tenant ?? '',
        };
        this.selectedTime = {
          label: timeLabels[state.time] ?? state.time ?? '',
          value: state.time ?? '',
        };
      });

    this.fetchTenants();
    this.dispatchActions();
    this.retrieveData();
  }

  onTenantChange(value: string) {
    this.cookieService.set('TENANT', value, 2, '/');
    this.store.dispatch(setTenant({ tenant: value }));
    this.dispatchActions();
    this.retrieveData();
  }

  onTimeChange(value: string) {
    this.cookieService.set('TIME', value, 2, '/');
    this.store.dispatch(setTime({ time: value }));
    this.dispatchActions();
    this.retrieveData();
  }

  private dispatchActions(): void {
    this.store.dispatch(totalViews());
    this.store.dispatch(uniqueUsers());
    this.store.dispatch(mostUsedModule());
    this.store.dispatch(uniqueUserCountInMostVisitedModule());
    this.store.dispatch(visitsTrends());
    this.store.dispatch(platformTrends());
    this.store.dispatch(moduleTrends());
    this.store.dispatch(usersTrends());
    this.store.dispatch(moduleDetailedTrends());
  }

  private fetchTenants(): void {
    this.tenants$ = this.metricService.getAllTenants().pipe(
      takeUntil(this.destroy$)
    );
  }

  private retrieveData(): void {
    this.retrieveTotalViews();
    this.retrieveUniqueUsers();
    this.retreiveMostUsedModule();
    this.retrieveUniqueUsersInMostUsedModule();
    this.retrieveVisitsTrends();
    this.retrievePlatformTrends();
    this.retrieveModuleTrends();
    this.retrieveUsersTrends();
    this.retrieveModuleDetailsTrends();
  }

  private retrieveTotalViews(): void {
    this.totalViews$ = this.store.pipe(
      select(selectTotalViews),
      map((state) => state.data.totalViews)
    );
    this.totalViewsPercentage$ = this.store.pipe(
      select(selectTotalViews),
      map((state) => state.data.percentageChange)
    );
    this.subscribeToLoadingState('views', selectTotalViews);
  }

  private retrieveUniqueUsers(): void {
    this.totalUsers$ = this.store.pipe(
      select(selectUniqueUsers),
      map((state) => state.data.totalUsers)
    );
    this.totalUsersPercentage$ = this.store.pipe(
      select(selectUniqueUsers),
      map((state) => state.data.percentageChange)
    );
    this.subscribeToLoadingState('users', selectUniqueUsers);
  }

  private retreiveMostUsedModule(): void {
    this.mostUsedModule$ = this.store.pipe(
      select(selectMostUsedModule),
      map((state) => state.data.mostVisitedModule)
    );

    this.mostUsedModuleViews$ = this.store.pipe(
      select(selectMostUsedModule),
      map((state) => state.data.views)
    );
    this.subscribeToLoadingState('mostUsedModule', selectMostUsedModule);
  }

  private retrieveUniqueUsersInMostUsedModule(): void {
    this.uniqueUsersInMostUsedModule$ = this.store.pipe(
      select(selectUniqueUsersInMostUsedModule),
      map((state) => state.data.mostVisitedModule)
    );

    this.uniqueUsersInMostUsedModuleCount$ = this.store.pipe(
      select(selectUniqueUsersInMostUsedModule),
      map((state) => state.data.uniqueUserCount)
    );
    this.subscribeToLoadingState(
      'uniqueUsersInMostVisitedModule',
      selectUniqueUsersInMostUsedModule
    );
  }

  private retrieveVisitsTrends(): void {
    this.visitsOptions$ = this.store.pipe(select(selectVisitsTrends));
    this.subscribeToLoadingState('visitsTrends', selectVisitsTrends);
  }

  private retrievePlatformTrends(): void {
    this.platformOptions$ = this.store.pipe(select(selectPlatformTrends));
    this.subscribeToLoadingState('platformTrends', selectPlatformTrends);
  }

  private retrieveModuleTrends(): void {
    this.moduleTrendsOptions$ = this.store.pipe(select(selectModuleTrends));
    this.subscribeToLoadingState('moduleTrends', selectModuleTrends);
  }

  private retrieveUsersTrends(): void {
    this.userTrendsOptions$ = this.store.pipe(select(selectUserTrends));
    this.subscribeToLoadingState('userTrends', selectUserTrends);
  }

  private retrieveModuleDetailsTrends(): void {
    this.moduleDetailsTrendsOptions$ = this.store.pipe(
      select(selectModuleDetailedTrends)
    );
    this.subscribeToLoadingState(
      'moduleDetailsTrends',
      selectModuleDetailedTrends
    );
  }

  handleChartType(chartType: string) {
    switch (chartType) {
      case 'visits':
        this.visitsChartType = !this.visitsChartType;
        break;

      default:
        break;
    }
  }
  private subscribeToLoadingState(
    prop: keyof (typeof VisitsComponent)['prototype']['loadingState'],
    selector: any
  ): void {
    this.store
      .pipe(
        select(selector),
        tap((state: any) => (this.loadingState[prop] = state.loading)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

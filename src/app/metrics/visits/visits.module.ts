import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VisitsRoutingModule } from './visits-routing.module';
import { VisitsComponent } from './visits.component';
import { visitsReducer } from './store/visits.reducers';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { VisitsEffects } from './store/visits.effects';


@NgModule({
  declarations: [
    VisitsComponent
  ],
  imports: [
    CommonModule,
    VisitsRoutingModule,
    SharedModule,
    FormsModule,
    DropdownModule,
    StoreModule.forFeature('visits', visitsReducer),
    EffectsModule.forFeature([VisitsEffects]),
  ]
})
export class VisitsModule { }

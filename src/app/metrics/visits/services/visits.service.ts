import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
  throwError,
  Observable,
  tap,
  map,
  catchError,
  Subscription,
  Subject,
  takeUntil,
} from 'rxjs';
import {
  VISITS_CARD_TOTAL_VIEWS,
  VISITS_CARD_UNIQUE_USERS,
  VISITS_CARD_MODULE_USER_COUNT,
  VISITS_CARD_MOST_VISITED_MODULE,
  VISITS_CHART_PLATFORM_COUNT,
  VISITS_CHART_VISITS_TRENDS,
  VISITS_CHART_MODULE_TRENDS,
  VISITS_TABLE_USERS_TRENDS,
  VISITS_TABLE_DATA,
} from 'src/app/common/apis/constants';
import { metricData } from '../../store/metrics.selectors';
import { CardMostVisitedModule, CardUniqueUsersInMostVisitedModule, CardUsersData, CardViewsData } from 'src/app/common/interfaces/cardStates';
import { ModuleChartData, ModuleDetailsTrendsChartData, PlatformChartData, UsersTrendsChartData, VisitsChartData } from 'src/app/common/interfaces/chartStates';

@Injectable({
  providedIn: 'root',
})
export class VisitsService {
  private tenantId!: string | null;
  private range!: string | null;
  private body!: { tenantId: string | null; range: string | null };
  private destroy$: Subject<void> = new Subject<void>();


  constructor(private http: HttpClient, private store: Store) {
    this.store.pipe(select(metricData), takeUntil(this.destroy$)).subscribe((state) => {
      this.tenantId = state.tenant;
      this.range = state.time;
      this.body = { tenantId: this.tenantId, range: this.range };
    });
  }

  private handleError(error: string) {
    console.error('An error occurred:', error);
    return throwError('Something went wrong');
  }

  private post(url: string): Observable<Object> {
    console.log(this.http.post(url, this.body));
    return this.http.post(url, this.body);
  }

  getTotalViews(): Observable<CardViewsData> {
    return this.post(VISITS_CARD_TOTAL_VIEWS).pipe(
      map((response: any) => ({
        totalViews: response.totalViews,
        percentageChange: response.clickPercentageChange,
      })),
      catchError(this.handleError)
    );
  }

  getUniqueUsers(): Observable<CardUsersData> {
    return this.post(VISITS_CARD_UNIQUE_USERS).pipe(
      map((response: any) => ({
        totalUsers: response.totalUsers,
        percentageChange: response.usersPercentageChange,
      })),
      catchError(this.handleError)
    );
  }

  getMostUsedModule(): Observable<CardMostVisitedModule> {
    return this.post(VISITS_CARD_MODULE_USER_COUNT).pipe(
      map((response: any) => ({
        mostVisitedModule: response.mostVisitedModuleCount.mostVisitedModule,
        views: response.mostVisitedModuleCount.views,
      })),
      catchError(this.handleError)
    );
  }

  getUniqueUserCountInMostVisitedModule(): Observable<CardUniqueUsersInMostVisitedModule> {
    return this.post(VISITS_CARD_MOST_VISITED_MODULE).pipe(
      // tap((data) => console.log(data.mostVisitedModuleUserCount.uniqueUserCount)),
      map((response: any) => ({
        mostVisitedModule:
          response.mostVisitedModuleUserCount.mostVisitedModule,
        uniqueUserCount: response.mostVisitedModuleUserCount.uniqueUserCount,
      })),
      catchError(this.handleError)
    );
  }

  getVisitsTrends(): Observable<VisitsChartData> {
    return this.post(VISITS_CHART_VISITS_TRENDS).pipe(
      map((response: any) => ({
        clickTrends: response.clickTrends,
      })),
      catchError(this.handleError)
    );
  }

  getPlatformTrends(): Observable<PlatformChartData> {
    return this.post(VISITS_CHART_PLATFORM_COUNT).pipe(
      map((response: any) => ({
        result: response.platformCount.result,
      })),
      catchError(this.handleError)
    );
  }

  getUsersTrends(): Observable<UsersTrendsChartData> {
    return this.post(VISITS_TABLE_USERS_TRENDS).pipe(
      map((response: any) => ({
        visitedUsers: response.visitedUsers,
      })),
      catchError(this.handleError)
    );
  }

  getModuleTrends(): Observable<ModuleChartData> {
    return this.post(VISITS_CHART_MODULE_TRENDS).pipe(
      map((response: any) => ({
        visitedModules: response.visitedModules.result,
      })),
      catchError(this.handleError)
    );
  }

  getModuleDetailedMetrics(): Observable<ModuleDetailsTrendsChartData> {
    return this.post(VISITS_TABLE_DATA).pipe(
      map((response: any) => ({
        moduleDetailedMetrics: response.moduleDetailedMetrics,
      })),
      catchError(this.handleError)
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
